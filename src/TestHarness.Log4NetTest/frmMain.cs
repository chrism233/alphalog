﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="frmMain.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The frm main.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Log4NetTest
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Windows.Forms;

    using log4net;

    /// <summary>
    ///     The frm main.
    /// </summary>
    public partial class frmMain : Form
    {
        // Here is the once-per-class call to initialize the log object
        #region Static Fields

        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="frmMain" /> class.
        /// </summary>
        public frmMain()
        {
            this.InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// The debug_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void debug_Click(object sender, EventArgs e)
        {
            log.Debug("Debug level log entry", new FileNotFoundException());
        }

        /// <summary>
        /// The error_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void error_Click(object sender, EventArgs e)
        {
            log.Error("Error level log entry", new FileNotFoundException());
        }

        /// <summary>
        /// The fatal_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void fatal_Click(object sender, EventArgs e)
        {
            log.Fatal("Fatal level log entry", new FileNotFoundException());
        }

        /// <summary>
        /// The frm main_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void frmMain_Load(object sender, EventArgs e)
        {
            // You should try to call the logger as soon as possible in your application
            log.Debug("Application started");
        }

        /// <summary>
        /// The info_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void info_Click(object sender, EventArgs e)
        {
            log.Info("Info level log entry", new FileNotFoundException());
        }

        #endregion
    }
}