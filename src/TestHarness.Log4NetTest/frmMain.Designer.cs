﻿namespace Log4NetTest
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.error = new System.Windows.Forms.Button();
            this.debug = new System.Windows.Forms.Button();
            this.info = new System.Windows.Forms.Button();
            this.fatal = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // error
            // 
            this.error.Location = new System.Drawing.Point(12, 12);
            this.error.Name = "error";
            this.error.Size = new System.Drawing.Size(75, 23);
            this.error.TabIndex = 2;
            this.error.Text = "Error";
            this.error.UseVisualStyleBackColor = true;
            this.error.Click += new System.EventHandler(this.error_Click);
            // 
            // debug
            // 
            this.debug.Location = new System.Drawing.Point(94, 11);
            this.debug.Name = "debug";
            this.debug.Size = new System.Drawing.Size(75, 23);
            this.debug.TabIndex = 3;
            this.debug.Text = "Debug";
            this.debug.UseVisualStyleBackColor = true;
            this.debug.Click += new System.EventHandler(this.debug_Click);
            // 
            // info
            // 
            this.info.Location = new System.Drawing.Point(176, 11);
            this.info.Name = "info";
            this.info.Size = new System.Drawing.Size(75, 23);
            this.info.TabIndex = 4;
            this.info.Text = "Info";
            this.info.UseVisualStyleBackColor = true;
            this.info.Click += new System.EventHandler(this.info_Click);
            // 
            // fatal
            // 
            this.fatal.Location = new System.Drawing.Point(258, 11);
            this.fatal.Name = "fatal";
            this.fatal.Size = new System.Drawing.Size(75, 23);
            this.fatal.TabIndex = 5;
            this.fatal.Text = "Fatal";
            this.fatal.UseVisualStyleBackColor = true;
            this.fatal.Click += new System.EventHandler(this.fatal_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(349, 47);
            this.Controls.Add(this.fatal);
            this.Controls.Add(this.info);
            this.Controls.Add(this.debug);
            this.Controls.Add(this.error);
            this.Name = "frmMain";
            this.Text = "Main";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button error;
        private System.Windows.Forms.Button debug;
        private System.Windows.Forms.Button info;
        private System.Windows.Forms.Button fatal;
    }
}

