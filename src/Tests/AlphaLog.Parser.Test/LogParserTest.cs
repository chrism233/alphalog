﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LogParserTest.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The testable log parser.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Parser.Tests
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Text.RegularExpressions;

    using AlphaLog.RegexColumniser.Parser;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    // public class TestableLogParser : LogParser
    // {
    // #region Public Methods and Operators

    // /// <summary>
    // /// The get index of log entry.
    // /// </summary>
    // /// <param name="details">
    // /// The details.
    // /// </param>
    // /// <param name="entry">
    // /// The entry.
    // /// </param>
    // /// <returns>
    // /// The <see cref="long"/>.
    // /// </returns>
    // public new long GetIndexOfLogEntry(LogFileDetails details, DateTime entry)
    // {
    // return base.GetIndexOfLogEntry(details, entry);
    // }

    // #endregion
    // }
    /// <summary>
    ///     The testable log parser.
    /// </summary>
    /// <summary>
    ///     The log parser test.
    /// </summary>
    [TestClass]
    public class LogParserTest
    {
        #region Public Methods and Operators

        /// <summary>
        ///     The basic test.
        /// </summary>
        [TestMethod]
        public void BasicTest()
        {
            // var parser = new LogParser();

            // IEnumerable<string> parseLogEntries = parser.ParseFile("Example.log");

            // Assert.IsTrue(parseLogEntries.Count() > 0);
        }

        /// <summary>
        ///     The get average line length.
        /// </summary>
        [TestMethod]
        public void GetAverageLineLength()
        {
            using (StreamReader sr = File.OpenText("Example.log"))
            {
                int totalLines = 0;

                while (!sr.EndOfStream)
                {
                    sr.ReadLine();
                    totalLines++;
                }

                Debug.Print("Average line length = {0}", sr.BaseStream.Length / totalLines);
            }
        }

        /// <summary>
        ///     The get index of entry.
        /// </summary>
        [Ignore]
        [TestMethod]
        public void GetIndexOfEntry()
        {
            int entry = 4;

            using (StreamReader sr = File.OpenText("SimpleLogFile.txt"))
            {
                long upperIndex = 0;
                long lowwerIndex = sr.BaseStream.Length;

                int logId = 0;

                do
                {
                    long index = upperIndex + ((lowwerIndex - upperIndex) / 2);

                    Tuple<int, long> result = this.GetLogIdAtIndex(sr, index);

                    logId = result.Item1;

                    if (entry < logId)
                    {
                        lowwerIndex = result.Item2;
                    }
                    else
                    {
                        upperIndex = result.Item2;
                    }
                }
                while (entry != logId);
            }
        }

        /// <summary>
        ///     The get index of entry_ simple.
        /// </summary>
        [TestMethod]
        public void GetIndexOfEntry_Simple()
        {
            using (StreamReader sr = File.OpenText("Example.log"))
            {
                string line = string.Empty;

                Match match = Constants.StartOfLogEntryRegex.Match(line);

                DateTime date;

                long pos;

                do
                {
                    do
                    {
                        pos = sr.BaseStream.Position;

                        line = sr.ReadLine();

                        match = Constants.StartOfLogEntryRegex.Match(line);
                    }
                    while (!match.Success);

                    date = DateTime.Parse(match.Value.Replace(",", "."));
                }
                while (date != new DateTime(2013, 09, 10, 11, 06, 47, 014));
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get indexof log entry.
        /// </summary>
        /// <summary>
        /// The parse from start date.
        /// </summary>
        /// <summary>
        /// The parse to end date.
        /// </summary>
        /// <summary>
        /// The get log id at index.
        /// </summary>
        /// <param name="sr">
        /// The sr.
        /// </param>
        /// <param name="index">
        /// The index.
        /// </param>
        /// <returns>
        /// The <see cref="Tuple"/>.
        /// </returns>
        private Tuple<int, long> GetLogIdAtIndex(StreamReader sr, long index)
        {
            sr.BaseStream.Position = index;
            sr.DiscardBufferedData();

            Match match;

            long tempPos = 0;

            do
            {
                tempPos = sr.BaseStream.Position;
                match = Regex.Match(sr.ReadLine(), @"\-(?'Group'\d{1,2})\s\-");
            }
            while (!match.Success);

            return new Tuple<int, long>(int.Parse(match.Groups["Group"].Value), tempPos);
        }

        #endregion
    }
}