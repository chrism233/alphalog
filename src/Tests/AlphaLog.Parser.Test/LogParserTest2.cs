﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LogParserTest2.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The log parser test 2.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Parser.Tests
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Text.RegularExpressions;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    ///     The log parser test 2.
    /// </summary>
    [TestClass]
    public class LogParserTest2
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get actual position.
        /// </summary>
        /// <param name="reader">
        /// The reader.
        /// </param>
        /// <returns>
        /// The <see cref="long"/>.
        /// </returns>
        public long GetActualPosition(StreamReader reader)
        {
            // The current buffer of decoded characters
            var charBuffer =
                (char[])
                reader.GetType()
                    .InvokeMember(
                        "charBuffer", 
                        BindingFlags.DeclaredOnly | BindingFlags.NonPublic | BindingFlags.Instance
                        | BindingFlags.GetField, 
                        null, 
                        reader, 
                        null);

            // The current position in the buffer of decoded characters
            var charPos =
                (int)
                reader.GetType()
                    .InvokeMember(
                        "charPos", 
                        BindingFlags.DeclaredOnly | BindingFlags.NonPublic | BindingFlags.Instance
                        | BindingFlags.GetField, 
                        null, 
                        reader, 
                        null);

            // The number of bytes that the already-read characters need when encoded.
            int numReadBytes = reader.CurrentEncoding.GetByteCount(charBuffer, 0, charPos);

            // The number of encoded bytes that are in the current buffer
            var byteLen =
                (int)
                reader.GetType()
                    .InvokeMember(
                        "byteLen", 
                        BindingFlags.DeclaredOnly | BindingFlags.NonPublic | BindingFlags.Instance
                        | BindingFlags.GetField, 
                        null, 
                        reader, 
                        null);

            return reader.BaseStream.Position - byteLen + numReadBytes;
        }

        /// <summary>
        ///     The get index of entry.
        /// </summary>
        [DeploymentItem("SimpleLogFile2.txt")]
        [TestMethod]
        public void GetIndexOfEntry()
        {
            var entry = new DateTime(2013, 09, 05, 09, 53, 04);

            using (StreamReader sr = File.OpenText("SimpleLogFile2.txt"))
            {
                long upperIndex = 0;
                long lowwerIndex = sr.BaseStream.Length;

                DateTime logId = DateTime.Now;

                Tuple<DateTime, long> result = null;
                long index = 0;

                while (entry != logId)
                {
                    index = upperIndex + ((lowwerIndex - upperIndex) / 2);

                    result = this.GetLogIdAtIndex(sr, index);

                    logId = result.Item1;

                    if (entry < logId)
                    {
                        lowwerIndex = index;
                    }
                    else
                    {
                        upperIndex = index;
                    }
                }

                Tuple<DateTime, long> result2 = this.GetLogIdAtIndex(sr, result.Item2);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get log id at index.
        /// </summary>
        /// <param name="sr">
        /// The sr.
        /// </param>
        /// <param name="index">
        /// The index.
        /// </param>
        /// <returns>
        /// The <see cref="Tuple"/>.
        /// </returns>
        private Tuple<DateTime, long> GetLogIdAtIndex(StreamReader sr, long index)
        {
            sr.BaseStream.Position = index;
            sr.DiscardBufferedData();

            Match match;

            long tempPos = 0;

            do
            {
                tempPos = this.GetActualPosition(sr);
                match = Regex.Match(sr.ReadLine(), @"(?'Group'\d{4}\-\d{2}\-\d{2}\s\d{2}\:\d{2}\:\d{2})\s\-");
            }
            while (!match.Success);

            return new Tuple<DateTime, long>(DateTime.Parse(match.Groups["Group"].Value), tempPos);
        }

        #endregion
    }
}