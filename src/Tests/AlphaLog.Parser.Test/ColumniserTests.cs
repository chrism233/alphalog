﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ColumniserTests.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The columniser tests.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Parser.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    ///     The columniser tests.
    /// </summary>
    [TestClass]
    public class ColumniserTests
    {
        #region Public Methods and Operators

        /// <summary>
        ///     The basic test.
        /// </summary>
        [TestMethod]
        [DeploymentItem("Example.log")]
        public void BasicTest()
        {
            string sample = string.Empty;

            // var parser = new LogParser();

            // IEnumerable<string> entries = parser.ParseFile("Example.log");

            // IColumniser columniser = new RegexColumniser(entries.First());

            // IEnumerable<Dictionary<string, object>> result = columniser.ColumniseEntries(entries);
        }

        #endregion
    }
}