﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LogFileDetailsTests.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The log file details tests.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Parser.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    ///     The log file details tests.
    /// </summary>
    [TestClass]
    public class LogFileDetailsTests
    {
        /*
         * Test scenario
         * 
         *  File1.log  Start:01/01/2013 10:11 End:01/01/2013 10:20
         *  File2.log  Start:01/01/2013 10:21 End:01/01/2013 10:30
         *  File3.log  Start:01/01/2013 10:31 End:01/01/2013 10:40
         *  
         *  -- StartDate
         *  -
         *  -   -- Start File1
         *  -   -
         *  -   -
         *  -   -- End  
         *  -   -- Start File2
         *  -   -
         *  -   -
         *  -   -- End
         *  -
         *  -- EndDate 
         */
        #region Public Methods and Operators

        // <summary>
        // The faile contains end.
        // </summary>
        // <param name="endDate">
        // The end date.
        // </param>
        /// <param name="expected">
        /// The expected.
        /// </param>
        // public void FaileContainsEnd(string endDate, bool expected)
        // {
        // DateTime end = DateTime.Parse(endDate);

        // var details = new LogFileDetails(
        // "File1.log", 
        // new DateTime(2013, 01, 01, 10, 11, 00), 
        // new DateTime(2013, 01, 01, 10, 20, 00));

        // Assert.AreEqual(expected, details.FileContainsDate(end));
        // }

        /// <summary>
        /// The file contains end test.
        /// </summary>
        // [TestMethod]
        // public void FileContainsEndTest()
        // {
        // this.FaileContainsEnd("1/1/2013 10:15", true);
        // this.FaileContainsEnd("1/1/2013 10:25", false);
        // }

        /// <summary>
        /// The file contains start.
        /// </summary>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="expected">
        /// The expected.
        /// </param>
        // public void FileContainsStart(string startDate, bool expected)
        // {
        // DateTime start = DateTime.Parse(startDate);

        // var details = new LogFileDetails(
        // "File1.log", 
        // new DateTime(2013, 01, 01, 10, 11, 00), 
        // new DateTime(2013, 01, 01, 10, 20, 00));

        // Assert.AreEqual(expected, details.FileContainsDate(start));
        // }

        /// <summary>
        /// The file contains start test.
        /// </summary>
        // [TestMethod]
        // public void FileContainsStartTest()
        // {
        // this.FileContainsStart("1/1/2013 10:15", true);
        // this.FileContainsStart("1/1/2013 10:25", false);
        // }

        /// <summary>
        /// The is f ile in scope test.
        /// </summary>
        // [TestMethod]
        // public void IsFIleInScopeTest()
        // {
        // this.IsFileInScope("File1.log", "1/1/2013 10:10", "1/1/2013 10:50", true);
        // this.IsFileInScope("File2.log", "1/1/2013 10:10", "1/1/2013 10:25", true);
        // this.IsFileInScope("File3.log", "1/1/2013 10:10", "1/1/2013 10:25", false);
        // }

        /// <summary>
        /// The is file in scope.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <param name="expected">
        /// The expected.
        /// </param>
        // public void IsFileInScope(string file, string startDate, string endDate, bool expected)
        // {
        // DateTime start = DateTime.Parse(startDate);
        // DateTime end = DateTime.Parse(endDate);

        // var f = new MockFactory(MockBehavior.Default);

        // Mock<ILogParser> parser = f.Create<ILogParser>();

        // var logFileDetails = new Dictionary<string, LogFileDetails>();

        // logFileDetails.Add(
        // "File1.log", 
        // new LogFileDetails(
        // "File1.log", 
        // new DateTime(2013, 01, 01, 10, 11, 00), 
        // new DateTime(2013, 01, 01, 10, 20, 00)));

        // logFileDetails.Add(
        // "File2.log", 
        // new LogFileDetails(
        // "File2.log", 
        // new DateTime(2013, 01, 01, 10, 21, 00), 
        // new DateTime(2013, 01, 01, 10, 30, 00)));

        // logFileDetails.Add(
        // "File3.log", 
        // new LogFileDetails(
        // "File3.log", 
        // new DateTime(2013, 01, 01, 10, 31, 00), 
        // new DateTime(2013, 01, 01, 10, 40, 00)));

        // parser.Setup(p => p.GetLogFileDetails("File1.log")).Returns(logFileDetails["File1.log"]);
        // parser.Setup(p => p.GetLogFileDetails("File2.log")).Returns(logFileDetails["File2.log"]);
        // parser.Setup(p => p.GetLogFileDetails("File3.log")).Returns(logFileDetails["File3.log"]);

        // Assert.AreEqual(expected, logFileDetails[file].FileIsInScope(start, end));
        // }
        #endregion
    }
}