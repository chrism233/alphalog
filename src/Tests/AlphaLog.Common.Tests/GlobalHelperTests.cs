﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GlobalHelperTests.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The global helper tests.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Common.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    ///     The global helper tests.
    /// </summary>
    [TestClass]
    public class GlobalHelperTests
    {
        #region Public Methods and Operators

        /// <summary>
        ///     The can save column items.
        /// </summary>
        [TestMethod]
        public void CanSaveColumnItems()
        {
            // IList<ColumnItem> items = new List<ColumnItem>();

            // items.Add(new ColumnItem("Name", null, null));

            // bool result = GlobalHelper.SaveColumnItems(items, GlobalHelper.COLUMN_ITEMS_PATH);

            // Assert.IsTrue(result);

            // IList<ColumnItem> itemsReturned = GlobalHelper.ParseColumnItems(GlobalHelper.COLUMN_ITEMS_PATH);
        }

        #endregion
    }
}