﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LogDescriptorTests.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The log descriptor tests.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Common.Tests
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    using AlphaLog.UI.Common;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    ///     The log descriptor tests.
    /// </summary>
    [TestClass]
    public class LogDescriptorTests
    {
        #region Public Methods and Operators

        /// <summary>
        ///     The can get type.
        /// </summary>
        [TestMethod]
        public void CanGetCount()
        {
            var descriptor =
                new LogDescriptorCollection(
                    new Dictionary<string, object> { { "Name", "Chris" }, { "DateTime", DateTime.Now } });

            PropertyDescriptorCollection temp = descriptor.GetProperties();

            Assert.AreEqual(2, temp.Count);
        }

        #endregion
    }
}