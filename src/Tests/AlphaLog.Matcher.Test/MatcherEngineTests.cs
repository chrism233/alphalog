﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatcherEngineTests.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The matcher engine tests.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Matcher.Tests
{
    using System;
    using System.Collections.Generic;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    ///     The matcher engine tests.
    /// </summary>
    [TestClass]
    public class MatcherEngineTests
    {
        #region Fields

        /// <summary>
        ///     The m_record.
        /// </summary>
        private Dictionary<string, object> record;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The numeric matchers_ stubbed regord_ finds match.
        /// </summary>
        /// <param name="matchType">
        /// The match type.
        /// </param>
        /// <param name="searchValue">
        /// The search value.
        /// </param>
        /// <param name="searchField">
        /// The search field.
        /// </param>
        public void NumericMatchers_StubbedRegord_FindsMatch(string matchType, string searchValue, string searchField)
        {
            var matchService = new MatchService();
            var matcherType = (MatcherType)Enum.Parse(typeof(MatcherType), matchType);

            matchService.AddMatcher(matcherType, searchValue, searchField);

            Assert.IsTrue(matchService.IsMatch(this.record));
        }

        /// <summary>
        ///     The numeric matchers_ stubbed regord_ finds match test.
        /// </summary>
        [TestMethod]
        public void NumericMatchers_StubbedRegord_FindsMatchTest()
        {
            this.NumericMatchers_StubbedRegord_FindsMatch("EqualTo", "32", "Numeric");
            this.NumericMatchers_StubbedRegord_FindsMatch("GreaterThan", "5", "Numeric");
            this.NumericMatchers_StubbedRegord_FindsMatch("LessThan", "300", "Numeric");

            // NumericMatchers_StubbedRegord_FindsMatch("GreaterThanOrEqualTo", "5", "Numeric");
            // NumericMatchers_StubbedRegord_FindsMatch("LessThanOrEqualTo", "500", "Numeric");
        }

        /// <summary>
        /// The numeric matchers_ stubbed regord_ no match.
        /// </summary>
        /// <param name="matchType">
        /// The match type.
        /// </param>
        /// <param name="searchValue">
        /// The search value.
        /// </param>
        /// <param name="searchField">
        /// The search field.
        /// </param>
        public void NumericMatchers_StubbedRegord_NoMatch(string matchType, string searchValue, string searchField)
        {
            var matchService = new MatchService();
            var matcherType = (MatcherType)Enum.Parse(typeof(MatcherType), matchType);

            matchService.AddMatcher(matcherType, searchValue, searchField);

            Assert.IsFalse(matchService.IsMatch(this.record));
        }

        /// <summary>
        ///     The numeric matchers_ stubbed regord_ no match test.
        /// </summary>
        [TestMethod]
        public void NumericMatchers_StubbedRegord_NoMatchTest()
        {
            this.NumericMatchers_StubbedRegord_NoMatch("EqualTo", "4", "Numeric");
            this.NumericMatchers_StubbedRegord_NoMatch("GreaterThan", "500", "Numeric");
            this.NumericMatchers_StubbedRegord_NoMatch("LessThan", "3", "Numeric");

            // NumericMatchers_StubbedRegord_NoMatch("GreaterThanOrEqualTo", "500", "Numeric");
            // NumericMatchers_StubbedRegord_NoMatch("LessThanOrEqualTo", "5", "Numeric");
        }

        /// <summary>
        ///     The set up.
        /// </summary>
        [TestInitialize]
        public void SetUp()
        {
            this.record = new Dictionary<string, object>();

            this.record.Add("Name", "The quick brown fox jumped over the lazy dog.");
            this.record.Add("Numeric", 32);
        }

        /// <summary>
        /// The test matchers_ stubbed record_ finds match.
        /// </summary>
        /// <param name="matchType">
        /// The match type.
        /// </param>
        /// <param name="searchValue">
        /// The search value.
        /// </param>
        /// <param name="searchField">
        /// The search field.
        /// </param>
        public void TestMatchers_StubbedRecord_FindsMatch(string matchType, string searchValue, string searchField)
        {
            var matchService = new MatchService();
            var matcherType = (MatcherType)Enum.Parse(typeof(MatcherType), matchType);

            matchService.AddMatcher(matcherType, searchValue, searchField);

            Assert.IsTrue(matchService.IsMatch(this.record));
        }

        /// <summary>
        ///     The test matchers_ stubbed record_ finds match test.
        /// </summary>
        [TestMethod]
        public void TestMatchers_StubbedRecord_FindsMatchTest()
        {
            this.TestMatchers_StubbedRecord_FindsMatch("Always", string.Empty, "Name");
            this.TestMatchers_StubbedRecord_FindsMatch("Contains", "quick brown", "Name");

            // [TestCase("ContainsCaseSensative", "Quick brown", "Name")]
            this.TestMatchers_StubbedRecord_FindsMatch("EndsWith", "lazy dog.", "Name");
            this.TestMatchers_StubbedRecord_FindsMatch("StartsWith", "The quick", "Name");
            this.TestMatchers_StubbedRecord_FindsMatch("Regex", @"\sfox\s", "Name");
            this.TestMatchers_StubbedRecord_FindsMatch("Exact", "The quick brown fox jumped over the lazy dog.", "Name");
        }

        /// <summary>
        /// The test matchers_ stubbed record_ no match.
        /// </summary>
        /// <param name="matchType">
        /// The match type.
        /// </param>
        /// <param name="searchValue">
        /// The search value.
        /// </param>
        /// <param name="searchField">
        /// The search field.
        /// </param>
        public void TestMatchers_StubbedRecord_NoMatch(string matchType, string searchValue, string searchField)
        {
            var matchService = new MatchService();
            var matcherType = (MatcherType)Enum.Parse(typeof(MatcherType), matchType);

            matchService.AddMatcher(matcherType, searchValue, searchField);

            Assert.IsFalse(matchService.IsMatch(this.record));
        }

        /// <summary>
        ///     The test matchers_ stubbed record_ no match test.
        /// </summary>
        [TestMethod]
        public void TestMatchers_StubbedRecord_NoMatchTest()
        {
            this.TestMatchers_StubbedRecord_NoMatch("Contains", "brown quick", "Name");

            // [TestCase("ContainsCaseSensative", "quick brown", "Name")]
            this.TestMatchers_StubbedRecord_NoMatch("EndsWith", "dog lazy.", "Name");
            this.TestMatchers_StubbedRecord_NoMatch("StartsWith", "quick The", "Name");
            this.TestMatchers_StubbedRecord_NoMatch("Regex", @"\spig\s", "Name");
        }

        #endregion
    }
}