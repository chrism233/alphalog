﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatcherDatabaseTests.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The matcher database tests.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Matcher.Tests.Database
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    ///     The matcher database tests.
    /// </summary>
    [TestClass]
    public class MatcherDatabaseTests
    {
        #region Public Methods and Operators

        ///// <summary>
        ///// The basic test.
        ///// </summary>
        // [DeploymentItem("Matchers.xml")]
        // [TestMethod]
        // public void BasicTest()
        // {
        // var database = new MatcherDatabase("Matchers.xml");

        // IEnumerable<MatcherItem> l = database.GetMatchers();

        // foreach (MatcherItem i in l)
        // {
        // Console.WriteLine(i);
        // }
        // }

        ///// <summary>
        ///// The basic test 2.
        ///// </summary>
        // [DeploymentItem("Matchers.xml")]
        // [TestMethod]
        // public void BasicTest2()
        // {
        // var database = new MatcherDatabase("Matchers.xml");

        // var items = new List<MatcherItem>();

        // // items.Add(new MatcherItem() {Name = "sample", RawMatcher = "Raw"});
        // // items.Add(new MatcherItem() { Name = "sample", RawMatcher = "Raw" });
        // database.Save();
        // }
        #endregion
    }
}