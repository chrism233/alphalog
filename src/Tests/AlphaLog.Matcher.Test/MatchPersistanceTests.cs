﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchPersistanceTests.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The match persistance tests.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Matcher.Tests
{
    using System.Diagnostics.CodeAnalysis;
    using System.Xml;
    using System.Xml.Linq;

    using AlphaLog.Matcher.Matchers.Group;
    using AlphaLog.Matcher.Matchers.Text;

    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Microsoft.XmlDiffPatch;

    /// <summary>
    ///     The match persistance tests.
    /// </summary>
    [TestClass]
    public class MatchPersistanceTests
    {
        #region Public Methods and Operators

        public void Example()
        {
            string expected = @"<And>
                                    <Contains searchField=""Message"" searchValue=""Hello"" caseSensitive=""false"" />
                                    <Exact searchField=""Message"" searchValue=""World"" caseSensitive=""false""/>
                                </And>";

            var expected2 = new And(
                                new ContainsMatcher("Hello", "Message", false),
                                new ExactMatcher("Hello", "Message", false)
                                );
        }

        /// <summary>
        ///     The can generate xml.
        /// </summary>
        [TestMethod]
        public void CanGenerateXML()
        {
            var matcher = new Or(
                MatchFactory.Create(MatcherType.Always), 
                new And(
                    MatchFactory.Create(MatcherType.Contains, "Hello", "Message"), 
                    MatchFactory.Create(MatcherType.Exact, "World", "Message")));

            // Sensitive 
            string expected = @"<Or>
                                  <Always />
                                  <And>
                                    <Contains searchField=""Message"" searchValue=""Hello"" caseSensitive=""false"" />
                                    <Exact searchField=""Message"" searchValue=""World"" caseSensitive=""false""/>
                                  </And>
                                </Or>";

            // http://msdn.microsoft.com/en-us/library/aa302294.aspx
            var xmldiff =
                new XmlDiff(
                    XmlDiffOptions.IgnoreChildOrder | XmlDiffOptions.IgnoreNamespaces | XmlDiffOptions.IgnorePrefixes);

            var expectedXml = new XmlDocument();
            expectedXml.LoadXml(expected);

            var result = new XmlDocument();
            result.LoadXml(matcher.Serialize().ToString());

            Assert.IsTrue(
                xmldiff.Compare(expectedXml.FirstChild, result.FirstChild), 
                "The generated XML does not match the expected result.");
        }

        /// <summary>
        ///     The can parse xml.
        /// </summary>
        [SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "searchValue")]
        [TestMethod]
        public void CanParseXML()
        {
            string expected = @"<Or>
                                  <Always />
                                  <And>
                                    <Contains searchField=""Message"" searchValue=""Hello"" caseSensitive=""false""/>
                                    <Exact searchField=""Message"" searchValue=""World"" caseSensitive=""false""/>
                                  </And>
                                </Or>";

            var matcher = new Or(XDocument.Parse(expected).Root);

            // http://msdn.microsoft.com/en-us/library/aa302294.aspx
            var xmldiff =
                new XmlDiff(
                    XmlDiffOptions.IgnoreChildOrder | XmlDiffOptions.IgnoreNamespaces | XmlDiffOptions.IgnorePrefixes);

            var expectedXml = new XmlDocument();
            expectedXml.LoadXml(expected);

            var result = new XmlDocument();
            result.LoadXml(matcher.Serialize().ToString());

            Assert.IsTrue(
                xmldiff.Compare(expectedXml.FirstChild, result.FirstChild), 
                "The generated XML does not match the expected result.");
        }

        #endregion
    }
}