﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IntegrationTests.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The integration tests.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Integration.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reactive;

    using AlphaLog.Columniser;
    using AlphaLog.Matcher;
    using AlphaLog.RegexColumniser;
    using AlphaLog.UI.Common;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    /// <summary>
    ///     The integration tests.
    /// </summary>
    [TestClass]
    public class IntegrationTests
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the context.
        /// </summary>
        private TestContext Context { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The basic integration.
        /// </summary>
        [TestMethod]
        public void BasicIntegration()
        {
            var settingsManager = new SettingsManager("AlphaLogIntegrationTestSettings.xml");

            var columniser = new RegexDescriptor(settingsManager);

            // Parse the entries 
            var parser = new AlphaLogParser(new[] { columniser });

            parser.Add(@"Example.log");

            var parsedEntries = new List<Dictionary<string, object>>();
            parser.Subscribe(parsedEntries.Add);
            parser.Update();

            // = entries.Select(e => regexColumniser.ColumnizewEntry(e));
            int expectedTotal = parsedEntries.Count();
            Console.WriteLine("Parse {0} entries in {1} ms", expectedTotal, 5);

            int totalCount = 0;
            int tempCount = 0;

            // Filter the entries
            var matchService = new MatchService();

            matchService.AddMatcher(MatcherType.Contains, "DEBUG", "Level");
            tempCount = parsedEntries.Where(matchService.IsMatch).Count();
            totalCount += tempCount;
            Console.WriteLine("Found {0} debug messages", tempCount);

            matchService.Reset();

            matchService.AddMatcher(MatcherType.Contains, "INFO", "Level");
            tempCount = parsedEntries.Where(matchService.IsMatch).Count();
            totalCount += tempCount;
            Console.WriteLine("Found {0} info messages", tempCount);

            matchService.Reset();

            matchService.AddMatcher(MatcherType.Contains, "ERROR", "Level");
            tempCount = parsedEntries.Where(matchService.IsMatch).Count();
            totalCount += tempCount;
            Console.WriteLine("Found {0} info messages", tempCount);

            matchService.Reset();

            matchService.AddMatcher(MatcherType.Contains, "WARN", "Level");
            tempCount = parsedEntries.Where(matchService.IsMatch).Count();
            totalCount += tempCount;
            Console.WriteLine("Found {0} warn messages", tempCount);

            matchService.Reset();

            matchService.AddMatcher(MatcherType.Contains, "VERBOSE", "Level");
            tempCount = parsedEntries.Where(matchService.IsMatch).Count();
            totalCount += tempCount;
            Console.WriteLine("Found {0} verbose messages", tempCount);

            Assert.AreEqual(expectedTotal, totalCount);
        }

        /// <summary>
        /// The conflictig columnisers_ results in error descriptor.
        /// </summary>
        [TestMethod]
        public void ConflictigColumnisers_ResultsInErrorDescriptor()
        {
            var columniser1 = new Mock<IColumnizer>();
            columniser1.Setup(m => m.GetColumns()).Returns(new Dictionary<string, Type> { { "Thread", typeof(string) } });
            var columniser2 = new Mock<IColumnizer>();
            columniser2.Setup(m => m.GetColumns()).Returns(new Dictionary<string, Type> { { "Thread", typeof(int) } });

            var mockDescriptor = new Mock<IColumnizerDescriptor>();
            mockDescriptor.Setup(m => m.CanColumnize(It.IsAny<string>())).Returns(true);
            mockDescriptor.Setup(m => m.Create("file1.txt")).Returns(columniser1.Object);
            mockDescriptor.Setup(m => m.Create("file2.txt")).Returns(columniser2.Object);

            // Parse the entries 
            var parser = new AlphaLogParser(new[] { mockDescriptor.Object });

            ProgressInfo latestInfo = null;
            parser.Progress.Subscribe(Observer.Create<ProgressInfo>(p => latestInfo = p));

            Details file1Result = parser.Add("file1.txt");
            Details file2Result = parser.Add("file2.txt");

            Assert.IsInstanceOfType(file1Result, typeof(Details));
            Assert.IsInstanceOfType(file2Result, typeof(ErrorDetails));

            Assert.AreEqual(latestInfo.State, ProcessState.Error);

            parser.Remove("file2.txt");

            Assert.AreEqual(latestInfo.State, ProcessState.None);
        }

        #endregion
    }
}