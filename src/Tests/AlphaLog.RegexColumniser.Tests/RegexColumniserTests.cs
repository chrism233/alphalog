﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RegexColumniserTests.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The regex columniser tests.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.RegexColumniser.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    ///     The regex columniser tests.
    /// </summary>
    [TestClass]
    public class RegexColumniserTests
    {
        #region Constants

        /// <summary>
        ///     The m_sample.
        /// </summary>
        private const string Sample =
            "2013-09-05 09:53:28,240 [Start DataTransferApplication:DataTransferApplicationAlphaLog 5] INFO  Framework.Core.Logging.LogManager [Director Init] - Logging.LogLevel DEBUG  Debug";

        #endregion

        ///// <summary>
        ///// The car parse_log 4 netentry_ success.
        ///// </summary>
        // [TestMethod]
        // public void CarParse_log4netentry_Success()
        // {
        // var regexColumniser = new RegexColumniser();

        // Assert.IsTrue(regexColumniser.CanColumnizerFile(Sample));
        // }

        ///// <summary>
        ///// The parse entry.
        ///// </summary>
        // [TestMethod]
        // public void ParseEntry()
        // {
        // var regexColumniser = new RegexColumniser();

        // Dictionary<string, object> result = regexColumniser.ColumnizewEntry(Sample);

        // Assert.IsTrue(result.ContainsKey("TimeStamp"));
        // Assert.IsTrue(result.ContainsKey("Thread"));
        // Assert.IsTrue(result.ContainsKey("Level"));
        // Assert.IsTrue(result.ContainsKey("Logger"));
        // Assert.IsTrue(result.ContainsKey("Context"));
        // Assert.IsTrue(result.ContainsKey("Message"));
        // }

        ///// <summary>
        /////     Ensures that default regular expressions are mutually exclusive.
        ///// </summary>
        // [TestMethod]
        // public void TestRegularExpressionsAreMutuallyExclusive()
        // {
        // var regexDatabase = new RegexDatabase();

        // int matchingRegexCount = 0;

        // foreach (RegexItem regex in regexDatabase.GetRegularExpressions())
        // {
        // if (regex.Regex.IsMatch(Sample))
        // {
        // matchingRegexCount++;
        // }
        // }

        // Assert.AreEqual(1, matchingRegexCount);
        // }
    }
}