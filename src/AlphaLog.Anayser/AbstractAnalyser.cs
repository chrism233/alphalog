﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AbstractAnalyser.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The abstract analyser.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Analyser
{
    using AlphaLog.UI.Common;

    /// <summary>
    ///     The abstract analyser.
    /// </summary>
    public abstract class AbstractAnalyser
    {
        #region Public Properties

        /// <summary>
        ///     Gets the analyser name.
        /// </summary>
        public abstract string AnalyserName { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The is match.
        /// </summary>
        /// <param name="logItem">
        /// The log item.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public abstract bool IsMatch(LogDescriptorCollection logItem);

        #endregion
    }
}