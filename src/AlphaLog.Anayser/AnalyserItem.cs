﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AnalyserItem.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The analyser item.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Analyser
{
    using Microsoft.Practices.Prism.Mvvm;

    /// <summary>
    ///     The analyser item.
    /// </summary>
    public class AnalyserItem : BindableBase
    {
        #region Fields

        /// <summary>
        ///     The _analyser.
        /// </summary>
        private AbstractAnalyser analyser;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AnalyserItem"/> class.
        /// </summary>
        /// <param name="createInstance">
        /// The analyser instance.
        /// </param>
        public AnalyserItem(AbstractAnalyser createInstance)
        {
            this.Analyser = createInstance;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the analyser instance.
        /// </summary>
        public AbstractAnalyser Analyser
        {
            get
            {
                return this.analyser;
            }

            set
            {
                if (this.analyser != value)
                {
                    this.SetProperty(ref this.analyser, value);
                }
            }
        }

        #endregion
    }
}