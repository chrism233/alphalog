﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FilteringDataGrid.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   A grid that makes inline filtering possible.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Filtering
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;

    using AlphaLog.Matcher;
    using AlphaLog.Matcher.Matchers;
    using AlphaLog.Matcher.Matchers.Group;
    using AlphaLog.UI.Common;
    using AlphaLog.UI.Filtering.ViewModels;

    /// <summary>
    ///     A grid that makes inline filtering possible.
    /// </summary>
    public class FilteringDataGrid : DataGrid
    {
        #region Fields

        /// <summary>
        ///     The matcher.
        /// </summary>
        private IMatch matcher;

        /// <summary>
        ///     The view.
        /// </summary>
        private ICollectionView view;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes static members of the <see cref="FilteringDataGrid" /> class.
        /// </summary>
        static FilteringDataGrid()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(FilteringDataGrid), 
                new FrameworkPropertyMetadata(typeof(FilteringDataGrid)));
        }

        #endregion

        private static readonly DependencyProperty DateTimeFormatProperty = DependencyProperty.Register(
            "DateTimeFormat",
            typeof(string),
            typeof(FilteringDataGrid),
            new PropertyMetadata("dd/MM/yyyy hh:mm:ss.fff"));

        /// <summary>
        /// Gets or sets the datetime format string.
        /// </summary>
        public string DateTimeFormat
        {
            get
            {
                return GetValue(DateTimeFormatProperty) as string;
            }

            set
            {
                this.SetValue(DateTimeFormatProperty, value);
            }
        }

        #region Methods

        /// <summary>
        /// The on auto generated columns.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnAutoGeneratedColumns(EventArgs e)
        {
            this.view = CollectionViewSource.GetDefaultView(this.ItemsSource);
            this.view.Filter = this.ApplyFilter;

            base.OnAutoGeneratedColumns(e);
        }

        /// <summary>
        /// The on auto generating column.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnAutoGeneratingColumn(DataGridAutoGeneratingColumnEventArgs e)
        {
            ColumnHeaderVM columnHeader = null;
            if (e.PropertyType == typeof(DateTime))
            {
                columnHeader = new DateTimeColumnHeaderVM(e.PropertyName, this.Columns);
                var c = e.Column as DataGridTextColumn;
                c.Binding.StringFormat = this.DateTimeFormat;
            }
            else if (e.PropertyType == typeof(string))
            {
                columnHeader = new StringColumnHeaderVM(e.PropertyName, this.Columns);
            }
            else
            {
                columnHeader = new NumericColumnHeaderVM(e.PropertyName, this.Columns);
            }

            columnHeader.MatcherUpdatedEvent += this.MatcherUpdatedEvent;
            e.Column.Header = columnHeader;

            base.OnAutoGeneratingColumn(e);
        }

        /// <summary>
        /// The apply filter.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool ApplyFilter(object obj)
        {
            Dictionary<string, object> items = ((LogDescriptorCollection)obj).Items;

            return this.matcher == null || !this.matcher.CanMatch(items) || this.matcher.IsMatch(items);
        }

        /// <summary>
        /// The matcher updated event.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void MatcherUpdatedEvent(object sender, MatcherUpdatedEventArgs e)
        {
            if (this.view != null)
            {
                if (this.Columns.Count == 0)
                {
                    this.matcher = new AlwaysMatcher();
                }
                else
                {
                    this.matcher = new And(this.Columns.Select(h => h.Header).Cast<ColumnHeaderVM>().Select(c => c.Matcher).ToArray());
                }

                this.view.Refresh();
            }
        }

        #endregion
    }
}