// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatcherUpdatedEventArgs.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The matcher updated event args.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AlphaLog.UI.Filtering.ViewModels
{
    using System;

    using AlphaLog.Matcher;

    /// <summary>
    ///     The matcher updated event args.
    /// </summary>
    public class MatcherUpdatedEventArgs : EventArgs
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MatcherUpdatedEventArgs"/> class.
        /// </summary>
        /// <param name="searchText">
        /// The search text.
        /// </param>
        /// <param name="matcher">
        /// The matcher.
        /// </param>
        public MatcherUpdatedEventArgs(string searchText, IMatch matcher)
        {
            this.SearchText = searchText;
            this.Matcher = matcher;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the matcher.
        /// </summary>
        public IMatch Matcher { get; private set; }

        /// <summary>
        ///     Gets the search text.
        /// </summary>
        public string SearchText { get; private set; }

        #endregion
    }
}