// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ColumnHeaderVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The column header context.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Filtering.ViewModels
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Xml.Linq;

    using AlphaLog.Matcher;
    using AlphaLog.Matcher.Matchers;
    using AlphaLog.UI.Common.Models;

    using Microsoft.Practices.Prism.Commands;
    using Microsoft.Practices.Prism.Mvvm;

    /// <summary>
    ///     The column header context.
    /// </summary>
    public abstract class ColumnHeaderVM : BindableBase, INotifyDataErrorInfo
    {
        #region Fields

        /// <summary>
        ///     The errors.
        /// </summary>
        protected readonly Dictionary<string, List<string>> ErrorsCollection = new Dictionary<string, List<string>>();

        /// <summary>
        ///     The filtering text box.
        /// </summary>
        private string filteringTextBox = string.Empty;

        /// <summary>
        ///     The matcher.
        /// </summary>
        private IMatch matcher;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ColumnHeaderVM"/> class.
        /// </summary>
        /// <param name="propertyName">
        /// The property Name.
        /// </param>
        /// <param name="columns">
        /// The columns.
        /// </param>
        public ColumnHeaderVM(string propertyName, ObservableCollection<DataGridColumn> columns)
        {
            this.ShowHideColumnCommand = new DelegateCommand<string>(this.ShowHideColumn);
            this.Title = propertyName;
            this.Columns = columns;
            this.Matcher = new AlwaysMatcher();
        }

        #endregion

        #region Public Events

        /// <summary>
        ///     The errors changed.
        /// </summary>
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        /// <summary>
        ///     The matcher updated event.
        /// </summary>
        public event EventHandler<MatcherUpdatedEventArgs> MatcherUpdatedEvent;

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the filtering data grid.
        /// </summary>
        public ObservableCollection<DataGridColumn> Columns { get; set; }

        /// <summary>
        ///     Gets or sets the data type.
        /// </summary>
        public DataType DataType { get; set; }

        /// <summary>
        ///     Gets or sets the Filtering text box content.
        /// </summary>
        public string FilteringTextBox
        {
            get
            {
                return this.filteringTextBox;
            }

            set
            {
                this.SetProperty(ref this.filteringTextBox, value);
                this.UpdateMatcher();
                this.Validate();
            }
        }

        /// <summary>
        ///     Gets a value indicating whether has errors.
        /// </summary>
        public bool HasErrors
        {
            get
            {
                return this.ErrorsCollection.Values.FirstOrDefault(l => l.Count > 0) != null;
            }
        }

        /// <summary>
        ///     Gets or sets the <see cref="IMatch" />
        /// </summary>
        public IMatch Matcher
        {
            get
            {
                return this.matcher;
            }

            protected set
            {
                this.SetProperty(ref this.matcher, value);
                this.OnMatcherUpdatedEvent(new MatcherUpdatedEventArgs(this.Title, this.matcher));
            }
        }

        /// <summary>
        ///     Gets or sets the show hide column command.
        /// </summary>
        public ICommand ShowHideColumnCommand { get; protected set; }

        /// <summary>
        ///     Gets or sets the title.
        /// </summary>
        public string Title { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Deserialise the column header
        /// </summary>
        /// <param name="columnHeader">
        /// The column header
        /// </param>
        public abstract void Deserialize(XElement columnHeader);

        /// <summary>
        /// The get errors.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable GetErrors(string propertyName)
        {
            if (propertyName == null)
            {
                return null;
            }

            List<string> errorsForName;
            this.ErrorsCollection.TryGetValue(propertyName, out errorsForName);
            return errorsForName;
        }

        /// <summary>
        /// The on errors changed.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        public void OnErrorsChanged(string propertyName)
        {
            EventHandler<DataErrorsChangedEventArgs> handler = this.ErrorsChanged;
            if (handler == null)
            {
                return;
            }

            var arg = new DataErrorsChangedEventArgs(propertyName);
            handler.Invoke(this, arg);
        }

        /// <summary>
        ///     The serialize.
        /// </summary>
        /// <returns>
        ///     The <see cref="XElement" />.
        /// </returns>
        public abstract XElement Serialize();

        #endregion

        #region Methods

        /// <summary>
        /// The on matcher updated event.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected virtual void OnMatcherUpdatedEvent(MatcherUpdatedEventArgs e)
        {
            EventHandler<MatcherUpdatedEventArgs> handler = this.MatcherUpdatedEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        ///     The update matcher.
        /// </summary>
        protected abstract void UpdateMatcher();

        /// <summary>
        ///     The validate.
        /// </summary>
        protected abstract void Validate();

        /// <summary>
        /// The show hide column.
        /// </summary>
        /// <param name="title">
        /// The title.
        /// </param>
        private void ShowHideColumn(string title)
        {
            DataGridColumn dataGridColumn = this.Columns.FirstOrDefault(c => ((ColumnHeaderVM)c.Header).Title == title);
            if (dataGridColumn != null)
            {
                dataGridColumn.Visibility = dataGridColumn.Visibility == Visibility.Hidden
                                                ? Visibility.Visible
                                                : Visibility.Hidden;
            }
        }

        #endregion
    }
}