// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringColumnHeaderVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The string column header vm.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Filtering.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Controls;
    using System.Xml.Linq;

    using AlphaLog.Matcher;
    using AlphaLog.Matcher.Matchers;

    /// <summary>
    ///     The string column header vm.
    /// </summary>
    public class StringColumnHeaderVM : ColumnHeaderVM
    {
        #region Fields

        /// <summary>
        ///     The casesensative.
        /// </summary>
        private bool casesensative;

        /// <summary>
        ///     The selected search type.
        /// </summary>
        private MatcherType selectedSearchType;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="StringColumnHeaderVM"/> class.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        /// <param name="columns">
        /// The columns.
        /// </param>
        public StringColumnHeaderVM(string propertyName, ObservableCollection<DataGridColumn> columns)
            : base(propertyName, columns)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets a value indicating whether sensitivity should be applied.
        /// </summary>
        public bool CaseSensative
        {
            get
            {
                return this.casesensative;
            }

            set
            {
                this.SetProperty(ref this.casesensative, value);
                this.UpdateMatcher();
            }
        }

        /// <summary>
        ///     Gets the search types.
        /// </summary>
        public ObservableCollection<MatcherType> SearchTypes
        {
            get
            {
                return
                    new ObservableCollection<MatcherType>(
                        Enum.GetValues(typeof(MatcherType))
                            .Cast<MatcherType>()
                            .Where(item => MatcherHelpers.MatcherTypeToCategory(item) == MatcherCategory.Text)
                            .ToList());
            }
        }

        /// <summary>
        ///     Gets or sets the selected search type.
        /// </summary>
        public MatcherType SelectedSearchType
        {
            get
            {
                return this.selectedSearchType;
            }

            set
            {
                this.SetProperty(ref this.selectedSearchType, value);
                this.UpdateMatcher();
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The deserialize.
        /// </summary>
        /// <param name="columnHeader">
        /// The column header.
        /// </param>
        public override void Deserialize(XElement columnHeader)
        {
            try
            {
                this.SelectedSearchType =
                    (MatcherType)Enum.Parse(typeof(MatcherType), columnHeader.Attribute("matcher").Value);
                this.FilteringTextBox = columnHeader.Attribute("searchValue").Value;
                this.CaseSensative = bool.Parse(columnHeader.Attribute("caseSensativity").Value);
            }
            catch
            {
            }
        }

        /// <summary>
        ///     The serialize.
        /// </summary>
        /// <returns>
        ///     The <see cref="XElement" />.
        /// </returns>
        public override XElement Serialize()
        {
            return new XElement(
                "ColumnHeader", 
                new XAttribute("matcher", this.SelectedSearchType), 
                new XAttribute(
                    "searchValue", 
                    string.IsNullOrEmpty(this.FilteringTextBox) ? string.Empty : this.FilteringTextBox), 
                new XAttribute("caseSensativity", this.CaseSensative));
        }

        #endregion

        #region Methods

        /// <summary>
        ///     The update matcher.
        /// </summary>
        protected override void UpdateMatcher()
        {
            if (string.IsNullOrEmpty(this.FilteringTextBox))
            {
                this.Matcher = new AlwaysMatcher();
            }
            else
            {
                this.Matcher = MatchFactory.Create(
                    this.SelectedSearchType, 
                    this.FilteringTextBox, 
                    this.Title, 
                    this.CaseSensative);
            }
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        protected override void Validate()
        {
            // if (selectedSearchType == MatcherType.Regex)
            // {
            // Validate Name
            List<string> errorsForName;
            if (!this.ErrorsCollection.TryGetValue("FilteringTextBox", out errorsForName))
            {
                errorsForName = new List<string>();
            }
            else
            {
                errorsForName.Clear();
            }

            this.ErrorsCollection["FilteringTextBox"] = errorsForName;

            if (errorsForName.Count > 0)
            {
                this.OnErrorsChanged("FilteringTextBox");
            }

            // }
        }

        #endregion
    }
}