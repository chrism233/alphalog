// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataTimeColumnHeaderVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The date time column header vm.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Filtering.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Text.RegularExpressions;
    using System.Windows.Controls;
    using System.Xml.Linq;

    using AlphaLog.Matcher;
    using AlphaLog.Matcher.Matchers;
    using AlphaLog.Matcher.Matchers.Group;

    /// <summary>
    ///     The date time column header vm.
    /// </summary>
    public class DateTimeColumnHeaderVM : ColumnHeaderVM
    {
        #region Fields

        /// <summary>
        ///     The validate.
        /// </summary>
        private readonly Regex validate = new Regex(@"(?x) # ignore pattern whitespace
^(?n) # Captures only explicit groups
(?<From>\d{2}[/-]\d{2}[/-]\d{4}(\s\d{1,2}\:\d{1,2}\:\d{1,2}(\.\d{3})?)?)
(?<Operator>\s+[-><])
(?<To>\s+\d{2}[/-]\d{2}[/-]\d{4}(\s\d{1,2}\:\d{1,2}\:\d{1,2}(\.\d{3})?)?)?");

        /// <summary>
        ///     The errors.
        /// </summary>
        private string errors = string.Empty;

        /// <summary>
        ///     The from date time.
        /// </summary>
        private DateTime fromDateTime;

        /// <summary>
        ///     The from is enabled.
        /// </summary>
        private bool fromIsEnabled;

        /// <summary>
        ///     The has erros.
        /// </summary>
        private bool hasErros;

        /// <summary>
        ///     The previous.
        /// </summary>
        private string previous = string.Empty;

        /// <summary>
        ///     The to date time.
        /// </summary>
        private DateTime toDateTime;

        /// <summary>
        ///     The to is enabled.
        /// </summary>
        private bool toIsEnabled;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DateTimeColumnHeaderVM"/> class.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        /// <param name="columns">
        /// The columns.
        /// </param>
        public DateTimeColumnHeaderVM(string propertyName, ObservableCollection<DataGridColumn> columns)
            : base(propertyName, columns)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the from date time.
        /// </summary>
        public DateTime FromDateTime
        {
            get
            {
                return this.fromDateTime;
            }

            set
            {
                this.SetProperty(ref this.fromDateTime, value);
                this.UpdateMatcher();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether from is enabled.
        /// </summary>
        public bool FromIsEnabled
        {
            get
            {
                return this.fromIsEnabled;
            }

            set
            {
                this.SetProperty(ref this.fromIsEnabled, value);
                this.UpdateMatcher();
            }
        }

        /// <summary>
        ///     Gets or sets the to date time.
        /// </summary>
        public DateTime ToDateTime
        {
            get
            {
                return this.toDateTime;
            }

            set
            {
                this.SetProperty(ref this.toDateTime, value);
                this.UpdateMatcher();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether to is enabled.
        /// </summary>
        public bool ToIsEnabled
        {
            get
            {
                return this.toIsEnabled;
            }

            set
            {
                this.SetProperty(ref this.toIsEnabled, value);
                this.UpdateMatcher();
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Deserialise the column header
        /// </summary>
        /// <param name="columnHeader">
        /// The column header.
        /// </param>
        public override void Deserialize(XElement columnHeader)
        {
        }

        /// <summary>
        ///     The serialize.
        /// </summary>
        /// <returns>
        ///     The <see cref="XElement" />.
        /// </returns>
        public override XElement Serialize()
        {
            return new XElement(
                "ColumnHeader", 
                new XAttribute("fromDate", this.FromDateTime), 
                new XAttribute("fromEnabled", this.FromIsEnabled), 
                new XAttribute("toDate", this.ToDateTime), 
                new XAttribute("toEnabled", this.ToIsEnabled), 
                new XAttribute("searchValue", this.FilteringTextBox));
        }

        #endregion

        #region Methods

        /// <summary>
        ///     The update matcher.
        /// </summary>
        protected override void UpdateMatcher()
        {
            this.Refresh();

            IMatch greaterMatcher = MatchFactory.Create(
                MatcherType.GreaterThan, 
                this.FromDateTime.ToUnixTime().ToString(CultureInfo.InvariantCulture), 
                this.Title);
            IMatch lessMatcher = MatchFactory.Create(
                MatcherType.LessThan, 
                this.ToDateTime.ToUnixTime().ToString(CultureInfo.InvariantCulture), 
                this.Title);
            var and = new And();

            if (!this.toIsEnabled && !this.fromIsEnabled)
            {
                and.Add(new AlwaysMatcher());
            }

            if (this.toIsEnabled)
            {
                and.Add(lessMatcher);
            }

            if (this.fromIsEnabled)
            {
                and.Add(greaterMatcher);
            }

            this.Matcher = and;
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        protected override void Validate()
        {
            this.Refresh();

            // Validate Name
            List<string> errorsForName;
            if (!this.ErrorsCollection.TryGetValue("FilteringTextBox", out errorsForName))
            {
                errorsForName = new List<string>();
            }
            else
            {
                errorsForName.Clear();
            }

            if (this.hasErros)
            {
                errorsForName.Add(this.errors);
            }

            this.ErrorsCollection["FilteringTextBox"] = errorsForName;

            if (errorsForName.Count > 0)
            {
                this.OnErrorsChanged("FilteringTextBox");
            }
        }

        /// <summary>
        ///     The refresh.
        /// </summary>
        private void Refresh()
        {
            if (this.FilteringTextBox == string.Empty)
            {
                this.hasErros = false;
                this.errors = string.Empty;
            }

            if (this.FilteringTextBox != this.previous)
            {
                this.hasErros = false;
                this.previous = this.FilteringTextBox;
                Match match = this.validate.Match(this.FilteringTextBox);
                if (match.Success)
                {
                    string from = match.Groups["From"].Value;
                    string @operator = match.Groups["Operator"].Value.Trim();
                    string to = match.Groups["To"].Value;

                    if (@operator == ">")
                    {
                        this.FromIsEnabled = true;
                        this.ToIsEnabled = false;
                        this.FromDateTime = DateTime.Parse(from);
                    }

                    if (@operator == "<")
                    {
                        this.FromIsEnabled = false;
                        this.ToIsEnabled = true;
                        this.ToDateTime = DateTime.Parse(from);
                    }

                    if (@operator == "-")
                    {
                        DateTime toResult;
                        if (DateTime.TryParse(to, out toResult))
                        {
                            this.FromIsEnabled = true;
                            this.ToIsEnabled = true;
                            this.FromDateTime = DateTime.Parse(from);
                            this.ToDateTime = toResult;
                        }
                        else
                        {
                            this.errors = "Invalid format: must specify to date when using the - operator";
                            this.hasErros = true;
                        }
                    }
                }
                else
                {
                    this.errors = "Invalid format: dd/MM/yyyy hh:mm:ss.fff -,<,> dd/MM/yyyy hh:mm:ss.fff";
                    this.hasErros = true;
                }
            }
        }

        #endregion
    }
}