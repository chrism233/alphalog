﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaLog.UI.Filtering
{
    public class HeaderContext
    {
        public string Title { get; set; }
        public FilteringDataGrid FilteringDataGrid { get; set; }
    }
}
