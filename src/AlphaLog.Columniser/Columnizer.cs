﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Columnizer.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The columnizer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Columniser
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Reactive.Subjects;

    /// <summary>
    ///     The columnizer.
    /// </summary>
    public abstract class Columnizer : IColumnizer
    {
        #region Fields

        /// <summary>
        ///     The disposed.
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", 
            Justification = "Reviewed. Suppression is OK here.")]
        protected bool disposed;

        /// <summary>
        ///     The subscribers.
        /// </summary>
        private readonly Subject<Dictionary<string, object>> subscribers = new Subject<Dictionary<string, object>>();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Columnizer"/> class.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        public Columnizer(string file)
        {
            this.File = file;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the file.
        /// </summary>
        public string File { get; protected set; }

        /// <summary>
        ///     Gets the name.
        /// </summary>
        public abstract string Name { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The dispose.
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
            this.Dispose(true);
        }

        /// <summary>
        ///     The column name to type mappings.
        /// </summary>
        /// <returns>
        ///     A <see cref="Dictionary{String, Type}" /> mappings column names to their types.
        /// </returns>
        public abstract Dictionary<string, Type> GetColumns();

        /// <summary>
        /// The subscribe.
        /// </summary>
        /// <param name="observer">
        /// The observer.
        /// </param>
        /// <returns>
        /// The <see cref="IDisposable"/>.
        /// </returns>
        public IDisposable Subscribe(IObserver<Dictionary<string, object>> observer)
        {
            return this.subscribers.Subscribe(observer);
        }

        /// <summary>
        ///     The update.
        /// </summary>
        public void Update()
        {
            this.UpdateCore(this.subscribers);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                }

                this.disposed = true;
            }
        }

        /// <summary>
        /// The update core.
        /// </summary>
        /// <param name="observer">
        /// The observer.
        /// </param>
        protected abstract void UpdateCore(IObserver<Dictionary<string, object>> observer);

        #endregion
    }
}