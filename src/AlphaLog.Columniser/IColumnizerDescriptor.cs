﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IColumnizerDescriptor.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The ColumnizerBase interface. descriptor
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Columniser
{
    /// <summary>
    ///     The ColumnizerBase interface. descriptor
    /// </summary>
    public interface IColumnizerDescriptor
    {
        #region Public Methods and Operators

        /// <summary>
        /// Determine if this columniser can parse the entry.
        /// </summary>
        /// <param name="filePath">
        /// The file Path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool CanColumnize(string filePath);

        /// <summary>
        /// Creates a columniser for the provided file path.
        /// </summary>
        /// <param name="filePath">
        /// The file to create a matcher for.
        /// </param>
        /// <returns>
        /// The <see cref="IColumnizer"/>.
        /// </returns>
        IColumnizer Create(string filePath);

        #endregion
    }
}