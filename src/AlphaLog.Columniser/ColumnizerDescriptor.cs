﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ColumnizerDescriptor.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   Provides a base class for columnizers.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Columniser
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;

    using AlphaLog.UI.Common;

    /// <summary>
    ///     Provides a base class for columnizers.
    /// </summary>
    [Export]
    public abstract class ColumnizerDescriptor<T> : IColumnizerDescriptor where T : IPersist 
    {
        #region Fields

        /// <summary>
        /// The disposed.
        /// </summary>
        private bool disposed = false;

        /// <summary>
        /// The repository.
        /// </summary>
        private Repository<T> repository;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ColumnizerDescriptor" /> class.
        /// </summary>
        public ColumnizerDescriptor(Repository<T> repository)
        {
            this.GetItems = repository.GetItems();
            repository.Updated += RepositoryUpdated;
        }

        #endregion

        #region Properties

        public IEnumerable<T> GetItems { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Determine if this columniser can parse the entry.
        /// </summary>
        /// <param name="filePath">
        /// The file Path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public abstract bool CanColumnize(string filePath);

        /// <summary>
        /// Create a columniser for the specified <paramref name="filePath"/>.
        /// </summary>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        /// <returns>
        /// The <see cref="IColumnizer"/>.
        /// </returns>
        /// <exception cref="ObjectDisposedException">
        /// The <see cref="ColumnizerDescriptor"/> has been disposed.
        /// </exception>
        public IColumnizer Create(string filePath)
        {
            if (this.disposed)
            {
                throw new ObjectDisposedException(typeof(ColumnizerDescriptor<T>).Name);
            }

            return this.DoCreate(filePath);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The do create.
        /// </summary>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        /// <returns>
        /// The <see cref="IColumnizer"/>.
        /// </returns>
        protected abstract IColumnizer DoCreate(string filePath);

        private void RepositoryUpdated(object sender, EventArgs e)
        {
            this.GetItems = this.repository.GetItems();
        }

        #endregion
    }
}