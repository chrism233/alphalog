﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IColumnizer.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The Columnizer interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Columniser
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    ///     The Columnizer interface.
    /// </summary>
    public interface IColumnizer : IObservable<Dictionary<string, object>>, IDisposable
    {
        #region Public Properties

        /// <summary>
        ///     Gets the file.
        /// </summary>
        string File { get; }

        /// <summary>
        ///     Gets the name.
        /// </summary>
        string Name { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The get columns.
        /// </summary>
        /// <returns>
        ///     The <see cref="Dictionary" />.
        /// </returns>
        Dictionary<string, Type> GetColumns();

        /// <summary>
        ///     The update.
        /// </summary>
        void Update();

        #endregion
    }
}