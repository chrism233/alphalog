﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AddRegex.xaml.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   Interaction logic for <c ref="AddRegex.xaml" />
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.RegexColumniser.Views
{
    using System.ComponentModel.Composition;
    using System.Windows.Controls;

    using AlphaLog.RegexColumniser.ViewModels;
    using AlphaLog.UI.Common;

    using Microsoft.Practices.Prism.Regions;

    /// <summary>
    ///     Interaction logic for <c ref="AddRegex.xaml" />
    /// </summary>
    [ViewExport(RegionName = RegionNames.SettingsTab, TreePath = "Columnisers.Regex")]
    [ViewSortHint("003")]
    public partial class AddRegex : UserControl
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="AddRegex" /> class.
        /// </summary>
        public AddRegex()
        {
            this.InitializeComponent();
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the view model.
        /// </summary>
        [Import]
        public AddRegexVM ViewModel
        {
            get
            {
                return this.DataContext as AddRegexVM;
            }

            set
            {
                this.DataContext = value;
            }
        }

        #endregion
    }
}