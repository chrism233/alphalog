// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RegexDescriptor.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The regex columniser.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.RegexColumniser
{
    using System;
    using System.ComponentModel.Composition;
    using System.IO;
    using System.Text.RegularExpressions;

    using AlphaLog.Columniser;
    using AlphaLog.RegexColumniser.Model;
    using AlphaLog.UI.Common;

    /// <summary>
    ///     The regex columniser.
    /// </summary>
    [Export(typeof(IColumnizerDescriptor))]
    public class RegexDescriptor : ColumnizerDescriptor<RegexItem>
    {
        #region Fields

        /// <summary>
        ///     The disposed.
        /// </summary>
        private bool disposed;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RegexDescriptor"/> class.
        /// </summary>
        /// <param name="settingsManager">
        /// The settings Manager.
        /// </param>
        [ImportingConstructor]
        public RegexDescriptor(ISettingsManager settingsManager)
            : base (settingsManager.GetsSetting<RegexDatabase>())
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Can this columniser parse the entry.
        /// </summary>
        /// <param name="filePath">
        /// The file Path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool CanColumnize(string filePath)
        {
            bool canParse = false;

            try
            {
                this.GetValidRegex(File.ReadAllLines(filePath)[0]);
                canParse = true;
            }
            catch (InvalidOperationException)
            {
                canParse = false;
            }

            return canParse;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The do create.
        /// </summary>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        /// <returns>
        /// The <see cref="IColumnizer"/>.
        /// </returns>
        protected override IColumnizer DoCreate(string filePath)
        {
            return new RegexColumniser(this.GetValidRegex(File.ReadAllLines(filePath)[0]), filePath);
        }

        /// <summary>
        /// Get a valid regular expression for the selected file.
        /// </summary>
        /// <param name="sampleLogEntry">
        /// A sample log entry.
        /// </param>
        /// <returns>
        /// The <see cref="Regex"/>.
        /// </returns>
        private RegexItem GetValidRegex(string sampleLogEntry)
        {
            foreach (RegexItem reg in this.GetItems)
            {
                if (reg.Regex.IsMatch(sampleLogEntry))
                {
                    return reg;
                }
            }

            throw new InvalidOperationException("Could not find a valid regular expression for the selected file type");
        }

        #endregion
    }
}