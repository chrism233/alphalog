﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LogParser.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The log parser.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.RegexColumniser.Parser
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Text.RegularExpressions;

    /// <summary>
    ///     The log parser.
    /// </summary>
    public class LogParser : IDisposable
    {
        #region Fields

        /// <summary>
        ///     The current regex.
        /// </summary>
        private readonly Regex currentRegex;

        /// <summary>
        ///     The stream reader.
        /// </summary>
        private readonly StreamReader streamReader;

        /// <summary>
        ///     The disposed.
        /// </summary>
        private bool disposed;

        /// <summary>
        ///     The file to parse.
        /// </summary>
        private string fileToParse;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LogParser"/> class.
        /// </summary>
        /// <param name="fileToParse">
        /// The file to parse.
        /// </param>
        /// <param name="currentRegex">
        /// The current regex.
        /// </param>
        public LogParser(string fileToParse, Regex currentRegex)
        {
            this.currentRegex = currentRegex;
            this.fileToParse = fileToParse;
            this.streamReader =
                new StreamReader(
                    new FileStream(
                        this.fileToParse, 
                        FileMode.Open, 
                        FileAccess.Read, 
                        FileShare.Delete | FileShare.ReadWrite));
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     Parse the current log file to a particular end date.
        /// </summary>
        /// <returns>
        ///     The parsed log entries.
        /// </returns>
        public IEnumerable<string> ParseFile()
        {
            var entries = new List<string>();
            do
            {
                string entry = this.ReadLogEntries(this.streamReader);
                if (!string.IsNullOrEmpty(entry))
                {
                    entries.Add(entry);
                }
            }
            while (!this.streamReader.EndOfStream);

            return entries;
        }

        /// <summary>
        ///     The reset.
        /// </summary>
        public void Reset()
        {
            this.streamReader.BaseStream.Position = 0;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Get the actual position of a stream.
        /// </summary>
        /// <param name="reader">
        /// The stream to read.
        /// </param>
        /// <returns>
        /// The <see cref="long"/>.
        /// </returns>
        private static long GetActualPosition(StreamReader reader)
        {
            // The current buffer of decoded characters
            var charBuffer = (char[])reader.GetType()
                    .InvokeMember(
                        "charBuffer", 
                        BindingFlags.DeclaredOnly | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetField, 
                        null, 
                        reader, 
                        null);

            // The current position in the buffer of decoded characters
            var charPos = (int)reader.GetType()
                    .InvokeMember(
                        "charPos", 
                        BindingFlags.DeclaredOnly | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetField, 
                        null, 
                        reader, 
                        null);

            // The number of bytes that the already-read characters need when encoded.
            int numReadBytes = reader.CurrentEncoding.GetByteCount(charBuffer, 0, charPos);

            // The number of encoded bytes that are in the current buffer
            var byteLen = (int)reader.GetType()
                    .InvokeMember(
                        "byteLen", 
                        BindingFlags.DeclaredOnly | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetField, 
                        null, 
                        reader, 
                        null);

            return reader.BaseStream.Position - byteLen + numReadBytes;
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (!this.disposed)
                {
                    this.streamReader.Dispose();
                }

                this.disposed = true;
            }
        }

        /// <summary>
        /// Reads log entries into a list of strings.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <remarks>
        /// A single log entry can span multiple lines in a log file.
        /// </remarks>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string ReadLogEntries(StreamReader stream)
        {
            string logEntry = null;
            long lastPos = 0;

            do
            {
                lastPos = GetActualPosition(stream);
                string line = stream.ReadLine();

                if (line != null)
                {
                    Match match = this.currentRegex.Match(line);

                    if (match.Success)
                    {
                        if (!string.IsNullOrEmpty(logEntry))
                        {
                            stream.BaseStream.Position = lastPos;
                            stream.DiscardBufferedData();
                            return logEntry;
                        }

                        logEntry = line;
                    }
                    else
                    {
                        logEntry += line;
                    }
                }
            }
            while (!stream.EndOfStream);

            return logEntry;
        }

        #endregion
    }
}