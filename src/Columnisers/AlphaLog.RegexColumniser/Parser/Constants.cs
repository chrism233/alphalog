﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Constants.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The constants.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.RegexColumniser.Parser
{
    using System.Text.RegularExpressions;

    /// <summary>
    ///     The constants.
    /// </summary>
    public static class Constants
    {
        #region Static Fields

        /// <summary>
        ///     The start of log entry regex.
        /// </summary>
        public static readonly Regex StartOfLogEntryRegex =
            new Regex(@"^\d{2,4}[-\\/]\d{2}[-\\/]\d{2,4}\s+\d{2}\:\d{2}\:\d{2}[\,\.]\d{3}");

        #endregion
    }
}