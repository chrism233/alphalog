﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RegexColumniser.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The regex columniser.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.RegexColumniser
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;

    using AlphaLog.Columniser;
    using AlphaLog.RegexColumniser.Model;
    using AlphaLog.RegexColumniser.Parser;
    using AlphaLog.UI.Common.Models;
    using AlphaLog.UI.Common.Settings.Model;

    /// <summary>
    ///     The regex columniser.
    /// </summary>
    public class RegexColumniser : Columnizer
    {
        #region Fields

        /// <summary>
        ///     The current regex.
        /// </summary>
        private readonly RegexItem currentRegex;

        /// <summary>
        ///     The log parser.
        /// </summary>
        private readonly LogParser logParser;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RegexColumniser"/> class.
        /// </summary>
        /// <param name="regexItem">
        /// The regex item.
        /// </param>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        public RegexColumniser(RegexItem regexItem, string filePath)
            : base(filePath)
        {
            this.currentRegex = regexItem;
            this.logParser = new LogParser(filePath, this.currentRegex.Regex);
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the name.
        /// </summary>
        public override string Name
        {
            get
            {
                return string.Format("Regex {0}", this.currentRegex.SettingsName);
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The column name to type mappings.
        /// </summary>
        /// <returns>
        ///     A <see cref="Dictionary{String, Type}" /> mappings column names to their types.
        /// </returns>
        /// <exception cref="ObjectDisposedException">
        ///     The <see cref="RegexColumniser" /> has been disposed.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     The <see cref="DataType" /> is not supported by this columniser.
        /// </exception>
        public override Dictionary<string, Type> GetColumns()
        {
            if (this.disposed)
            {
                throw new ObjectDisposedException(typeof(RegexColumniser).Name);
            }

            var items = new Dictionary<string, Type>();
            foreach (SourceField item in this.currentRegex.Fields)
            {
                switch (item.DataType)
                {
                    case DataType.Text:
                        items.Add(item.Title, typeof(string));
                        break;
                    case DataType.Numeric:
                        items.Add(item.Title, typeof(double));
                        break;
                    case DataType.DateTime:
                        items.Add(item.Title, typeof(DateTime));
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return items;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The update core.
        /// </summary>
        /// <param name="observer">
        /// The observer.
        /// </param>
        /// <exception cref="ObjectDisposedException">
        /// The <see cref="RegexColumniser"/> has been disposed.
        /// </exception>
        protected override void UpdateCore(IObserver<Dictionary<string, object>> observer)
        {
            if (this.disposed)
            {
                throw new ObjectDisposedException(typeof(RegexColumniser).Name);
            }

            foreach (var entry in this.logParser.ParseFile().Select(this.ColumnizeEntry))
            {
                observer.OnNext(entry);
            }
        }

        /// <summary>
        /// Parse a single log entry
        /// </summary>
        /// <param name="entry">
        /// The entry to columnise
        /// </param>
        /// <returns>
        /// A <see cref="Dictionary{String, Object}"/> mapping column name to values for the specified entry.
        /// </returns>
        private Dictionary<string, object> ColumnizeEntry(string entry)
        {
            var logentry = new Dictionary<string, object>();
            Match match = this.currentRegex.Regex.Match(entry);
            if (match.Success)
            {
                GroupCollection groups = match.Groups;

                foreach (SourceField f in this.currentRegex.Fields)
                {
                    switch (f.DataType)
                    {
                        case DataType.Text:
                            logentry[f.Title] = groups[f.Title].Value;
                            break;
                        case DataType.Numeric:
                            logentry[f.Title] = double.Parse(groups[f.Title].Value);
                            break;
                        case DataType.DateTime:
                            logentry[f.Title] = DateTime.ParseExact(
                                groups[f.Title].Value, 
                                f.Format, 
                                CultureInfo.InvariantCulture);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }

                return logentry;
            }

            throw new InvalidDataException(
                string.Format("The RegEx could not parse the log entry, log entry = {0}", entry));
        }

        #endregion
    }
}