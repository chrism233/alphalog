﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RegexDatabase.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The regex database.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.RegexColumniser.Model
{
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Linq;
    using System.Xml.Linq;

    using AlphaLog.UI.Common;

    /// <summary>
    ///     The regex database.
    /// </summary>
    [Export]
    public class RegexDatabase : Repository<RegexItem>
    {
        #region Constants

        /// <summary>
        ///     The regex settings name.
        /// </summary>
        private const string RegexSettingsName = "RegexItems";

        #endregion

        #region Fields

        /// <summary>
        ///     The expressions.
        /// </summary>
        private Dictionary<string, RegexItem> regexItems = new Dictionary<string, RegexItem>();

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the settings name.
        /// </summary>
        public override string SettingsName
        {
            get
            {
                return RegexSettingsName;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Get an enumeration of regular expression.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerable{RegexItem}" />.
        /// </returns>
        public override IEnumerable<RegexItem> GetItems()
        {
            return this.regexItems.Values;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RegexDatabase"/> class.
        /// </summary>
        /// <param name="regex">
        /// The matcher.
        /// </param>
        public override void Initialize(XElement regex)
        {
            this.regexItems = regex.Elements("RegexItem")
                .Select(r => new RegexItem(r))
                .ToDictionary(k => k.SettingsName, v => v);
        }

        /// <summary>
        /// Remove regular expression.
        /// </summary>
        /// <param name="name">
        /// The regex to remove.
        /// </param>
        public void RemoveRegex(string name)
        {
            this.regexItems.Remove(name);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The store core.
        /// </summary>
        /// <param name="items">
        /// The items.
        /// </param>
        protected override void StoreCore(IEnumerable<RegexItem> items)
        {
            // Remove
            IEnumerable<RegexItem> itemsToRemove = this.regexItems.Values.Except(items);
            foreach (RegexItem item in itemsToRemove.ToList())
            {
                this.regexItems.Remove(item.SettingsName);
            }

            // Create/Update 
            foreach (RegexItem item in items)
            {
                if (this.regexItems.ContainsKey(item.SettingsName))
                {
                    this.regexItems[item.SettingsName] = item;
                }
                else
                {
                    this.regexItems.Add(item.SettingsName, item);
                }
            }
        }

        #endregion
    }
}