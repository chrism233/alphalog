﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RegexItem.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The regex item.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.RegexColumniser.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Xml.Linq;

    using AlphaLog.UI.Common;
    using AlphaLog.UI.Common.Settings.Model;

    using Microsoft.Practices.Prism.Mvvm;

    /// <summary>
    ///     The regex item.
    /// </summary>
    public class RegexItem : BindableBase, IEquatable<RegexItem>, IPersist
    {
        #region Constants

        /// <summary>
        ///     The regex item name.
        /// </summary>
        private const string RegexItemName = "RegexItem";

        #endregion

        #region Fields

        /// <summary>
        ///     The fields.
        /// </summary>
        private IEnumerable<SourceField> fields;

        /// <summary>
        ///     The name.
        /// </summary>
        private string settingsName;

        /// <summary>
        ///     The value.
        /// </summary>
        private string value;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RegexItem"/> class.
        /// </summary>
        /// <param name="regex">
        /// The regex.
        /// </param>
        public RegexItem(XElement regex)
            : this(
                regex.Attribute("name").Value, 
                regex.Value, 
                regex.Element("Fields").Elements().Select(e => new SourceField(e)))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RegexItem"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="fields">
        /// The fields.
        /// </param>
        public RegexItem(string name, string value, IEnumerable<SourceField> fields)
        {
            this.SettingsName = name;
            this.Value = value;
            this.Fields = new List<SourceField>(fields);
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the fields.
        /// </summary>
        public IEnumerable<SourceField> Fields
        {
            get
            {
                return this.fields;
            }

            set
            {
                this.SetProperty(ref this.fields, value);
            }
        }

        /// <summary>
        ///     Gets the regex.
        /// </summary>
        public Regex Regex
        {
            get
            {
                return new Regex(this.Value.Trim());
            }
        }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        public string SettingsName
        {
            get
            {
                return this.settingsName;
            }

            set
            {
                this.SetProperty(ref this.settingsName, value);
            }
        }

        /// <summary>
        ///     Gets or sets the value.
        /// </summary>
        public string Value
        {
            get
            {
                return this.value;
            }

            set
            {
                this.SetProperty(ref this.value, value);
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The equals.
        /// </summary>
        /// <param name="other">
        /// The other.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Equals(RegexItem other)
        {
            return this.settingsName == other.settingsName && this.value == other.value;
        }

        /// <summary>
        ///     The serialize.
        /// </summary>
        /// <returns>
        ///     The <see cref="XElement" />.
        /// </returns>
        public XElement Serialize()
        {
            return new XElement(
                RegexItemName, 
                new XAttribute("name", this.settingsName), 
                new XCData(this.Value), 
                new XElement("Fields", this.Fields.Select(f => f.Serialize())));
        }

        #endregion
    }
}