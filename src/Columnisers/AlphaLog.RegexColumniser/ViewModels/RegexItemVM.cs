﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RegexItemVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The regex item vm.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.RegexColumniser.ViewModels
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Windows.Input;

    using AlphaLog.RegexColumniser.Model;
    using AlphaLog.UI.Common;
    using AlphaLog.UI.Common.Settings.Model;
    using AlphaLog.UI.Common.ViewModels;

    using Microsoft.Practices.Prism.Commands;
    using Microsoft.Practices.Prism.Mvvm;
    using Microsoft.Practices.Prism.ViewModel;

    /// <summary>
    ///     The regex item vm.
    /// </summary>
    public class RegexItemVM : BindableBase, INotifyDataErrorInfo
    {
        #region Fields

        /// <summary>
        ///     The error container.
        /// </summary>
        private readonly ErrorsContainer<string> errorContainer;

        /// <summary>
        ///     The regex item.
        /// </summary>
        private readonly RegexItem regexItem;

        /// <summary>
        ///     The has un saved changes.
        /// </summary>
        private bool hasUnSavedChanges;

        /// <summary>
        ///     The settings.
        /// </summary>
        private ISettingsManager settings;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RegexItemVM"/> class.
        /// </summary>
        /// <param name="regexItem">
        /// The regex item.
        /// </param>
        /// <param name="settings">
        /// The settings.
        /// </param>
        public RegexItemVM(RegexItem regexItem, ISettingsManager settings)
        {
            this.errorContainer = new ErrorsContainer<string>(this.OnErrorsChanged);

            this.settings = settings;
            this.regexItem = regexItem;
            this.Fields =
                new BindingListEx<SourceFieldVM>(
                    regexItem.Fields.Select(f => new SourceFieldVM(f, settings)).ToList(), 
                    () => new SourceFieldVM(new SourceField(), settings));
            this.Fields.ListChanged += this.Fields_ListChanged;
            this.PropertyChanged += this.RegexItemVM_PropertyChanged;
            this.RemoveFieldCommand = new DelegateCommand<SourceFieldVM>(this.RemoveField);
        }

        #endregion

        #region Public Events

        /// <summary>
        ///     The errors changed.
        /// </summary>
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the fields.
        /// </summary>
        public BindingList<SourceFieldVM> Fields { get; set; }

        /// <summary>
        ///     Gets a value indicating whether has errors.
        /// </summary>
        public bool HasErrors
        {
            get
            {
                return this.errorContainer.HasErrors;
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether has un saved changes.
        /// </summary>
        public bool HasUnSavedChanges
        {
            get
            {
                return this.hasUnSavedChanges;
            }

            set
            {
                this.SetProperty(ref this.hasUnSavedChanges, value);
            }
        }

        /// <summary>
        ///     Gets the item.
        /// </summary>
        public RegexItem Item
        {
            get
            {
                return this.regexItem;
            }
        }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.regexItem.SettingsName;
            }

            set
            {
                this.regexItem.SettingsName = value;
                this.OnPropertyChanged(() => this.Name);
            }
        }

        /// <summary>
        ///     Gets the possible fields.
        /// </summary>
        public IEnumerable<string> PossibleFields
        {
            get
            {
                return new Regex(this.Value.Trim()).GetGroupNames().Where(f => f != "0");
            }
        }

        /// <summary>
        ///     Gets or sets the command to remove a field.
        /// </summary>
        public ICommand RemoveFieldCommand { get; protected set; }

        /// <summary>
        ///     Gets or sets the value.
        /// </summary>
        public string Value
        {
            get
            {
                return this.regexItem.Value;
            }

            set
            {
                this.regexItem.Value = value;
                this.OnPropertyChanged(() => this.Value);
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get errors.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable GetErrors(string propertyName)
        {
            return this.errorContainer.GetErrors(propertyName);
        }

        /// <summary>
        ///     The save changes.
        /// </summary>
        public void SaveChanges()
        {
            if (!this.HasErrors && this.Fields.Count(f => f.HasErrors) == 0)
            {
                this.regexItem.SettingsName = this.Name;
                this.regexItem.Value = this.Value;
                this.regexItem.Fields = this.Fields.Select(f => f.GetField());
                this.HasUnSavedChanges = false;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The on errors changed.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        protected virtual void OnErrorsChanged(string propertyName)
        {
            EventHandler<DataErrorsChangedEventArgs> handler = this.ErrorsChanged;
            if (handler != null)
            {
                handler(this, new DataErrorsChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// The fields_ list changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void Fields_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (e.ListChangedType == ListChangedType.ItemChanged)
            {
                this.OnPropertyChanged(e.PropertyDescriptor.Name);
            }
        }

        /// <summary>
        /// The regex item v m_ property changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void RegexItemVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "HasUnSavedChanges")
            {
                this.HasUnSavedChanges = true;
            }
        }

        /// <summary>
        /// The remove field.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        private void RemoveField(SourceFieldVM obj)
        {
            this.Fields.Remove(obj);
        }

        #endregion
    }
}