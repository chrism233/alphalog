﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AddRegexVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   A view-model for managing regular expressions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.RegexColumniser.ViewModels
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.Composition;
    using System.Linq;
    using System.Windows.Input;

    using AlphaLog.RegexColumniser.Model;
    using AlphaLog.UI.Common;
    using AlphaLog.UI.Common.Settings.Model;

    using Microsoft.Practices.Prism.Commands;

    /// <summary>
    ///     A view-model for managing regular expressions.
    /// </summary>
    [Export]
    public class AddRegexVM : TabSettingsVM
    {
        #region Fields

        /// <summary>
        ///     The database.
        /// </summary>
        private readonly RegexDatabase database;

        /// <summary>
        ///     The settings.
        /// </summary>
        private readonly ISettingsManager settings;

        /// <summary>
        ///     The adding new regex.
        /// </summary>
        private bool addingNewRegex;

        /// <summary>
        ///     The regex items.
        /// </summary>
        private ObservableCollectionWithPropertyChanged<RegexItemVM> regexItems;

        /// <summary>
        ///     The selected regex item.
        /// </summary>
        private RegexItemVM selectedRegexItem;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AddRegexVM"/> class.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        [ImportingConstructor]
        public AddRegexVM(ISettingsManager settings)
            : base("Regex")
        {
            this.settings = settings;
            this.database = settings.GetsSetting<RegexDatabase>();
            this.SaveRegexCommand = new DelegateCommand(this.SaveExecuted);
            this.AddRegexCommand = new DelegateCommand(this.AddExecuted);
            this.RemoveRegexCommand = new DelegateCommand<RegexItemVM>(this.RemoveExecuted);
            this.CancelRegexCommand = new DelegateCommand(this.CancelRegex);
            this.RegexItems =
                new ObservableCollectionWithPropertyChanged<RegexItemVM>(
                    this.database.GetItems().Select(r => new RegexItemVM(r, settings)));
            this.RegexItems.ItemPropertyChanged += this.RegexItems_ItemPropertyChanged;

            if (this.RegexItems.Count >= 1)
            {
                this.SelectedRegexItem = this.RegexItems.FirstOrDefault();
            }
            else
            {
                this.AddExecuted();
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the add regex command.
        /// </summary>
        public ICommand AddRegexCommand { get; protected set; }

        /// <summary>
        ///     Gets or sets a value indicating whether adding new regex.
        /// </summary>
        public bool AddingNewRegex
        {
            get
            {
                return this.addingNewRegex;
            }

            set
            {
                this.SetProperty(ref this.addingNewRegex, value);
            }
        }

        /// <summary>
        ///     Gets or sets the cancel regex command.
        /// </summary>
        public ICommand CancelRegexCommand { get; protected set; }

        /// <summary>
        ///     Gets or sets the regex items.
        /// </summary>
        public ObservableCollectionWithPropertyChanged<RegexItemVM> RegexItems
        {
            get
            {
                return this.regexItems;
            }

            set
            {
                this.SetProperty(ref this.regexItems, value);
            }
        }

        /// <summary>
        ///     Gets or sets the remove regex command.
        /// </summary>
        public DelegateCommand<RegexItemVM> RemoveRegexCommand { get; protected set; }

        /// <summary>
        ///     Gets or sets the save regex command.
        /// </summary>
        public ICommand SaveRegexCommand { get; protected set; }

        /// <summary>
        ///     Gets or sets the selected regex item.
        /// </summary>
        public RegexItemVM SelectedRegexItem
        {
            get
            {
                return this.selectedRegexItem;
            }

            set
            {
                this.SetProperty(ref this.selectedRegexItem, value);
                this.RemoveRegexCommand.RaiseCanExecuteChanged();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     The add executed.
        /// </summary>
        private void AddExecuted()
        {
            if (this.AddingNewRegex)
            {
                this.RegexItems.Add(this.SelectedRegexItem);
                this.AddingNewRegex = false;
                this.OnPropertyChanged("SelectedRegexItem");
            }
            else
            {
                this.AddingNewRegex = true;
                this.SelectedRegexItem =
                    new RegexItemVM(new RegexItem(string.Empty, string.Empty, new List<SourceField>()), this.settings);
            }
        }

        /// <summary>
        ///     The cancel regex.
        /// </summary>
        private void CancelRegex()
        {
            this.AddingNewRegex = false;
            this.SelectedRegexItem = this.RegexItems.First();
        }

        /// <summary>
        /// The regex items_ item property changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void RegexItems_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "HasUnSavedChanges")
            {
                this.HasUnSavedChanges = this.RegexItems.Any(i => i.HasUnSavedChanges);
            }
        }

        /// <summary>
        /// The remove executed.
        /// </summary>
        /// <param name="itemToRemove">
        /// The item To Remove.
        /// </param>
        private void RemoveExecuted(RegexItemVM itemToRemove)
        {
            this.RegexItems.Remove(itemToRemove);
            if (this.RegexItems.Count >= 1)
            {
                this.SelectedRegexItem = this.RegexItems.FirstOrDefault();
            }
            else
            {
                this.AddExecuted();
            }
        }

        /// <summary>
        ///     The save executed.
        /// </summary>
        private void SaveExecuted()
        {
            this.HasUnSavedChanges = false;
            this.AddingNewRegex = false;
            this.RegexItems.ToList().ForEach(i => i.SaveChanges());
            this.database.Store(this.RegexItems.Where(f => !f.HasErrors).Select(r => r.Item).ToArray());
        }

        #endregion
    }
}