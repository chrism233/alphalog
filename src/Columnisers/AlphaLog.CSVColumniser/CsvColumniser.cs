﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CsvColumniser.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The csv columniser.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.CSVColumniser
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using AlphaLog.Columniser;
    using AlphaLog.CSVColumniser.Model;
    using AlphaLog.UI.Common.Models;
    using AlphaLog.UI.Common.Settings.Model;

    using Microsoft.VisualBasic.FileIO;

    /// <summary>
    ///     The csv columniser.
    /// </summary>
    public class CsvColumniser : Columnizer
    {
        #region Fields

        /// <summary>
        ///     The csv item.
        /// </summary>
        private readonly CsvItem csvItem;

        /// <summary>
        ///     The header name to source field.
        /// </summary>
        private readonly Dictionary<string, SourceField> headerNameToSourceField;

        /// <summary>
        ///     The headers.
        /// </summary>
        private readonly string[] headers;

        /// <summary>
        ///     The parser.
        /// </summary>
        private readonly TextFieldParser parser;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CsvColumniser"/> class.
        /// </summary>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        /// <param name="csvItem">
        /// The csv item.
        /// </param>
        public CsvColumniser(string filePath, CsvItem csvItem)
            : base(filePath)
        {
            this.csvItem = csvItem;
            this.parser = new TextFieldParser(new StreamReader(new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Delete | FileShare.ReadWrite)));
            this.parser.SetDelimiters(DelimiterSettings.DelimiterToString(csvItem.Settings.Delimiter));
            this.headers = this.parser.ReadFields();
            this.headerNameToSourceField = this.headers.ToDictionary(k => k, v => csvItem.SourceFields.FirstOrDefault(f => f.Title == v));
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the name.
        /// </summary>
        public override string Name
        {
            get
            {
                return "CSV";
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The column name to type mappings.
        /// </summary>
        /// <returns>
        ///     A <see cref="Dictionary{String, Type}" /> mappings column names to their types.
        /// </returns>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     The <see cref="DataType" /> has is not supported.
        /// </exception>
        public override Dictionary<string, Type> GetColumns()
        {
            var items = new Dictionary<string, Type>();
            foreach (SourceField item in this.csvItem.SourceFields)
            {
                switch (item.DataType)
                {
                    case DataType.Text:
                        items.Add(item.Title, typeof(string));
                        break;
                    case DataType.Numeric:
                        items.Add(item.Title, typeof(double));
                        break;
                    case DataType.DateTime:
                        items.Add(item.Title, typeof(DateTime));
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return items;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The update core.
        /// </summary>
        /// <param name="observer">
        /// The observer.
        /// </param>
        protected override void UpdateCore(IObserver<Dictionary<string, object>> observer)
        {
            try
            {
                while (!this.parser.EndOfData)
                {
                    var items = new Dictionary<string, object>();
                    string[] fields = this.parser.ReadFields();
                    for (int i = 0; i < fields.Length; i++)
                    {
                        SourceField tempField = this.headerNameToSourceField[this.headers[i]];
                        if (tempField != null)
                        {
                            switch (tempField.DataType)
                            {
                                case DataType.Text:
                                    items.Add(this.headers[i], fields[i]);
                                    break;
                                case DataType.Numeric:
                                    items.Add(this.headers[i], double.Parse(fields[i]));
                                    break;
                                case DataType.DateTime:
                                    items.Add(this.headers[i], DateTime.Parse(fields[i]));
                                    break;
                            }
                        }
                    }

                    observer.OnNext(items);
                }
            }
            catch (MalformedLineException)
            {
            }
        }

        #endregion
    }
}