﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CsvColumniserDescriptor.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The csv columniser.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.CSVColumniser
{
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.IO;
    using System.Linq;

    using AlphaLog.Columniser;
    using AlphaLog.CSVColumniser.Model;
    using AlphaLog.UI.Common;

    using Microsoft.VisualBasic.FileIO;

    /// <summary>
    ///     The csv columniser.
    /// </summary>
    [Export(typeof(IColumnizerDescriptor))]
    public class CsvColumniserDescriptor : ColumnizerDescriptor<CsvItem>
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CsvColumniserDescriptor"/> class.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        [ImportingConstructor]
        public CsvColumniserDescriptor(ISettingsManager settings)
            : base(settings.GetsSetting<CsvRepository>())
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The can columnizer file.
        /// </summary>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool CanColumnize(string filePath)
        {
            if (Path.GetExtension(filePath) != ".csv")
            {
                return false;
            }

            return this.FindMatch(filePath) != null;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The do create.
        /// </summary>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        /// <returns>
        /// The <see cref="IColumnizer"/>.
        /// </returns>
        protected override IColumnizer DoCreate(string filePath)
        {
            return new CsvColumniser(filePath, this.FindMatch(filePath));
        }

        /// <summary>
        /// The find match.
        /// </summary>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        /// <returns>
        /// The <see cref="CsvItem"/>.
        /// </returns>
        private CsvItem FindMatch(string filePath)
        {
            CsvItem bestMatch = null;
            foreach (CsvItem item in this.GetItems)
            {
                bool canReadHeaders;
                var headers = new string[0];
                using (var streamReader = new StreamReader(new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Delete | FileShare.ReadWrite)))
                using (var parser = new TextFieldParser(streamReader))
                {
                    parser.SetDelimiters(DelimiterSettings.DelimiterToString(item.Settings.Delimiter));
                    try
                    {
                        headers = parser.ReadFields();
                        canReadHeaders = true;
                    }
                    catch (MalformedLineException)
                    {
                        canReadHeaders = false;
                    }
                }

                if (canReadHeaders && headers != null)
                {
                    // All of the defined fields are contained in the provided .csv file.
                    IEnumerable<string> temp = item.SourceFields.Select(f => f.Title).Except(headers);
                    if (!temp.Any())
                    {
                        bestMatch = item;
                    }
                }
            }

            return bestMatch;
        }

        #endregion
    }
}