﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AddCsv.xaml.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   Interaction logic for AddCsv.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.CSVColumniser.Views
{
    using System.ComponentModel.Composition;
    using System.Windows.Controls;

    using AlphaLog.CSVColumniser.ViewModels;
    using AlphaLog.UI.Common;

    using Microsoft.Practices.Prism.Regions;

    /// <summary>
    ///     Interaction logic for AddCsv.xaml
    /// </summary>
    [ViewExport(RegionName = RegionNames.SettingsTab, TreePath = "Columnisers.Csv")]
    [ViewSortHint("003")]
    public partial class AddCsv : UserControl
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AddCsv"/> class.
        /// </summary>
        /// <param name="viewModel">
        /// The view model.
        /// </param>
        [ImportingConstructor]
        public AddCsv(AddCsvVM viewModel)
            : this()
        {
            this.DataContext = viewModel;
        }

        /// <summary>
        ///     Prevents a default instance of the <see cref="AddCsv" /> class from being created.
        /// </summary>
        private AddCsv()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}