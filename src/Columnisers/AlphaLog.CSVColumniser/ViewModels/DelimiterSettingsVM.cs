﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DelimiterSettingsVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The delimiter settings vm.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.CSVColumniser.ViewModels
{
    using AlphaLog.CSVColumniser.Model;

    using Microsoft.Practices.Prism.Mvvm;

    /// <summary>
    ///     The delimiter settings vm.
    /// </summary>
    public class DelimiterSettingsVM : BindableBase
    {
        #region Fields

        /// <summary>
        ///     The settings.
        /// </summary>
        private readonly DelimiterSettings settings;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DelimiterSettingsVM"/> class.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        public DelimiterSettingsVM(DelimiterSettings settings)
        {
            this.settings = settings;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the delimiter.
        /// </summary>
        public DelimiterOptions Delimiter
        {
            get
            {
                return this.settings.Delimiter;
            }

            set
            {
                this.settings.Delimiter = value;
                this.OnPropertyChanged("Delimiter");
            }
        }

        /// <summary>
        ///     Gets the item.
        /// </summary>
        public DelimiterSettings Item
        {
            get
            {
                return this.settings;
            }
        }

        /// <summary>
        ///     Gets or sets the text qualifier.
        /// </summary>
        public TextQualifier TextQualifier
        {
            get
            {
                return this.settings.TextQualifier;
            }

            set
            {
                this.settings.TextQualifier = value;
                this.OnPropertyChanged("TextQualifier");
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether treat consecutive as one.
        /// </summary>
        public bool TreatConsecutiveAsOne
        {
            get
            {
                return this.settings.TreatConsecutiveAsOne;
            }

            set
            {
                this.settings.TreatConsecutiveAsOne = value;
                this.OnPropertyChanged("TreatConsecutiveAsOne");
            }
        }

        #endregion
    }
}