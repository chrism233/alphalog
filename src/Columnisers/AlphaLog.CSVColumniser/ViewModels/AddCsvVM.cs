﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AddCsvVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The add csv vm.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.CSVColumniser.ViewModels
{
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.ComponentModel.Composition;
    using System.Linq;
    using System.Windows.Input;

    using AlphaLog.CSVColumniser.Model;
    using AlphaLog.UI.Common;

    using Microsoft.Practices.Prism.Commands;

    /// <summary>
    ///     The add csv vm.
    /// </summary>
    [Export]
    public class AddCsvVM : TabSettingsVM
    {
        #region Fields

        /// <summary>
        ///     The csv database.
        /// </summary>
        private readonly CsvRepository csvDatabase;

        /// <summary>
        ///     The settings manager.
        /// </summary>
        private readonly ISettingsManager settings;

        /// <summary>
        ///     The adding new csv.
        /// </summary>
        private bool addingNewCsv;

        /// <summary>
        ///     The selected csv item.
        /// </summary>
        private CsvItemVM selectedCsvItem;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AddCsvVM"/> class.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        [ImportingConstructor]
        public AddCsvVM(ISettingsManager settings)
            : base("CSV")
        {
            this.settings = settings;
            this.csvDatabase = settings.GetsSetting<CsvRepository>();
            this.CsvItems =
                new ObservableCollectionWithPropertyChanged<CsvItemVM>(
                    this.csvDatabase.GetItems().ToList().Select(i => new CsvItemVM(i, settings)));
            this.CsvItems.ItemPropertyChanged += this.CsvItems_ItemPropertyChanged;
            this.CsvItems.CollectionChanged += this.CsvItems_CollectionChanged;

            this.AddCsvCommand = new DelegateCommand(this.AddCsv);
            this.CancelCsvCommand = new DelegateCommand(this.CancelCsv);
            this.RemoveCsvCommand = new DelegateCommand<CsvItemVM>(this.RemoveCsv);

            if (this.CsvItems.Count == 0)
            {
                this.AddingNewCsv = true;
            }
            else
            {
                this.SelectedCsvItem = this.CsvItems.FirstOrDefault();
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the add csv command.
        /// </summary>
        public ICommand AddCsvCommand { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether adding new csv.
        /// </summary>
        public bool AddingNewCsv
        {
            get
            {
                return this.addingNewCsv;
            }

            set
            {
                if (value)
                {
                    this.SelectedCsvItem = CsvItemVM.Create(this.settings);

                    // new CsvItemVM(new CsvItem(), this.settings);
                }

                this.SetProperty(ref this.addingNewCsv, value);
            }
        }

        /// <summary>
        ///     Gets or sets the cancel csv command.
        /// </summary>
        public ICommand CancelCsvCommand { get; set; }

        /// <summary>
        ///     Gets or sets the csv items.
        /// </summary>
        public ObservableCollectionWithPropertyChanged<CsvItemVM> CsvItems { get; set; }

        /// <summary>
        ///     Gets or sets the remove csv command.
        /// </summary>
        public ICommand RemoveCsvCommand { get; set; }

        /// <summary>
        ///     Gets or sets the selected CSV item.
        /// </summary>
        public CsvItemVM SelectedCsvItem
        {
            get
            {
                return this.selectedCsvItem;
            }

            set
            {
                this.selectedCsvItem = value;
                this.OnPropertyChanged("SelectedCsvItem");
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     The save changes.
        /// </summary>
        protected override void SaveChanges()
        {
            if (this.AddingNewCsv)
            {
                this.AddCsv();
            }

            this.CsvItems.ToList().ForEach(i => i.SaveChanges());
            this.csvDatabase.Store(this.CsvItems.Where(f => !f.HasErrors).Select(r => r.Item).ToArray());
            base.SaveChanges();
        }

        /// <summary>
        ///     The add csv.
        /// </summary>
        private void AddCsv()
        {
            if (this.AddingNewCsv)
            {
                this.CsvItems.Add(this.SelectedCsvItem);
                this.HasUnSavedChanges = true;
                this.AddingNewCsv = false;
                this.OnPropertyChanged("SelectedCsvItem");
            }
            else
            {
                this.AddingNewCsv = true;
                this.SelectedCsvItem = new CsvItemVM(CsvItem.Create(), this.settings);
            }
        }

        /// <summary>
        ///     The cancel csv.
        /// </summary>
        private void CancelCsv()
        {
            this.AddingNewCsv = false;
            this.SelectedCsvItem = this.CsvItems.First();
        }

        /// <summary>
        /// The csv items_ collection changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void CsvItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                if (this.CsvItems.Count >= 1)
                {
                    this.SelectedCsvItem = this.CsvItems.FirstOrDefault();
                }
                else
                {
                    this.AddCsv();
                }
            }
        }

        /// <summary>
        /// The csv items_ item property changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void CsvItems_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "HasUnSavedChanges")
            {
                this.HasUnSavedChanges = this.CsvItems.Any(i => i.HasUnSavedChanges);
            }
        }

        /// <summary>
        /// The remove csv.
        /// </summary>
        /// <param name="itemToRemove">
        /// The item To Remove.
        /// </param>
        private void RemoveCsv(CsvItemVM itemToRemove)
        {
            this.CsvItems.Remove(itemToRemove);
            if (this.CsvItems.Count >= 1)
            {
                this.SelectedCsvItem = this.CsvItems.FirstOrDefault();
            }
            else
            {
                this.AddCsv();
            }
        }

        #endregion
    }
}