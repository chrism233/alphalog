﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CsvItemVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The csv item vm.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.CSVColumniser.ViewModels
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows.Input;

    using AlphaLog.CSVColumniser.Model;
    using AlphaLog.UI.Common;
    using AlphaLog.UI.Common.Settings.Model;
    using AlphaLog.UI.Common.ViewModels;

    using Microsoft.Practices.Prism.Commands;
    using Microsoft.Practices.Prism.Mvvm;
    using Microsoft.Practices.Prism.ViewModel;

    /// <summary>
    ///     The csv item vm.
    /// </summary>
    public class CsvItemVM : BindableBase, INotifyDataErrorInfo
    {
        #region Fields

        /// <summary>
        ///     The error container.
        /// </summary>
        private readonly ErrorsContainer<string> errorContainer;

        /// <summary>
        ///     The item.
        /// </summary>
        private readonly CsvItem item;

        /// <summary>
        ///     The name.
        /// </summary>
        private string name;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CsvItemVM"/> class.
        /// </summary>
        /// <param name="csvItem">
        /// The csv item.
        /// </param>
        /// <param name="settings">
        /// The settings manager instance.
        /// </param>
        public CsvItemVM(CsvItem csvItem, ISettingsManager settings)
        {
            this.item = csvItem;
            this.Name = csvItem.Name;
            this.errorContainer = new ErrorsContainer<string>(this.OnErrorsChanged);
            this.Settings = new DelimiterSettingsVM(csvItem.Settings);
            this.Fields =
                new BindingListEx<SourceFieldVM>(
                    csvItem.SourceFields.Select(f => new SourceFieldVM(f, settings)).ToList(), 
                    () => new SourceFieldVM(new SourceField(), settings));
            this.Fields.ListChanged += this.Fields_ListChanged;
            this.PropertyChanged += this.CsvItemVMPropertyChanged;
            this.Settings.PropertyChanged += this.CsvItemVMPropertyChanged;
            this.RemoveFieldCommand = new DelegateCommand<SourceFieldVM>(this.RemoveField);
        }

        #endregion

        #region Public Events

        /// <summary>
        ///     The errors changed.
        /// </summary>
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the fields.
        /// </summary>
        public BindingList<SourceFieldVM> Fields { get; set; }

        /// <summary>
        ///     Gets a value indicating whether this item has errors.
        /// </summary>
        public bool HasErrors { get; private set; }

        /// <summary>
        ///     Gets or sets a value indicating whether this item has unsaved changes.
        /// </summary>
        public bool HasUnSavedChanges { get; set; }

        /// <summary>
        ///     Gets the item.
        /// </summary>
        public CsvItem Item
        {
            get
            {
                return this.item;
            }
        }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
                this.OnPropertyChanged("Name");
            }
        }

        /// <summary>
        ///     Gets or sets the remove field command.
        /// </summary>
        public ICommand RemoveFieldCommand { get; set; }

        /// <summary>
        ///     Gets the settings.
        /// </summary>
        public DelimiterSettingsVM Settings { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <returns>
        /// The <see cref="CsvItemVM"/>.
        /// </returns>
        public static CsvItemVM Create(ISettingsManager settings)
        {
            return new CsvItemVM(CsvItem.Create(), settings);
        }

        /// <summary>
        /// The get errors.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable GetErrors(string propertyName)
        {
            return this.errorContainer.GetErrors(propertyName);
        }

        /// <summary>
        ///     The save changes.
        /// </summary>
        public void SaveChanges()
        {
            if (!this.HasErrors && this.Fields.Count(f => f.HasErrors) == 0)
            {
                this.item.Name = this.Name;
                this.item.Settings = this.Settings.Item;
                this.item.SourceFields = this.Fields.Select(f => f.GetField());
                this.HasUnSavedChanges = false;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// On errors changed.
        /// </summary>
        /// <param name="propertyName">
        /// The associated property.
        /// </param>
        protected virtual void OnErrorsChanged(string propertyName)
        {
            EventHandler<DataErrorsChangedEventArgs> handler = this.ErrorsChanged;
            if (handler != null)
            {
                handler(this, new DataErrorsChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// The csv item v m_ property changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void CsvItemVMPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "HasUnSavedChanges")
            {
                this.HasUnSavedChanges = true;
            }
        }

        /// <summary>
        /// The fields_ list changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void Fields_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (e.ListChangedType == ListChangedType.ItemChanged)
            {
                this.OnPropertyChanged(e.PropertyDescriptor.Name);
            }
        }

        /// <summary>
        /// The remove field.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        private void RemoveField(SourceFieldVM obj)
        {
            this.Fields.Remove(obj);
        }

        #endregion
    }
}