﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DelimiterSettings.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The delimiter settings.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.CSVColumniser.Model
{
    using System;
    using System.Xml.Linq;

    using AlphaLog.CSVColumniser.ViewModels;
    using AlphaLog.UI.Common;

    /// <summary>
    ///     The delimiter settings.
    /// </summary>
    public class DelimiterSettings : IPersist
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DelimiterSettings"/> class.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        public DelimiterSettings(XElement element)
        {
            this.Delimiter =
                (DelimiterOptions)Enum.Parse(typeof(DelimiterOptions), element.Attribute("delimiter").Value);
            this.TextQualifier =
                (TextQualifier)Enum.Parse(typeof(TextQualifier), element.Attribute("textQualifier").Value);
            this.TreatConsecutiveAsOne = bool.Parse(element.Attribute("treatConsecutiveAsOne").Value);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DelimiterSettings"/> class.
        /// </summary>
        /// <param name="delimiter">
        /// The delimiter.
        /// </param>
        /// <param name="textQualifier">
        /// The text Qualifier.
        /// </param>
        /// <param name="treatConsecutiveAsOne">
        /// The treat Consecutive As One.
        /// </param>
        public DelimiterSettings(DelimiterOptions delimiter, TextQualifier textQualifier, bool treatConsecutiveAsOne)
        {
            this.Delimiter = delimiter;
            this.TextQualifier = textQualifier;
            this.TreatConsecutiveAsOne = treatConsecutiveAsOne;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the delimiter.
        /// </summary>
        public DelimiterOptions Delimiter { get; set; }

        /// <summary>
        ///     Gets the settings name.
        /// </summary>
        public string SettingsName
        {
            get
            {
                return "Settings";
            }
        }

        /// <summary>
        ///     Gets or sets the text qualifier.
        /// </summary>
        public TextQualifier TextQualifier { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether treat consecutive as one.
        /// </summary>
        public bool TreatConsecutiveAsOne { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The delimiter to string.
        /// </summary>
        /// <param name="delimiter">
        /// The delimiter.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// The <see cref="DelimiterOptions"/> is not supported.
        /// </exception>
        public static string DelimiterToString(DelimiterOptions delimiter)
        {
            switch (delimiter)
            {
                case DelimiterOptions.Tab:
                    return "\t";
                case DelimiterOptions.Semicolon:
                    return ";";
                case DelimiterOptions.Comma:
                    return ",";
                case DelimiterOptions.Space:
                    return " ";
                default:
                    throw new ArgumentOutOfRangeException("delimiter");
            }
        }

        /// <summary>
        ///     The get default.
        /// </summary>
        /// <returns>
        ///     The <see cref="DelimiterSettings" />.
        /// </returns>
        public static DelimiterSettings GetDefault()
        {
            return new DelimiterSettings(DelimiterOptions.Comma, TextQualifier.Quote, true);
        }

        /// <summary>
        ///     The serialize.
        /// </summary>
        /// <returns>
        ///     The <see cref="XElement" />.
        /// </returns>
        public XElement Serialize()
        {
            return new XElement(
                this.SettingsName, 
                new XAttribute("delimiter", this.Delimiter), 
                new XAttribute("textQualifier", this.TextQualifier), 
                new XAttribute("treatConsecutiveAsOne", this.TreatConsecutiveAsOne));
        }

        #endregion
    }
}