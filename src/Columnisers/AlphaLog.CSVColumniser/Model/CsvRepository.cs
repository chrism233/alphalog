﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CsvDatabase.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The csv database.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.CSVColumniser.Model
{
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Linq;
    using System.Xml.Linq;

    using AlphaLog.UI.Common;

    /// <summary>
    ///     The csv database.
    /// </summary>
    [Export]
    public class CsvRepository : Repository<CsvItem>
    {
        #region Constants

        /// <summary>
        ///     The csv settings name.
        /// </summary>
        private const string CsvSettingsName = "CsvItems";

        #endregion

        #region Fields

        /// <summary>
        ///     The csv items.
        /// </summary>
        private Dictionary<string, CsvItem> csvItems = new Dictionary<string, CsvItem>();

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the settings name.
        /// </summary>
        public override string SettingsName
        {
            get
            {
                return CsvSettingsName;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Get an enumeration of csv items.
        /// </summary>
        /// <returns>
        ///     An <see cref="IEnumerable{CsvItems}" /> of csv items.
        /// </returns>
        public override IEnumerable<CsvItem> GetItems()
        {
            return this.csvItems.Values;
        }

        /// <summary>
        /// The initialize.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        public override void Initialize(XElement element)
        {
            this.csvItems = element.Elements(CsvItem.ItemName)
                .Select(e => new CsvItem(e))
                .ToDictionary(k => k.Name, v => v);
        }

        /// <summary>
        ///     Serialises the class to xml.
        /// </summary>
        /// <returns>
        ///     The <see cref="XElement" />.
        /// </returns>
        public override XElement Serialize()
        {
            return new XElement(CsvSettingsName, this.GetItems().Select(r => r.Serialize()));
        }

        #endregion

        #region Methods

        /// <summary>
        /// The add csv.
        /// </summary>
        /// <param name="items">
        /// The items.
        /// </param>
        protected override void StoreCore(IEnumerable<CsvItem> items)
        {
            this.csvItems = items.ToList().ToDictionary(k => k.Name, v => v);
        }

        #endregion
    }
}