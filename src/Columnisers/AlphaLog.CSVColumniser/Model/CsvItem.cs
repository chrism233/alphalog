﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CsvItem.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The csv item.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.CSVColumniser.Model
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    using AlphaLog.UI.Common;
    using AlphaLog.UI.Common.Settings.Model;

    /// <summary>
    ///     The csv item.
    /// </summary>
    public class CsvItem : IPersist
    {
        #region Constants

        /// <summary>
        ///     The item name.
        /// </summary>
        public const string ItemName = "CsvItem";

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CsvItem"/> class.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        public CsvItem(XElement element)
            : this(
                element.Attribute("name").Value, 
                new DelimiterSettings(element.Element("Settings")), 
                element.Element("Fields").Elements().Select(e => new SourceField(e)))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CsvItem"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="fields">
        /// The fields.
        /// </param>
        public CsvItem(string name, DelimiterSettings settings, IEnumerable<SourceField> fields)
        {
            this.Name = name;
            this.Settings = settings;
            this.SourceFields = new List<SourceField>(fields);
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Gets or sets the <see cref="DelimiterSettings" />.
        /// </summary>
        public DelimiterSettings Settings { get; set; }

        /// <summary>
        ///     Gets the settings name.
        /// </summary>
        public string SettingsName
        {
            get
            {
                return "CsvItem";
            }
        }

        /// <summary>
        ///     Gets or sets the source fields
        /// </summary>
        public IEnumerable<SourceField> SourceFields { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The create.
        /// </summary>
        /// <returns>
        ///     The <see cref="CsvItem" />.
        /// </returns>
        public static CsvItem Create()
        {
            return new CsvItem(string.Empty, DelimiterSettings.GetDefault(), new List<SourceField>());
        }

        /// <summary>
        /// The add fields.
        /// </summary>
        /// <param name="field">
        /// The field.
        /// </param>
        public void AddFields(SourceField field)
        {
        }

        /// <summary>
        ///     The serialize.
        /// </summary>
        /// <returns>
        ///     The <see cref="XElement" />.
        /// </returns>
        public XElement Serialize()
        {
            return new XElement(
                this.SettingsName, 
                new XAttribute("name", this.Name), 
                this.Settings.Serialize(), 
                new XElement("Fields", this.SourceFields.Select(f => f.Serialize())));
        }

        #endregion
    }
}