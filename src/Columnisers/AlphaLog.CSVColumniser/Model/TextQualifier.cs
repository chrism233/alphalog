// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TextQualifier.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The text qualifier.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.CSVColumniser.Model
{
    using System.ComponentModel;

    /// <summary>
    ///     The text qualifier.
    /// </summary>
    public enum TextQualifier
    {
        /// <summary>
        ///     The none.
        /// </summary>
        [Description("{none}")]
        None, 

        /// <summary>
        ///     The quote.
        /// </summary>
        [Description(@"""")]
        Quote, 

        /// <summary>
        ///     The single quote.
        /// </summary>
        [Description("'")]
        SingleQuote
    }
}