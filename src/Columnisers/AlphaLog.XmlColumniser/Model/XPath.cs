﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XPath.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The XPath.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.XmlColumniser.Model
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    using AlphaLog.UI.Common;

    using Microsoft.Practices.Prism.Mvvm;

    /// <summary>
    ///     The XPath.
    /// </summary>
    public class XPath : BindableBase, IPersist
    {
        // TODO think about merging this with the parent class.
        #region Constants

        /// <summary>
        ///     The x path name.
        /// </summary>
        public const string XPathName = "XPath";

        #endregion

        #region Fields

        /// <summary>
        ///     The name.
        /// </summary>
        private string name;

        /// <summary>
        ///     The xpath items.
        /// </summary>
        private IEnumerable<XPathItem> xpathItems;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="XPath"/> class.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        public XPath(XElement element)
            : this(
                element.Attribute("name").Value, 
                element.Elements(XPathItem.XPathItemName).Select(e => new XPathItem(e)))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="XPath"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="xpath">
        /// The xpath.
        /// </param>
        public XPath(string name, IEnumerable<XPathItem> xpath)
        {
            this.name = name;
            this.xpathItems = xpath;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.SetProperty(ref this.name, value);
            }
        }

        /// <summary>
        /// Gets the settings name.
        /// </summary>
        public string SettingsName
        {
            get
            {
                return XPathName;
            }
        }

        /// <summary>
        ///     Gets or sets the x path items.
        /// </summary>
        public IEnumerable<XPathItem> XPathItems
        {
            get
            {
                return this.xpathItems;
            }

            set
            {
                this.SetProperty(ref this.xpathItems, value);
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The serialize.
        /// </summary>
        /// <returns>
        ///     The <see cref="XElement" />.
        /// </returns>
        public XElement Serialize()
        {
            return new XElement(
                XPathName, 
                new XAttribute("name", this.SettingsName), 
                this.XPathItems.Select(i => i.Serialize()));
        }

        #endregion
    }
}