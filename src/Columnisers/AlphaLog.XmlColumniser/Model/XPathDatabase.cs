// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XPathDatabase.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The x path database.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.XmlColumniser.Model
{
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Linq;
    using System.Xml.Linq;

    using AlphaLog.UI.Common;

    /// <summary>
    ///     The x path database.
    /// </summary>
    [Export]
    public class XPathDatabase : Repository<XPath>
    {
        #region Constants

        /// <summary>
        ///     The x path settings name.
        /// </summary>
        public const string XPathSettingsName = "XPaths";

        #endregion

        #region Fields

        /// <summary>
        ///     The xpath dictionary.
        /// </summary>
        private Dictionary<string, XPath> xpathDictionary = new Dictionary<string, XPath>();

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the settings name.
        /// </summary>
        public override string SettingsName
        {
            get
            {
                return XPathSettingsName;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The get x path.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerable{XPath}" />.
        /// </returns>
        public override IEnumerable<XPath> GetItems()
        {
            return this.xpathDictionary.Values;
        }

        /// <summary>
        /// The initialize.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        public override void Initialize(XElement element)
        {
            if (element != null)
            {
                this.xpathDictionary =
                    element.Elements(XPath.XPathName).Select(e => new XPath(e)).ToDictionary(k => k.Name, v => v);
            }
        }

        /// <summary>
        ///     The serialize.
        /// </summary>
        /// <returns>
        ///     The <see cref="XElement" />.
        /// </returns>
        public override XElement Serialize()
        {
            return new XElement(XPathSettingsName, this.xpathDictionary.Values.Select(v => v.Serialize()));
        }

        #endregion

        #region Methods

        /// <summary>
        /// Store the items
        /// </summary>
        /// <param name="items">
        /// The items to store.
        /// </param>
        protected override void StoreCore(IEnumerable<XPath> items)
        {
            IEnumerable<XPath> itemsToRemove = this.xpathDictionary.Values.Except(items);
            foreach (XPath item in itemsToRemove.ToList())
            {
                this.xpathDictionary.Remove(item.Name);
            }

            foreach (XPath xpath in items.ToList())
            {
                if (this.xpathDictionary.ContainsKey(xpath.Name))
                {
                    this.xpathDictionary[xpath.Name] = xpath;
                }
                else
                {
                    this.xpathDictionary.Add(xpath.Name, xpath);
                }
            }
        }

        #endregion
    }
}