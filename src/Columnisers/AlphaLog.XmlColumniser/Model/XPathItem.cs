﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XPathItem.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The x path item.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.XmlColumniser.Model
{
    using System.Xml.Linq;

    using AlphaLog.UI.Common;

    using Microsoft.Practices.Prism.Mvvm;

    /// <summary>
    ///     The x path item.
    /// </summary>
    public class XPathItem : BindableBase, IPersist
    {
        #region Constants

        /// <summary>
        ///     The x path item name.
        /// </summary>
        public const string XPathItemName = "XPathItem";

        #endregion

        #region Fields

        /// <summary>
        ///     The field.
        /// </summary>
        private string field;

        /// <summary>
        ///     The value.
        /// </summary>
        private string value;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="XPathItem"/> class.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        public XPathItem(XElement element)
            : this(element.Attribute("field").Value, element.Attribute("value").Value)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="XPathItem"/> class.
        /// </summary>
        /// <param name="field">
        /// The field.
        /// </param>
        /// <param name="xpath">
        /// The xpath.
        /// </param>
        public XPathItem(string field, string xpath)
        {
            this.field = field;
            this.value = xpath;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the field.
        /// </summary>
        public string Field
        {
            get
            {
                return this.field;
            }

            set
            {
                this.SetProperty(ref this.field, value);
            }
        }

        /// <summary>
        ///     Gets the settings name.
        /// </summary>
        public string SettingsName
        {
            get
            {
                return XPathItemName;
            }
        }

        /// <summary>
        ///     Gets or sets the value.
        /// </summary>
        public string Value
        {
            get
            {
                return this.value;
            }

            set
            {
                this.SetProperty(ref this.value, value);
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The serialize.
        /// </summary>
        /// <returns>
        ///     The <see cref="XElement" />.
        /// </returns>
        public XElement Serialize()
        {
            return new XElement(
                this.SettingsName, 
                new XAttribute("field", this.Field), 
                new XAttribute("value", this.Value));
        }

        #endregion
    }
}