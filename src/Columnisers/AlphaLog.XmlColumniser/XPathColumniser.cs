﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XPathColumniser.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The x path columniser.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.XmlColumniser
{
    using System;
    using System.Collections.Generic;

    using AlphaLog.Columniser;

    /// <summary>
    ///     The x path columniser.
    /// </summary>
    public class XPathColumniser : Columnizer
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="XPathColumniser"/> class.
        /// </summary>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        public XPathColumniser(string filePath)
            : base(filePath)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the name.
        /// </summary>
        public override string Name
        {
            get
            {
                return "XPath";
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The get columns.
        /// </summary>
        /// <returns>
        ///     The <see cref="Dictionary" />.
        /// </returns>
        public override Dictionary<string, Type> GetColumns()
        {
            return new Dictionary<string, Type>();
        }

        #endregion

        #region Methods

        /// <summary>
        /// The update core.
        /// </summary>
        /// <param name="observer">
        /// The observer.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// The method has not been implemented.
        /// </exception>
        protected override void UpdateCore(IObserver<Dictionary<string, object>> observer)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}