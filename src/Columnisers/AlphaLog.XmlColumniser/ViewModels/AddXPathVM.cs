﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AddXPathVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The add x path vm.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.XmlColumniser.ViewModels
{
    using System.ComponentModel;
    using System.ComponentModel.Composition;
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;

    using AlphaLog.UI.Common;
    using AlphaLog.XmlColumniser.Model;

    using Microsoft.Practices.Prism.Commands;

    /// <summary>
    ///     The add x path vm.
    /// </summary>
    [Export]
    public class AddXPathVM : TabSettingsVM
    {
        #region Fields

        /// <summary>
        ///     The database.
        /// </summary>
        private readonly XPathDatabase database;

        /// <summary>
        ///     The adding newxpath.
        /// </summary>
        private bool addingNewxpath;

        /// <summary>
        ///     The selected x path item.
        /// </summary>
        private XPath selectedXPathItem;

        /// <summary>
        ///     The xpath items.
        /// </summary>
        private ObservableCollectionWithPropertyChanged<XPathItem> xpathItems;

        /// <summary>
        ///     The xpaths.
        /// </summary>
        private ObservableCollectionWithPropertyChanged<XPath> xpaths;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AddXPathVM"/> class.
        ///     Initializes a new instance of the <see cref="AddRegexVM"/> class.
        /// </summary>
        /// <param name="settingsManager">
        /// The settings Manager.
        /// </param>
        [ImportingConstructor]
        public AddXPathVM(ISettingsManager settingsManager)
            : base("XML")
        {
            this.database = settingsManager.GetsSetting<XPathDatabase>();
            this.AddXPathCommand = new DelegateCommand(this.AddExecuted);
            this.RemoveXPathCommand = new DelegateCommand<XPath>(this.RemoveExecuted);
            this.CancelXPathCommand = new DelegateCommand(this.CancelXPath);
            this.XPaths = new ObservableCollectionWithPropertyChanged<XPath>(this.database.GetItems());
            this.XPaths.ItemPropertyChanged += this.OnHasChanges;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the add XPath command.
        /// </summary>
        public ICommand AddXPathCommand { get; protected set; }

        /// <summary>
        ///     Gets or sets a value indicating whether adding new XPath.
        /// </summary>
        public bool AddingNewXPath
        {
            get
            {
                return this.addingNewxpath;
            }

            set
            {
                this.SetProperty(ref this.addingNewxpath, value);
            }
        }

        /// <summary>
        ///     Gets or sets the cancel XPath command.
        /// </summary>
        public ICommand CancelXPathCommand { get; protected set; }

        /// <summary>
        ///     Gets or sets the remove XPath command.
        /// </summary>
        public DelegateCommand<XPath> RemoveXPathCommand { get; protected set; }

        /// <summary>
        ///     Gets or sets the save XPath command.
        /// </summary>
        public ICommand SaveXPathCommand { get; protected set; }

        /// <summary>
        ///     Gets or sets the selected XPath item.
        /// </summary>
        public XPath SelectedXPathItem
        {
            get
            {
                return this.selectedXPathItem;
            }

            set
            {
                this.SetProperty(ref this.selectedXPathItem, value);
                this.RemoveXPathCommand.RaiseCanExecuteChanged();
                this.XPathItems =
                    new ObservableCollectionWithPropertyChanged<XPathItem>(this.selectedXPathItem.XPathItems);

                // this.XPathItems.ItemPropertyChanged += xpathItems_ItemPropertyChanged;
                WeakEventManager<ObservableCollectionWithPropertyChanged<XPathItem>, PropertyChangedEventArgs>
                    .AddHandler(this.XPathItems, "ItemPropertyChanged", this.OnHasChanges);
            }
        }

        /// <summary>
        ///     Gets or sets the x path items.
        /// </summary>
        public ObservableCollectionWithPropertyChanged<XPathItem> XPathItems
        {
            get
            {
                return this.xpathItems;
            }

            set
            {
                this.SetProperty(ref this.xpathItems, value);
            }
        }

        /// <summary>
        ///     Gets or sets the XPath items.
        /// </summary>
        public ObservableCollectionWithPropertyChanged<XPath> XPaths
        {
            get
            {
                return this.xpaths;
            }

            set
            {
                this.SetProperty(ref this.xpaths, value);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     The save changes.
        /// </summary>
        protected override void SaveChanges()
        {
            base.SaveChanges();
            this.AddingNewXPath = false;
            this.database.Store(this.XPaths.ToArray());
        }

        /// <summary>
        ///     The add executed.
        /// </summary>
        private void AddExecuted()
        {
            if (this.AddingNewXPath)
            {
                this.XPaths.Add(this.SelectedXPathItem);
                this.AddingNewXPath = false;
                this.OnPropertyChanged("SelectedXPathItem");
            }
            else
            {
                this.AddingNewXPath = true;
            }
        }

        /// <summary>
        ///     The cancel XPath.
        /// </summary>
        private void CancelXPath()
        {
            this.AddingNewXPath = false;
            this.SelectedXPathItem = this.XPaths.First();
        }

        /// <summary>
        /// The on has changes.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void OnHasChanges(object sender, PropertyChangedEventArgs e)
        {
            this.HasUnSavedChanges = true;
        }

        /// <summary>
        /// The remove executed.
        /// </summary>
        /// <param name="itemToRemove">
        /// The item To Remove.
        /// </param>
        private void RemoveExecuted(XPath itemToRemove)
        {
            this.XPaths.Remove(itemToRemove);
        }

        #endregion
    }
}