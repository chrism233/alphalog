﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XPathDescriptor.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The XPath columniser.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.XmlColumniser
{
    using System.ComponentModel.Composition;

    using AlphaLog.Columniser;
    using AlphaLog.UI.Common;
    using AlphaLog.XmlColumniser.Model;

    /// <summary>
    ///     The XPath columniser.
    /// </summary>
    [Export(typeof(IColumnizerDescriptor))]
    public class XPathDescriptor : ColumnizerDescriptor<XPath>
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="XPathDescriptor"/> class.
        /// </summary>
        /// <param name="settingsManager">
        /// The settings Manager.
        /// </param>
        [ImportingConstructor]
        public XPathDescriptor(ISettingsManager settingsManager)
            :base(settingsManager.GetsSetting<XPathDatabase>())
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The can columnizer file.
        /// </summary>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool CanColumnize(string filePath)
        {
            return false;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The do create.
        /// </summary>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        /// <returns>
        /// The <see cref="IColumnizer"/>.
        /// </returns>
        protected override IColumnizer DoCreate(string filePath)
        {
            return new XPathColumniser(filePath);
        }

        #endregion
    }
}