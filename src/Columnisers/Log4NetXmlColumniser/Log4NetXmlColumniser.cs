﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Log4NetXmlColumniser.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The log 4 net columniser.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Log4NetXmlColumniser
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml;

    using AlphaLog.Columniser;

    /// <summary>
    ///     The log 4 net columniser.
    /// </summary>
    public class Log4NetColumniser : Columnizer
    {
        #region Fields

        /// <summary>
        ///     The pc.
        /// </summary>
        private readonly XmlParserContext pc;

        /// <summary>
        ///     The reader.
        /// </summary>
        private readonly StreamReader reader;

        /// <summary>
        ///     The settings.
        /// </summary>
        private readonly XmlReaderSettings settings;

        /// <summary>
        ///     The xml reader.
        /// </summary>
        private XmlReader xmlReader;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Log4NetColumniser"/> class.
        /// </summary>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        public Log4NetColumniser(string filePath)
            : base(filePath)
        {
            this.settings = new XmlReaderSettings { ConformanceLevel = ConformanceLevel.Fragment };
            var nt = new NameTable();
            var mgr = new XmlNamespaceManager(nt);
            mgr.AddNamespace("log4j", "http://jakarta.apache.org/log4j");
            this.pc = new XmlParserContext(nt, mgr, string.Empty, XmlSpace.Default);
            var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Delete | FileShare.ReadWrite);
            this.reader = new StreamReader(stream, Encoding.Default, true);
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the name.
        /// </summary>
        public override string Name
        {
            get
            {
                return "Log4Net";
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The column name to type mappings.
        /// </summary>
        /// <returns>
        ///     A <see cref="Dictionary{String, Type}" /> mappings column names to their types.
        /// </returns>
        public override Dictionary<string, Type> GetColumns()
        {
            return new Dictionary<string, Type>
                       {
                           { "Timestamp", typeof(DateTime) }, 
                           { "Thread", typeof(string) }, 
                           { "Level", typeof(string) }, 
                           { "Message", typeof(string) }, 
                           { "MachineName", typeof(string) }, 
                           { "HostName", typeof(string) }, 
                           { "UserName", typeof(string) }, 
                           { "App", typeof(string) }, 
                           { "Class", typeof(string) }, 
                           { "Method", typeof(string) }, 
                           { "File", typeof(string) }, 
                           { "Line", typeof(string) }, 
                       };
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get entries.
        /// </summary>
        /// <param name="observer">
        /// The observer.
        /// </param>
        protected override void UpdateCore(IObserver<Dictionary<string, object>> observer)
        {
            this.xmlReader = XmlReader.Create(this.reader, this.settings, this.pc);
            while (this.xmlReader.Read())
            {
                var logentry = new Dictionary<string, object>();
                if ((this.xmlReader.NodeType == XmlNodeType.Element) && (this.xmlReader.Name == "log4j:event"))
                {
                    double epoc = Convert.ToDouble(this.xmlReader.GetAttribute("timestamp"));
                    logentry.Add("Timestamp", new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(epoc).ToLocalTime());
                    logentry.Add("Thread", this.xmlReader.GetAttribute("thread"));
                    logentry.Add("Level", this.xmlReader.GetAttribute("level"));
                    while (this.xmlReader.Read())
                    {
                        // end element
                        if (this.xmlReader.Name == "log4j:event")
                        {
                            break;
                        }

                        switch (this.xmlReader.Name)
                        {
                            case "log4j:message":
                                logentry.Add("Message", this.xmlReader.ReadString());
                                break;
                            case "log4j:data":
                                switch (this.xmlReader.GetAttribute("name"))
                                {
                                    case "log4jmachinename":
                                        logentry.Add("MachineName", this.xmlReader.GetAttribute("value"));
                                        break;
                                    case "log4net:HostName":
                                        logentry.Add("HostName", this.xmlReader.GetAttribute("value"));
                                        break;
                                    case "log4net:UserName":
                                        logentry.Add("UserName", this.xmlReader.GetAttribute("value"));
                                        break;
                                    case "log4japp":
                                        logentry.Add("App", this.xmlReader.GetAttribute("value"));
                                        break;
                                }

                                break;
                            case "log4j:throwable":
                                logentry.Add("Throwable", this.xmlReader.ReadString());
                                break;
                            case "log4j:locationInfo":
                                logentry.Add("Class", this.xmlReader.GetAttribute("class"));
                                logentry.Add("Method", this.xmlReader.GetAttribute("method"));
                                logentry.Add("File", this.xmlReader.GetAttribute("file"));
                                logentry.Add("Line", this.xmlReader.GetAttribute("line"));
                                break;
                        }
                    }

                    observer.OnNext(logentry);
                }
            }
        }

        #endregion
    }
}