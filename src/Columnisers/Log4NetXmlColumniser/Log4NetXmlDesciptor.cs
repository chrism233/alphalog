// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Log4NetXmlDesciptor.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The log 4 net xml columniser.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Log4NetXmlColumniser
{
    using System.ComponentModel.Composition;
    using System.IO;

    using AlphaLog.Columniser;

    /// <summary>
    ///     The log 4 net xml columniser.
    /// </summary>
    [Export(typeof(IColumnizerDescriptor))]
    public class Log4NetXmlDesciptor : IColumnizerDescriptor
    {
        #region Public Methods and Operators

        /// <summary>
        /// The can columnizer file.
        /// </summary>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool CanColumnize(string filePath)
        {
            bool canColumnize = false;
            using (var s = new StreamReader(new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Delete | FileShare.ReadWrite)))
            {
                string line = s.ReadLine();
                if (line != null)
                {
                    canColumnize = line.Contains("log4j:event");
                }
            }

            return canColumnize;
        }

        /// <summary>
        /// Creates and instance of a columniser.
        /// </summary>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        /// <returns>
        /// The <see cref="IColumnizer"/>.
        /// </returns>
        public IColumnizer Create(string filePath)
        {
            return new Log4NetColumniser(filePath);
        }

        #endregion
    }
}