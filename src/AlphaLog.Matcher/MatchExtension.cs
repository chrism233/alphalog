﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchExtension.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The match extension.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Matcher
{
    using System;

    using AlphaLog.Matcher.Matchers.Group;

    /// <summary>
    ///     The match extension.
    /// </summary>
    public static class MatchExtension
    {
        #region Public Methods and Operators

        /// <summary>
        /// The or.
        /// </summary>
        /// <param name="match">
        /// The match.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <returns>
        /// The <see cref="IMatch"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// The <paramref name="match"/> is null.
        /// </exception>
        public static IMatch Or(this MatchBase match, MatcherType type)
        {
            if (match == null)
            {
                throw new ArgumentNullException("match");
            }

            return new Or(match, MatchFactory.Create(type, match.SearchValue, match.SearchField));
        }

        #endregion
    }
}