﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchBase.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   A base class for matchers.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Matcher
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;

    /// <summary>
    ///     A base class for matchers.
    /// </summary>
    public abstract class MatchBase : IMatch
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MatchBase"/> class.
        /// </summary>
        /// <param name="searchValue">
        /// The value to search for.
        /// </param>
        /// <param name="searchField">
        /// The field to check.
        /// </param>
        protected MatchBase(string searchValue, string searchField)
        {
            this.SearchValue = searchValue;
            this.SearchField = searchField;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MatchBase"/> class.
        /// </summary>
        /// <param name="matcher">
        /// The XML representation of this matcher.
        /// </param>
        protected MatchBase(XElement matcher)
        {
            if (matcher == null)
            {
                throw new ArgumentNullException("matcher");
            }

            this.SearchValue = matcher.Attribute("searchValue").Value;
            this.SearchField = matcher.Attribute("searchField").Value;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets The current item to search.
        /// </summary>
        public IDictionary<string, object> Item { get; private set; }

        /// <summary>
        ///     Gets the match type.
        /// </summary>
        public abstract MatcherType MatchType { get; }

        /// <summary>
        ///     Gets the item in the dictionary to match against.
        /// </summary>
        public string SearchField { get; private set; }

        /// <summary>
        ///     Gets the search value.
        /// </summary>
        public string SearchValue { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Can this item be matched.
        /// </summary>
        /// <param name="items">
        /// The items to check.
        /// </param>
        /// <returns>
        /// Returns <c>true</c> if the dictionary contains all of the required search fields; otherwise <c>false</c>.
        /// </returns>
        public bool CanMatch(Dictionary<string, object> items)
        {
            return items.ContainsKey(this.SearchField) && items[this.SearchField] != null;
        }

        /// <summary>
        ///     The description.
        /// </summary>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public abstract string Description();

        /// <summary>
        /// Is there a match for this record.
        /// </summary>
        /// <param name="logItems">
        /// The log item to process.
        /// </param>
        /// <returns>
        /// <c>true</c> if a match is found.
        /// </returns>
        public bool IsMatch(IDictionary<string, object> logItems)
        {
            if (logItems == null)
            {
                throw new ArgumentNullException("logItems");
            }

            if (!logItems.ContainsKey(this.SearchField))
            {
                throw new ArgumentException(
                    @"The provided logItems does not contain the specified search field.", 
                    "logItems");
            }

            this.Item = logItems;

            return this.IsMatch(logItems[this.SearchField]);
        }

        /// <summary>
        /// Is there a match for this item.
        /// </summary>
        /// <param name="logItem">
        /// The item to match.
        /// </param>
        /// <returns>
        /// <c>true</c> if a match is found.
        /// </returns>
        public abstract bool IsMatch(object logItem);

        /// <summary>
        ///     The serialize.
        /// </summary>
        /// <returns>
        ///     The <see cref="XElement" />.
        /// </returns>
        public abstract XElement Serialize();

        #endregion
    }
}