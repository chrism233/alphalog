﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatcherHelpers.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The matcher category.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Matcher
{
    using System;

    /// <summary>
    ///     The matcher category.
    /// </summary>
    public enum MatcherCategory
    {
        /// <summary>
        ///     Miscellaneous category.
        /// </summary>
        Misc, 

        /// <summary>
        ///     Text category.
        /// </summary>
        Text, 

        /// <summary>
        ///     Numeric category
        /// </summary>
        Numeric, 

        /// <summary>
        ///     Group category.
        /// </summary>
        Group
    }

    /// <summary>
    ///     The matcher helpers.
    /// </summary>
    public static class MatcherHelpers
    {
        #region Public Methods and Operators

        /// <summary>
        /// Converts the <see cref="MatcherType"/> to its <see cref="MatcherCategory"/>
        /// </summary>
        /// <param name="matcherType">
        /// The matcher type.
        /// </param>
        /// <returns>
        /// The <see cref="MatcherCategory"/> of the provided <see cref="MatcherType"/>.
        /// </returns>
        /// <exception cref="ArgumentException">
        /// The <see cref="MatcherType"/> is not supported.
        /// </exception>
        public static MatcherCategory MatcherTypeToCategory(MatcherType matcherType)
        {
            switch (matcherType)
            {
                case MatcherType.Always:
                    return MatcherCategory.Misc;

                case MatcherType.Contains:
                case MatcherType.EndsWith:
                case MatcherType.StartsWith:
                case MatcherType.Exact:
                case MatcherType.Regex:
                case MatcherType.Wildcard:
                    return MatcherCategory.Text;

                case MatcherType.EqualTo:
                case MatcherType.GreaterThan:
                case MatcherType.LessThan:
                    return MatcherCategory.Numeric;

                case MatcherType.And:
                case MatcherType.Or:
                    return MatcherCategory.Group;
                default:
                    throw new ArgumentException("matcherType");
            }
        }

        #endregion
    }
}