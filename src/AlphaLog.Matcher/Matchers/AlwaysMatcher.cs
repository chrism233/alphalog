﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AlwaysMatcher.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The always matcher.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Matcher.Matchers
{
    using System.Collections.Generic;
    using System.Xml.Linq;

    /// <summary>
    ///     The always matcher.
    /// </summary>
    public class AlwaysMatcher : IMatch
    {
        #region Public Properties

        /// <summary>
        ///     Gets the match type.
        /// </summary>
        public MatcherType MatchType
        {
            get
            {
                return MatcherType.Always;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The can match.
        /// </summary>
        /// <param name="items">
        /// The items.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool CanMatch(Dictionary<string, object> items)
        {
            return true;
        }

        /// <summary>
        ///     The description.
        /// </summary>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public string Description()
        {
            return string.Format("{0}()", this.MatchType);
        }

        /// <summary>
        /// Is this a match.
        /// </summary>
        /// <param name="logItems">
        /// The log item to process.
        /// </param>
        /// <returns>
        /// <c>true</c> if a match is found.
        /// </returns>
        public bool IsMatch(IDictionary<string, object> logItems)
        {
            return true;
        }

        /// <summary>
        ///     Serialize the type to XML.
        /// </summary>
        /// <returns>
        ///     The <see cref="XElement" />.
        /// </returns>
        public XElement Serialize()
        {
            return new XElement(this.MatchType.ToString());
        }

        #endregion
    }
}