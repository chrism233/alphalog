﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GreaterThanMatcher.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The greater than matcher.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Matcher.Matchers.Numeric
{
    using System.Globalization;
    using System.Xml.Linq;

    /// <summary>
    ///     The greater than matcher.
    /// </summary>
    public class GreaterThanMatcher : NumericBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GreaterThanMatcher"/> class.
        /// </summary>
        /// <param name="searchValue">
        /// The value to search for.
        /// </param>
        /// <param name="field">
        /// The field to search.
        /// </param>
        public GreaterThanMatcher(string searchValue, string field)
            : base(searchValue, field)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GreaterThanMatcher"/> class.
        /// </summary>
        /// <param name="matcher">
        /// XML representation of the matcher.
        /// </param>
        public GreaterThanMatcher(XElement matcher)
            : base(matcher)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the match type.
        /// </summary>
        public override MatcherType MatchType
        {
            get
            {
                return MatcherType.GreaterThan;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Is there a match for this item.
        /// </summary>
        /// <param name="itemToMatch">
        /// The item to match.
        /// </param>
        /// <returns>
        /// <c>true</c> if a match is found.
        /// </returns>
        protected override bool IsMatch(double itemToMatch)
        {
            return itemToMatch > double.Parse(this.SearchValue, CultureInfo.InvariantCulture);
        }

        #endregion
    }
}