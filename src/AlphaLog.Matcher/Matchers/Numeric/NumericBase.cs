﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NumericBase.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   A base class for numeric matchers.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Matcher.Matchers.Numeric
{
    using System;
    using System.Globalization;
    using System.Xml.Linq;

    /// <summary>
    ///     A base class for numeric matchers.
    /// </summary>
    public abstract class NumericBase : MatchBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="NumericBase"/> class.
        /// </summary>
        /// <param name="searchValue">
        /// The value to search for.
        /// </param>
        /// <param name="searchField">
        /// The field to search.
        /// </param>
        protected NumericBase(string searchValue, string searchField)
            : base(searchValue, searchField)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NumericBase"/> class.
        /// </summary>
        /// <param name="matcher">
        /// XML representation of the matcher.
        /// </param>
        protected NumericBase(XElement matcher)
            : base(matcher)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the match type.
        /// </summary>
        public abstract override MatcherType MatchType { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The description.
        /// </summary>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public override string Description()
        {
            return string.Format("{0}.{1}({2})", this.SearchField, this.MatchType, this.SearchValue);
        }

        /// <summary>
        /// Is there a match for this record.
        /// </summary>
        /// <param name="logItem">
        /// The log item to match.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool IsMatch(object logItem)
        {
            if (!(IsNumber(logItem) | logItem is DateTime))
            {
                throw new ArgumentException("logItem");
            }

            if (logItem is DateTime)
            {
                return this.IsMatch(((DateTime)logItem).ToUnixTime());
            }

            return this.IsMatch(Convert.ToDouble(logItem, CultureInfo.InvariantCulture));
        }

        /// <summary>
        ///     Serialize this type to XML.
        /// </summary>
        /// <returns>Returns serialized data.</returns>
        public override XElement Serialize()
        {
            return new XElement(
                this.MatchType.ToString(), 
                new XAttribute("searchField", this.SearchField), 
                new XAttribute("searchValue", this.SearchValue));
        }

        #endregion

        #region Methods

        /// <summary>
        /// Is there a match for this record.
        /// </summary>
        /// <param name="itemToMatch">
        /// The item to match.
        /// </param>
        /// <returns>
        /// Returns <c>true</c> if <paramref name="itemToMatch"/> is a matcher; otherwise <c>false</c>.
        /// </returns>
        protected abstract bool IsMatch(double itemToMatch);

        /// <summary>
        /// Is this a number type.
        /// </summary>
        /// <param name="value">
        /// The object to check.
        /// </param>
        /// <returns>
        /// Returns <c>true</c> if the object is a number; otherwise <c>false</c>..
        /// </returns>
        private static bool IsNumber(object value)
        {
            return value is sbyte || value is byte || value is short || value is ushort || value is int || value is uint
                   || value is long || value is ulong || value is float || value is double || value is decimal;
        }

        #endregion
    }
}