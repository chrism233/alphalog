﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NumericBase.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   A base class for numeric matchers.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AlphaLog.Matcher.Matchers.DateTime
{
    using System;
    using System.Xml.Linq;

    /// <summary>
    ///     A base class for numeric matchers.
    /// </summary>
    public abstract class DateTimeBase : MatchBase
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initialize a new instance of <see cref="DateTimeBase"/> class.
        /// </summary>
        /// <param name="searchValue">
        /// </param>
        /// <param name="searchField">
        /// </param>
        protected DateTimeBase(string searchValue, string searchField)
            : base(searchValue, searchField)
        {
        }

        /// <summary>
        ///     Initialize a new instance of <c ref="NumericBase"/> class.
        /// </summary>
        /// <param name="matcher">
        /// XML representation of the matcher.
        /// </param>
        protected DateTimeBase(XElement matcher)
            : base(matcher)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the match type.
        /// </summary>
        public abstract override MatcherType MatchType { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Is there a match for this record.
        /// </summary>
        /// <param name="logItem">
        /// The log item to match.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool IsMatch(object logItem)
        {
            if (!IsDateTime(logItem))
            {
                throw new ArgumentException("logItem");
            }

            return this.IsMatch((System.DateTime)logItem);
        }

        /// <summary>
        ///     Serialize this type to XML.
        /// </summary>
        /// <returns>Returns serialized data.</returns>
        public override XElement Serialize()
        {
            return new XElement(
                this.MatchType.ToString(), 
                new XAttribute("searchField", this.SearchField), 
                new XAttribute("searchValue", this.SearchValue));
        }

        #endregion

        #region Methods

        /// <summary>
        /// Is there a match for this record.
        /// </summary>
        /// <param name="itemToMatch">
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        protected abstract bool IsMatch(DateTime itemToMatch);

        /// <summary>
        /// Is this a number type.
        /// </summary>
        /// <param name="value">
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool IsDateTime(object value)
        {
            return value is DateTime;
        }

        #endregion
    }
}