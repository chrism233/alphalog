﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EqualToMatcher.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   Is this item equal to a provided value.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Matcher.Matchers.Numeric
{
    using System.Globalization;
    using System.Xml.Linq;

    /// <summary>
    ///     Is this item equal to a provided value.
    /// </summary>
    public class EqualToMatcher : NumericBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EqualToMatcher"/> class.
        /// </summary>
        /// <param name="searchValue">
        /// The value to search for.
        /// </param>
        /// <param name="field">
        /// The field to search.
        /// </param>
        public EqualToMatcher(string searchValue, string field)
            : base(searchValue, field)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EqualToMatcher"/> class.
        /// </summary>
        /// <param name="matcher">
        /// XML representation of the matcher.
        /// </param>
        public EqualToMatcher(XElement matcher)
            : base(matcher)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the match type.
        /// </summary>
        public override MatcherType MatchType
        {
            get
            {
                return MatcherType.EqualTo;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Is this a match.
        /// </summary>
        /// <param name="itemToMatch">
        /// The value to match.
        /// </param>
        /// <returns>
        /// Returns <c>true</c> if a match is found; otherwise <c>false</c>.
        /// </returns>
        protected override bool IsMatch(double itemToMatch)
        {
            return itemToMatch == double.Parse(this.SearchValue, CultureInfo.InvariantCulture);
        }

        #endregion
    }
}