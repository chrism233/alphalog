﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="And.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   And matcher, all the matchers in this group should match.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Matcher.Matchers.Group
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Xml.Linq;

    /// <summary>
    ///     And matcher, all the matchers in this group should match.
    /// </summary>
    [SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "And", 
        Justification = "This is a logical name for the type.")]
    public class And : GroupBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="And"/> class.
        /// </summary>
        /// <param name="matchers">
        /// The matchers to add.
        /// </param>
        public And(params IMatch[] matchers)
            : base(matchers)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="And" /> class.
        /// </summary>
        public And()
            : base(new List<IMatch>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="And"/> class.
        /// </summary>
        /// <param name="matcher">
        /// The matcher.
        /// </param>
        public And(XElement matcher)
            : base(matcher)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the match type.
        /// </summary>
        public override MatcherType MatchType
        {
            get
            {
                return MatcherType.And;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Is this a match
        /// </summary>
        /// <param name="logItems">
        /// The log item to process.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool IsMatch(IDictionary<string, object> logItems)
        {
            if (this.Matchers.Count == 0)
            {
                throw new ArgumentException("Matchers");
            }

            return this.Matchers.All(m => m.IsMatch(logItems));
        }

        #endregion
    }
}