﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Or.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The or.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Matcher.Matchers.Group
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Xml.Linq;

    /// <summary>
    ///     The or.
    /// </summary>
    [SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Or", 
        Justification = "This is a logical name for the type.")]
    public class Or : GroupBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Or"/> class.
        /// </summary>
        /// <param name="matchers">
        /// The matchers to add.
        /// </param>
        public Or(params IMatch[] matchers)
            : base(matchers)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Or"/> class.
        /// </summary>
        /// <param name="matcher">
        /// The matcher.
        /// </param>
        public Or(XElement matcher)
            : base(matcher)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the match type.
        /// </summary>
        public override MatcherType MatchType
        {
            get
            {
                return MatcherType.Or;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Is there a match for this record.
        /// </summary>
        /// <param name="logItems">
        /// The items to process.
        /// </param>
        /// <returns>
        /// <c>true</c> if a match is found.
        /// </returns>
        public override bool IsMatch(IDictionary<string, object> logItems)
        {
            return this.Matchers.Any(r => r.IsMatch(logItems));
        }

        #endregion
    }
}