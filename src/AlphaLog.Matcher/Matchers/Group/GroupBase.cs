﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GroupBase.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The group base.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Matcher.Matchers.Group
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    /// <summary>
    ///     The group base.
    /// </summary>
    public abstract class GroupBase : IMatch
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GroupBase"/> class.
        /// </summary>
        /// <param name="matchers">
        /// The matchers to add.
        /// </param>
        protected GroupBase(IEnumerable<IMatch> matchers)
        {
            this.Matchers = new List<IMatch>(matchers);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GroupBase"/> class.
        /// </summary>
        /// <param name="matcher">
        /// XML representation for the matcher.
        /// </param>
        protected GroupBase(XElement matcher)
        {
            if (matcher == null)
            {
                throw new ArgumentNullException("matcher");
            }

            this.Matchers = matcher.Elements().Select(MatchFactory.Parse).ToList();
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the <see cref="MatcherType" />
        /// </summary>
        public abstract MatcherType MatchType { get; }

        /// <summary>
        ///     Gets the matchers.
        /// </summary>
        public IList<IMatch> Matchers { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Add a matcher to the group.
        /// </summary>
        /// <param name="matchers">
        /// The matchers.
        /// </param>
        public void Add(params IMatch[] matchers)
        {
            foreach (IMatch match in matchers)
            {
                this.Matchers.Add(match);
            }
        }

        /// <summary>
        /// The can match.
        /// </summary>
        /// <param name="items">
        /// The items.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool CanMatch(Dictionary<string, object> items)
        {
            return this.Matchers.All(m => m.CanMatch(items));
        }

        /// <summary>
        ///     The description.
        /// </summary>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public string Description()
        {
            return string.Join(string.Format(" {0} ", this.MatchType), this.Matchers.Select(m => m.Description()));
        }

        /// <summary>
        /// Is this a match
        /// </summary>
        /// <param name="logItems">
        /// The log items to process.
        /// </param>
        /// <returns>
        /// <c>true</c> if a match is found.
        /// </returns>
        public abstract bool IsMatch(IDictionary<string, object> logItems);

        /// <summary>
        ///     Serialize this type to xml.
        /// </summary>
        /// <returns>
        ///     The <see cref="XElement" />.
        /// </returns>
        public XElement Serialize()
        {
            return new XElement(this.MatchType.ToString(), this.Matchers.Select(m => m.Serialize()));
        }

        #endregion
    }
}