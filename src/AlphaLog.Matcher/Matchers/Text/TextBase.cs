﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TextBase.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   A base class for text base matches.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Matcher.Matchers.Text
{
    using System;
    using System.Xml.Linq;

    /// <summary>
    ///     A base class for text base matches.
    /// </summary>
    public abstract class TextBase : MatchBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TextBase"/> class.
        /// </summary>
        /// <param name="searchValue">
        /// The value to search for.
        /// </param>
        /// <param name="searchField">
        /// The field to search.
        /// </param>
        /// <param name="caseSensitive">
        /// <c>true</c>, if the match is case sensitive.
        /// </param>
        protected TextBase(string searchValue, string searchField, bool caseSensitive)
            : base(searchValue, searchField)
        {
            this.CaseSensitive = caseSensitive;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TextBase"/> class.
        /// </summary>
        /// <param name="matcher">
        /// XML representation of the matcher.
        /// </param>
        protected TextBase(XElement matcher)
            : base(matcher)
        {
            this.CaseSensitive = bool.Parse(matcher.Attribute("caseSensitive").Value);
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets a value indicating whether case sensitivity is applied.
        /// </summary>
        public bool CaseSensitive { get; private set; }

        /// <summary>
        ///     Gets the match type.
        /// </summary>
        public abstract override MatcherType MatchType { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The description.
        /// </summary>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public override string Description()
        {
            return string.Format("{0}.{1}({2})", this.SearchField, this.MatchType, this.SearchValue);
        }

        /// <summary>
        /// Is this a match
        /// </summary>
        /// <param name="logItem">
        /// The item to match.
        /// </param>
        /// <returns>
        /// <c>true</c> if a match is found.
        /// </returns>
        public override bool IsMatch(object logItem)
        {
            if (logItem == null)
            {
                throw new ArgumentNullException("logItem");
            }

            return this.IsMatch((string)logItem);
        }

        /// <summary>
        /// Is this a match
        /// </summary>
        /// <param name="logItem">
        /// The item to match.
        /// </param>
        /// <returns>
        /// <c>true</c> if a match is found.
        /// </returns>
        public abstract bool IsMatch(string logItem);

        /// <summary>
        ///     Serialize the type to XML.
        /// </summary>
        /// <returns>
        ///     The <see cref="XElement" />.
        /// </returns>
        public override XElement Serialize()
        {
            return new XElement(
                this.MatchType.ToString(), 
                new XAttribute("searchField", this.SearchField), 
                new XAttribute("searchValue", this.SearchValue), 
                new XAttribute("caseSensitive", this.CaseSensitive));
        }

        #endregion
    }
}