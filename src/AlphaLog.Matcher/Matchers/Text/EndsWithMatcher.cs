﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EndsWithMatcher.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The ends with matcher.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Matcher.Matchers.Text
{
    using System;
    using System.Xml.Linq;

    /// <summary>
    ///     The ends with matcher.
    /// </summary>
    public class EndsWithMatcher : TextBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EndsWithMatcher"/> class.
        /// </summary>
        /// <param name="searchValue">
        /// The value to search for.
        /// </param>
        /// <param name="field">
        /// The field to search.
        /// </param>
        /// <param name="caseSensitive">
        /// <c>true</c>, if the match is case sensitive.
        /// </param>
        public EndsWithMatcher(string searchValue, string field, bool caseSensitive)
            : base(searchValue, field, caseSensitive)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EndsWithMatcher"/> class.
        /// </summary>
        /// <param name="matcher">
        /// The matcher.
        /// </param>
        public EndsWithMatcher(XElement matcher)
            : base(matcher)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the match type.
        /// </summary>
        public override MatcherType MatchType
        {
            get
            {
                return MatcherType.EndsWith;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Is this a match
        /// </summary>
        /// <param name="logItem">
        /// The item to match.
        /// </param>
        /// <returns>
        /// <c>true</c> if a match is found.
        /// </returns>
        public override bool IsMatch(string logItem)
        {
            if (string.IsNullOrEmpty(logItem))
            {
                if (logItem == null)
                {
                    throw new ArgumentNullException("logItem");
                }

                throw new ArgumentException(@"Empty string.", "logItem");
            }

            return logItem.EndsWith(
                this.SearchValue, 
                this.CaseSensitive ? StringComparison.Ordinal : StringComparison.InvariantCultureIgnoreCase);
        }

        #endregion
    }
}