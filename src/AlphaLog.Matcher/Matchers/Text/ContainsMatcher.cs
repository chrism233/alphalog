﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContainsMatcher.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The contains matcher.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Matcher.Matchers.Text
{
    using System;
    using System.Xml.Linq;

    /// <summary>
    ///     The contains matcher.
    /// </summary>
    public class ContainsMatcher : TextBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ContainsMatcher"/> class.
        ///     Instantiate a new instance of <see cref="ContainsMatcher"/> class.
        /// </summary>
        /// <param name="searchValue">
        /// The search value.
        /// </param>
        /// <param name="field">
        /// The log entry name.
        /// </param>
        /// <param name="caseSensitive">
        /// True, if the match is case sensitive.
        /// </param>
        public ContainsMatcher(string searchValue, string field, bool caseSensitive)
            : base(searchValue, field, caseSensitive)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContainsMatcher"/> class.
        ///     Initialize a new instance of <see cref="ContainsMatcher"/> class.
        /// </summary>
        /// <param name="matcher">
        /// XML representation of the rule.
        /// </param>
        public ContainsMatcher(XElement matcher)
            : base(matcher)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the match type.
        /// </summary>
        public override MatcherType MatchType
        {
            get
            {
                return MatcherType.Contains;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Checks if the log-item contains the specified value.
        /// </summary>
        /// <param name="logItem">
        /// The log item to match.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool IsMatch(string logItem)
        {
            if (string.IsNullOrEmpty(logItem))
            {
                if (logItem == null)
                {
                    throw new ArgumentNullException("logItem");
                }

                throw new ArgumentException(@"Empty string.", "logItem");
            }

            return logItem.IndexOf(
                this.SearchValue, 
                this.CaseSensitive ? StringComparison.InvariantCulture : StringComparison.InvariantCultureIgnoreCase)
                   >= 0;
        }

        #endregion
    }
}