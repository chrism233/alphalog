﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WildcardMatcher.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The wildcard matcher.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Matcher.Matchers.Text
{
    using System;
    using System.Xml.Linq;

    /// <summary>
    ///     The wildcard matcher.
    /// </summary>
    public class WildcardMatcher : TextBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="WildcardMatcher"/> class.
        /// </summary>
        /// <param name="searchValue">
        /// The value to search.
        /// </param>
        /// <param name="searchField">
        /// The field to search.
        /// </param>
        /// <param name="caseSensitive">
        /// True, if the match is case sensitive.
        /// </param>
        public WildcardMatcher(string searchValue, string searchField, bool caseSensitive)
            : base(searchValue, searchField, caseSensitive)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WildcardMatcher"/> class.
        /// </summary>
        /// <param name="matcher">
        /// The matcher.
        /// </param>
        public WildcardMatcher(XElement matcher)
            : base(matcher)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the match type.
        /// </summary>
        public override MatcherType MatchType
        {
            get
            {
                return MatcherType.Wildcard;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Is this a match
        /// </summary>
        /// <param name="logItem">
        /// The item to check.
        /// </param>
        /// <returns>
        /// <c>true</c> if a match is found.
        /// </returns>
        public override bool IsMatch(string logItem)
        {
            if (string.IsNullOrEmpty(logItem))
            {
                if (logItem == null)
                {
                    throw new ArgumentNullException("logItem");
                }

                throw new ArgumentException("Empty string.", "logItem");
            }

            throw new NotImplementedException();
        }

        #endregion
    }
}