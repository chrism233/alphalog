﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Match.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The Match interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Matcher
{
    using System.Collections.Generic;
    using System.Xml.Linq;

    /// <summary>
    ///     The Match interface.
    /// </summary>
    public interface IMatch
    {
        #region Public Properties

        /// <summary>
        ///     Gets the match type.
        /// </summary>
        MatcherType MatchType { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Can this item be matched.
        /// </summary>
        /// <param name="items">
        /// The log items.
        /// </param>
        /// <returns>
        /// <c>true</c> the matcher is valid for the provided items.
        /// </returns>
        bool CanMatch(Dictionary<string, object> items);

        /// <summary>
        ///     The description.
        /// </summary>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        string Description();

        /// <summary>
        /// The is match.
        /// </summary>
        /// <param name="logItems">
        /// The log items.
        /// </param>
        /// <returns>
        /// <c>true</c> if a match is found.
        /// </returns>
        bool IsMatch(IDictionary<string, object> logItems);

        /// <summary>
        ///     The serialize.
        /// </summary>
        /// <returns>
        ///     The <see cref="XElement" />.
        /// </returns>
        XElement Serialize();

        #endregion
    }
}