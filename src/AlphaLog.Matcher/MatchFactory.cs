﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchFactory.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The match factory.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Matcher
{
    using System;
    using System.Xml.Linq;

    using AlphaLog.Matcher.Matchers;
    using AlphaLog.Matcher.Matchers.Group;
    using AlphaLog.Matcher.Matchers.Numeric;
    using AlphaLog.Matcher.Matchers.Text;

    /// <summary>
    ///     The match factory.
    /// </summary>
    public static class MatchFactory
    {
        #region Public Methods and Operators

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="matcherType">
        /// The matcher type.
        /// </param>
        /// <returns>
        /// The <see cref="IMatch"/>.
        /// </returns>
        public static IMatch Create(MatcherType matcherType)
        {
            return Create(matcherType, string.Empty, string.Empty);
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="matcherType">
        /// The matcher type.
        /// </param>
        /// <param name="searchValue">
        /// The search value.
        /// </param>
        /// <param name="field">
        /// The field.
        /// </param>
        /// <returns>
        /// The <see cref="IMatch"/>.
        /// </returns>
        public static IMatch Create(MatcherType matcherType, string searchValue, string field)
        {
            return Create(matcherType, searchValue, field, false);
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="matcherType">
        /// The matcher type.
        /// </param>
        /// <param name="searchValue">
        /// The search value.
        /// </param>
        /// <param name="field">
        /// The field.
        /// </param>
        /// <param name="caseSensative">
        /// The case sensitive.
        /// </param>
        /// <returns>
        /// The <see cref="IMatch"/>.
        /// </returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// The <see cref="MatcherType"/> has not been implemented.
        /// </exception>
        public static IMatch Create(MatcherType matcherType, string searchValue, string field, bool caseSensative)
        {
            switch (matcherType)
            {
                case MatcherType.Always:
                    return new AlwaysMatcher();
                case MatcherType.Contains:
                    return new ContainsMatcher(searchValue, field, caseSensative);
                case MatcherType.EndsWith:
                    return new EndsWithMatcher(searchValue, field, caseSensative);
                case MatcherType.StartsWith:
                    return new StartsWithMatcher(searchValue, field, caseSensative);
                case MatcherType.Exact:
                    return new ExactMatcher(searchValue, field, caseSensative);
                case MatcherType.Regex:
                    return new RegexMatcher(searchValue, field, caseSensative);
                case MatcherType.EqualTo:
                    return new EqualToMatcher(searchValue, field);
                case MatcherType.GreaterThan:
                    return new GreaterThanMatcher(searchValue, field);
                case MatcherType.LessThan:
                    return new LessThanMatcher(searchValue, field);

                    // case MatcherType.GreaterThanOrEqualTo:
                    // return new GreaterThanMatcher(searchValue, field).Or(MatcherType.EqualTo);
                    // case MatcherType.LessThanOrEqualTo:
                    // return new LessThanMatcher(searchValue, field).Or(MatcherType.EqualTo);
                case MatcherType.Or:
                    return new Or();
                case MatcherType.And:
                    return new And();
                case MatcherType.Wildcard:
                    return new WildcardMatcher(searchValue, field, caseSensative);
                default:
                    throw new ArgumentOutOfRangeException("matcherType");
            }
        }

        /// <summary>
        /// The get description.
        /// </summary>
        /// <param name="getMatcher">
        /// The get matcher.
        /// </param>
        public static void GetDescription(IMatch getMatcher)
        {
            getMatcher.Description();
        }

        /// <summary>
        /// Create a matcher from raw XML.
        /// </summary>
        /// <param name="matcher">
        /// XML representation of the matcher.
        /// </param>
        /// <returns>
        /// The <see cref="IMatch"/>.
        /// </returns>
        public static IMatch Parse(XElement matcher)
        {
            if (matcher == null)
            {
                throw new ArgumentNullException("matcher");
            }

            var type = (MatcherType)Enum.Parse(typeof(MatcherType), matcher.Name.ToString());

            switch (type)
            {
                case MatcherType.Always:
                    return new AlwaysMatcher();
                case MatcherType.Contains:
                    return new ContainsMatcher(matcher);
                case MatcherType.EndsWith:
                    return new EndsWithMatcher(matcher);
                case MatcherType.StartsWith:
                    return new StartsWithMatcher(matcher);
                case MatcherType.Exact:
                    return new ExactMatcher(matcher);
                case MatcherType.Regex:
                    return new RegexMatcher(matcher);
                case MatcherType.Wildcard:
                    return new WildcardMatcher(matcher);
                case MatcherType.EqualTo:
                    return new EqualToMatcher(matcher);
                case MatcherType.GreaterThan:
                    return new GreaterThanMatcher(matcher);
                case MatcherType.LessThan:
                    return new LessThanMatcher(matcher);

                    // case MatcherType.GreaterThanOrEqualTo:
                    // return new GreaterThanMatcher(matcher);
                    // case MatcherType.LessThanOrEqualTo:
                    // return new LessThanMatcher(matcher);
                case MatcherType.And:
                    return new And(matcher);
                case MatcherType.Or:
                    return new Or(matcher);
                default:
                    throw new ArgumentOutOfRangeException("matcher");
            }
        }

        #endregion
    }
}