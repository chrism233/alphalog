﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatcherType.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The matcher type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Matcher
{
    /// <summary>
    ///     The matcher type.
    /// </summary>
    public enum MatcherType
    {
        /// <summary>
        ///     The always.
        /// </summary>
        Always, 

        /// <summary>
        ///     The contains.
        /// </summary>
        Contains, 

        /// <summary>
        ///     The ends with.
        /// </summary>
        EndsWith, 

        /// <summary>
        ///     The starts with.
        /// </summary>
        StartsWith, 

        /// <summary>
        ///     The exact.
        /// </summary>
        Exact, 

        /// <summary>
        ///     The regex.
        /// </summary>
        Regex, 

        /// <summary>
        ///     The wildcard.
        /// </summary>
        Wildcard, 

        /// <summary>
        ///     The equal to.
        /// </summary>
        EqualTo, 

        /// <summary>
        ///     The greater than.
        /// </summary>
        GreaterThan, 

        /// <summary>
        ///     The less than.
        /// </summary>
        LessThan, 

        // GreaterThanOrEqualTo,

        // LessThanOrEqualTo,

        /// <summary>
        ///     The and.
        /// </summary>
        And, 

        /// <summary>
        ///     The or.
        /// </summary>
        Or, 
    }
}