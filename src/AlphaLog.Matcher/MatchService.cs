﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchService.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The match service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.Matcher
{
    using System.Collections.Generic;

    using AlphaLog.Matcher.Matchers.Group;

    /// <summary>
    ///     The match service.
    /// </summary>
    public class MatchService
    {
        #region Fields

        /// <summary>
        ///     The m_matcher.
        /// </summary>
        private And matcher;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="MatchService" /> class.
        ///     Instantiate a new instance of <see cref="MatchService" /> class.
        /// </summary>
        public MatchService()
        {
            this.matcher = new And();
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Add a new matcher.
        /// </summary>
        /// <param name="matcherType">
        /// The matcher type.
        /// </param>
        /// <param name="searchValue">
        /// The value to search for.
        /// </param>
        /// <param name="field">
        /// The field to search.
        /// </param>
        public void AddMatcher(MatcherType matcherType, string searchValue, string field)
        {
            IMatch matcher = MatchFactory.Create(matcherType, searchValue, field);

            this.matcher.Add(matcher);
        }

        /// <summary>
        /// Is this a match.
        /// </summary>
        /// <param name="descriptor">
        /// The matcher items.
        /// </param>
        /// <returns>
        /// <c>true</c> if a match is found.
        /// </returns>
        public bool IsMatch(Dictionary<string, object> descriptor)
        {
            return this.matcher.IsMatch(descriptor);
        }

        /// <summary>
        ///     Resets the service.
        /// </summary>
        public void Reset()
        {
            this.matcher = new And();
        }

        #endregion
    }
}