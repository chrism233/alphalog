﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ManageMatcherVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The manage matcher vm.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Matcher.ViewModel
{
    using System.ComponentModel;
    using System.ComponentModel.Composition;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Windows.Input;

    using AlphaLog.Matcher.Matchers;
    using AlphaLog.UI.Common;
    using AlphaLog.UI.Common.Models;

    using Microsoft.Practices.Prism.Commands;

    /// <summary>
    ///     The manage matcher vm.
    /// </summary>
    [Export]
    public class ManageMatcherVM : TabSettingsVM
    {
        #region Fields

        /// <summary>
        ///     The matchers.
        /// </summary>
        private readonly MatcherDatabase matchers;

        /// <summary>
        ///     The settings.
        /// </summary>
        private readonly FieldSettings settings;

        /// <summary>
        ///     The adding new matcher.
        /// </summary>
        private bool addingNewMatcher;

        /// <summary>
        ///     The matcher.
        /// </summary>
        private MatcherControlVM matcher;

        /// <summary>
        ///     The matcher items.
        /// </summary>
        private ObservableCollectionWithPropertyChanged<MatcherItem> matcherItems;

        /// <summary>
        ///     The new matcher name.
        /// </summary>
        private string newMatcherName;

        /// <summary>
        ///     The selected matcher item.
        /// </summary>
        private MatcherItem selectedMatcherItem;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ManageMatcherVM"/> class.
        /// </summary>
        /// <param name="settingsManager">
        /// The settings Manager.
        /// </param>
        [ImportingConstructor]
        public ManageMatcherVM(ISettingsManager settingsManager)
            : base("Matchers")
        {
            this.settings = settingsManager.GetsSetting<FieldSettings>();
            this.matchers = settingsManager.GetsSetting<MatcherDatabase>();
            this.MatcherItems = new ObservableCollectionWithPropertyChanged<MatcherItem>(this.matchers.GetItems());
            this.AddMatcherCommand = new DelegateCommand<string>(this.AddMatcher);
            this.RemoveMatcherCommand = new DelegateCommand<MatcherItem>(this.RemoveMatcher, this.CanRemoveMatcher);
            this.CancelMatcherCommand = new DelegateCommand(this.CancelAddingMatcher);

            if (this.MatcherItems.Any())
            {
                this.SelectedMatcherItem = this.MatcherItems.FirstOrDefault();
            }
            else
            {
                this.AddingNewMatcher = true;
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the add matcher command.
        /// </summary>
        public ICommand AddMatcherCommand { get; private set; }

        /// <summary>
        ///     Gets or sets a value indicating whether adding new matcher.
        /// </summary>
        public bool AddingNewMatcher
        {
            get
            {
                return this.addingNewMatcher;
            }

            set
            {
                this.SetProperty(ref this.addingNewMatcher, value);
            }
        }

        /// <summary>
        ///     Gets the cancel matcher command.
        /// </summary>
        public ICommand CancelMatcherCommand { get; private set; }

        /// <summary>
        ///     Gets or sets the matcher builder.
        /// </summary>
        public MatcherControlVM MatcherBuilder
        {
            get
            {
                return this.matcher;
            }

            set
            {
                this.SetProperty(ref this.matcher, value);
            }
        }

        /// <summary>
        ///     Gets or sets the matcher items.
        /// </summary>
        public ObservableCollectionWithPropertyChanged<MatcherItem> MatcherItems
        {
            get
            {
                return this.matcherItems;
            }

            set
            {
                this.SetProperty(ref this.matcherItems, value);
            }
        }

        /// <summary>
        ///     Gets or sets the matcher name to use when adding the new matcher.
        /// </summary>
        public string NewMatcherName
        {
            get
            {
                return this.newMatcherName;
            }

            set
            {
                this.SetProperty(ref this.newMatcherName, value);
            }
        }

        /// <summary>
        ///     Gets the remove matcher command.
        /// </summary>
        public ICommand RemoveMatcherCommand { get; private set; }

        /// <summary>
        ///     Gets or sets the selected <see cref="MatcherItem" />.
        /// </summary>
        public MatcherItem SelectedMatcherItem
        {
            get
            {
                return this.selectedMatcherItem;
            }

            set
            {
                this.SetProperty(ref this.selectedMatcherItem, value);
                this.NotifyCanExecuteChanged(this.RemoveMatcherCommand);
                if (this.selectedMatcherItem != null)
                {
                    var matcherControl = new MatcherControlVM(
                        this.settings.GetItems().Select(c => c.Title).ToList(), 
                        this.selectedMatcherItem.Matcher);
                    matcherControl.PropertyChanged += this.matcherControl_PropertyChanged;
                    this.MatcherBuilder = matcherControl;
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     The save changes.
        /// </summary>
        protected override void SaveChanges()
        {
            base.SaveChanges();
            if (this.AddingNewMatcher)
            {
                // this.settings.AddMatcher(this.NewMatcherName, this.MatcherBuilder.Matchers[0].GetMatcher());
                // this.MatcherItems.Add(new MatcherItem(this.NewMatcherName, this.MatcherBuilder.Matchers[0].GetMatcher()));
                // this.MatcherBuilder = new MatcherBuilderVM(this.settings);
                // this.AddingNewMatcher = false;
            }
            else
            {
                if (this.SelectedMatcherItem != null)
                {
                    this.matchers.Add(this.SelectedMatcherItem.Name, this.MatcherBuilder.GetMatcher());
                }
            }
        }

        /// <summary>
        /// The add matcher.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        private void AddMatcher(string obj)
        {
            this.AddingNewMatcher = true;
            this.MatcherBuilder = new MatcherControlVM(
                this.settings.GetItems().Select(c => c.Title).ToList(), 
                new AlwaysMatcher());
        }

        /// <summary>
        /// The can remove matcher.
        /// </summary>
        /// <param name="arg">
        /// The arg.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool CanRemoveMatcher(MatcherItem arg)
        {
            return this.SelectedMatcherItem != null;
        }

        /// <summary>
        ///     The can save matcher.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        private bool CanSaveMatcher()
        {
            return true;
        }

        /// <summary>
        ///     The cancel adding matcher.
        /// </summary>
        private void CancelAddingMatcher()
        {
            this.AddingNewMatcher = false;
        }

        /// <summary>
        /// The notify can execute changed.
        /// </summary>
        /// <param name="delegateCommand">
        /// The delegate command.
        /// </param>
        private void NotifyCanExecuteChanged(ICommand delegateCommand)
        {
            var command = delegateCommand as DelegateCommand<MatcherItem>;

            if (command != null)
            {
                command.RaiseCanExecuteChanged();
            }
        }

        /// <summary>
        /// The remove matcher.
        /// </summary>
        /// <param name="matcherItem">
        /// The matcher item.
        /// </param>
        private void RemoveMatcher(MatcherItem matcherItem)
        {
            this.matchers.Remove(this.SelectedMatcherItem);
            this.MatcherItems.Remove(this.SelectedMatcherItem);

            // this.MatcherBuilder = this.M
        }

        /// <summary>
        /// The matcher control_ property changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", 
            Justification = "Reviewed. Suppression is OK here.")]
        private void matcherControl_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.HasUnSavedChanges = true;
        }

        #endregion
    }
}