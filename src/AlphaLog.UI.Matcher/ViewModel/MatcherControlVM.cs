﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatcherControlVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   A view model for the matcher tree.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Matcher.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.ComponentModel.Composition;
    using System.Linq;
    using System.Windows.Input;

    using AlphaLog.Matcher;
    using AlphaLog.Matcher.Matchers;
    using AlphaLog.Matcher.Matchers.Group;
    using AlphaLog.Matcher.Matchers.Text;

    using Microsoft.Practices.Prism.Commands;
    using Microsoft.Practices.Prism.Mvvm;

    /// <summary>
    ///     A view model for the matcher tree.
    /// </summary>
    [Export]
    public class MatcherControlVM : BindableBase
    {
        #region Fields

        /// <summary>
        ///     The matchers.
        /// </summary>
        private readonly ObservableCollection<MatcherControlVM> matchers;

        /// <summary>
        ///     The parent.
        /// </summary>
        private readonly MatcherControlVM parent;

        /// <summary>
        ///     The search field.
        /// </summary>
        private string searchField;

        /// <summary>
        ///     The search fields.
        /// </summary>
        private ObservableCollection<string> searchFields;

        /// <summary>
        ///     The search value.
        /// </summary>
        private string searchValue;

        /// <summary>
        ///     The selected matcher.
        /// </summary>
        private MatcherType selectedMatcher;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MatcherControlVM"/> class.
        /// </summary>
        /// <param name="colummns">
        /// The columns to match against.
        /// </param>
        /// <param name="matcher">
        /// The matcher instance.
        /// </param>
        /// <param name="parent">
        /// The parent matcher.
        /// </param>
        public MatcherControlVM(List<string> colummns, IMatch matcher, MatcherControlVM parent = null)
        {
            this.matchers = new ObservableCollection<MatcherControlVM>();
            this.MatcherTypes =
                new ObservableCollection<MatcherType>(
                    Enum.GetNames(typeof(MatcherType))
                        .ToList()
                        .Select(e => (MatcherType)Enum.Parse(typeof(MatcherType), e))
                        .ToList());

            this.AddMatcherCommand = new DelegateCommand<string>(this.AddMatcher);
            this.RemoveMatcherCommand = new DelegateCommand<MatcherControlVM>(this.RemoveMatcher);

            this.SelectedMatcher = matcher.MatchType;
            this.SearchFields = new ObservableCollection<string>(colummns);
            this.parent = parent;
            this.UpdateMatcher(matcher);
            this.PropertyChanged += this.MatcherControlVM_PropertyChanged;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the add matcher command.
        /// </summary>
        public ICommand AddMatcherCommand { get; private set; }

        /// <summary>
        ///     Gets or sets a value indicating whether case sensative.
        /// </summary>
        public bool CaseSensative { get; set; }

        /// <summary>
        ///     Gets a value indicating whether is root.
        /// </summary>
        public bool IsRoot
        {
            get
            {
                return this.parent == null;
            }
        }

        /// <summary>
        ///     Gets or sets the matcher types.
        /// </summary>
        public ObservableCollection<MatcherType> MatcherTypes { get; set; }

        /// <summary>
        ///     Gets the matchers.
        /// </summary>
        public ObservableCollection<MatcherControlVM> Matchers
        {
            get
            {
                return this.matchers;
            }
        }

        /// <summary>
        ///     Gets the remove matcher command.
        /// </summary>
        public ICommand RemoveMatcherCommand { get; private set; }

        /// <summary>
        ///     Gets or sets the search field.
        /// </summary>
        public string SearchField
        {
            get
            {
                return this.searchField;
            }

            set
            {
                this.SetProperty(ref this.searchField, value);
            }
        }

        /// <summary>
        ///     Gets or sets the search fields.
        /// </summary>
        public ObservableCollection<string> SearchFields
        {
            get
            {
                return this.searchFields;
            }

            set
            {
                this.SetProperty(ref this.searchFields, value);
            }
        }

        /// <summary>
        ///     Gets or sets the search value.
        /// </summary>
        public string SearchValue
        {
            get
            {
                return this.searchValue;
            }

            set
            {
                this.SetProperty(ref this.searchValue, value);
            }
        }

        /// <summary>
        ///     Gets or sets the selected matcher.
        /// </summary>
        public MatcherType SelectedMatcher
        {
            get
            {
                return this.selectedMatcher;
            }

            set
            {
                this.SetProperty(ref this.selectedMatcher, value);
                this.UpdateMatcher(MatchFactory.Create(this.SelectedMatcher, this.SearchValue, this.SearchField));
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Get a <c ref="Matcher" /> from the view model.
        /// </summary>
        /// <returns>
        ///     The <see cref="IMatch" />.
        /// </returns>
        public IMatch GetMatcher()
        {
            IMatch match = MatchFactory.Create(this.selectedMatcher, this.SearchValue, this.SearchField);
            if (MatcherHelpers.MatcherTypeToCategory(match.MatchType) == MatcherCategory.Group)
            {
                ((GroupBase)match).Add(this.Matchers.Select(m => m.GetMatcher()).ToArray());
            }

            return match;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Add a matcher to the tree.
        /// </summary>
        /// <param name="selectedMatcher">
        /// The matcher to add.
        /// </param>
        private void AddMatcher(string selectedMatcher)
        {
            if (MatcherHelpers.MatcherTypeToCategory(this.SelectedMatcher) == MatcherCategory.Group)
            {
                this.Matchers.Add(new MatcherControlVM(this.SearchFields.ToList(), new AlwaysMatcher(), this));
            }
        }

        /// <summary>
        /// The matcher control v m_ property changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void MatcherControlVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // TODO Is this the best way to notify the parent. Should we be using a bubbling routed event?  
            if (this.parent != null)
            {
                this.parent.OnPropertyChanged(e.PropertyName);
            }
        }

        /// <summary>
        /// Remove a matcher from the tree.
        /// </summary>
        /// <param name="obj">
        /// The matcher to remove.
        /// </param>
        private void RemoveMatcher(MatcherControlVM obj)
        {
            if (this.parent != null)
            {
                this.parent.Matchers.Remove(obj);
            }
        }

        /// <summary>
        /// Update the matcher.
        /// </summary>
        /// <param name="selectedMatcher">
        /// The selected matcher.
        /// </param>
        private void UpdateMatcher(IMatch selectedMatcher)
        {
            switch (MatcherHelpers.MatcherTypeToCategory(selectedMatcher.MatchType))
            {
                case MatcherCategory.Group:
                    {
                        var group = selectedMatcher as GroupBase;
                        foreach (IMatch m in group.Matchers)
                        {
                            this.Matchers.Add(new MatcherControlVM(this.SearchFields.ToList(), m, this));
                        }

                        break;
                    }

                case MatcherCategory.Misc:
                    this.Matchers.Clear();
                    break;
                case MatcherCategory.Text:
                    var text = selectedMatcher as TextBase;
                    this.SearchField = string.IsNullOrEmpty(text.SearchField) ? string.Empty : text.SearchField;
                    this.SearchValue = string.IsNullOrEmpty(text.SearchValue) ? string.Empty : text.SearchValue;
                    this.Matchers.Clear();
                    this.CaseSensative = text.CaseSensitive;
                    break;
                case MatcherCategory.Numeric:
                    var baseMatch = selectedMatcher as MatchBase;
                    this.SearchField = string.IsNullOrEmpty(baseMatch.SearchField)
                                           ? string.Empty
                                           : baseMatch.SearchField;
                    this.SearchValue = string.IsNullOrEmpty(baseMatch.SearchValue)
                                           ? string.Empty
                                           : baseMatch.SearchValue;
                    this.Matchers.Clear();
                    break;
                default:
                    throw new ArgumentException("selectedMatcher");
            }
        }

        #endregion
    }
}