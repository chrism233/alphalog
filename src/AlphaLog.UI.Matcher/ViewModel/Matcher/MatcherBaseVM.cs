﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatcherBaseVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The matcher base vm.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Matcher.ViewModel.Matcher
{
    using System.Collections.ObjectModel;
    using System.ComponentModel.Composition;
    using System.Linq;

    using AlphaLog.Matcher;
    using AlphaLog.UI.Common.Models;

    using Microsoft.Practices.Prism.Mvvm;

    /// <summary>
    ///     The matcher base vm.
    /// </summary>
    [Export]
    public abstract class MatcherBaseVM : BindableBase
    {
        #region Fields

        /// <summary>
        ///     The search fields.
        /// </summary>
        private readonly ObservableCollection<string> searchFields;

        /// <summary>
        ///     The search field.
        /// </summary>
        private string searchField;

        /// <summary>
        ///     The search value.
        /// </summary>
        private string searchValue;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MatcherBaseVM"/> class.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        public MatcherBaseVM(FieldSettings settings)
        {
            if (settings != null)
            {
                this.searchFields = new ObservableCollection<string>(settings.GetItems().Select(c => c.Title));
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the match type.
        /// </summary>
        public MatcherType MatchType { get; set; }

        /// <summary>
        ///     Gets or sets the search field.
        /// </summary>
        public string SearchField
        {
            get
            {
                return this.searchField;
            }

            set
            {
                this.searchField = value;
                this.OnPropertyChanged("SearchField");
            }
        }

        /// <summary>
        ///     Gets the search fields.
        /// </summary>
        public ObservableCollection<string> SearchFields
        {
            get
            {
                return this.searchFields;
            }
        }

        /// <summary>
        ///     Gets or sets the search value.
        /// </summary>
        public string SearchValue
        {
            get
            {
                return this.searchValue;
            }

            set
            {
                this.searchValue = value;
                this.OnPropertyChanged("SearchValue");
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Get the matcher.
        /// </summary>
        /// <returns>
        ///     The <see cref="IMatch" />.
        /// </returns>
        public abstract IMatch GetMatcher();

        /// <summary>
        /// Populate this matcher.
        /// </summary>
        /// <param name="matcher">
        /// The matcher.
        /// </param>
        public abstract void Populate(IMatch matcher);

        #endregion
    }
}