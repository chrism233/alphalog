﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TextMatcherVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The text matcher vm.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Matcher.ViewModel.Matcher
{
    using System.ComponentModel.Composition;

    using AlphaLog.Matcher;
    using AlphaLog.Matcher.Matchers.Text;
    using AlphaLog.UI.Common.Models;

    /// <summary>
    ///     The text matcher vm.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class TextMatcherVM : MatcherBaseVM
    {
        #region Fields

        /// <summary>
        ///     A value indicating whether case sensitivity is applied to this matcher.
        /// </summary>
        private bool caseSensative;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TextMatcherVM"/> class.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        [ImportingConstructor]
        public TextMatcherVM(FieldSettings settings)
            : base(settings)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets a value indicating whether case sensative.
        /// </summary>
        public bool CaseSensative
        {
            get
            {
                return this.caseSensative;
            }

            set
            {
                this.caseSensative = value;
                this.OnPropertyChanged("CaseSensative");
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Get the matcher.
        /// </summary>
        /// <returns>
        ///     The <see cref="IMatch" />.
        /// </returns>
        public override IMatch GetMatcher()
        {
            return MatchFactory.Create(this.MatchType, this.SearchValue, this.SearchField, this.CaseSensative);
        }

        /// <summary>
        /// Populate this matcher.
        /// </summary>
        /// <param name="matcher">
        /// The matcher.
        /// </param>
        public override void Populate(IMatch matcher)
        {
            var text = matcher as TextBase;

            this.SearchField = string.IsNullOrEmpty(text.SearchField) ? string.Empty : text.SearchField;
            this.SearchValue = string.IsNullOrEmpty(text.SearchValue) ? string.Empty : text.SearchValue;
            this.CaseSensative = text.CaseSensitive;
            this.MatchType = text.MatchType;
        }

        #endregion
    }
}