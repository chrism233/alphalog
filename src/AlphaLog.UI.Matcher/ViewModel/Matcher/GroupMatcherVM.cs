﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GroupMatcherVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   A view model for a group matcher.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Matcher.ViewModel.Matcher
{
    using System;
    using System.Collections.ObjectModel;
    using System.ComponentModel.Composition;
    using System.Linq;

    using AlphaLog.Matcher;
    using AlphaLog.Matcher.Matchers.Group;

    /// <summary>
    ///     A view model for a group matcher.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class GroupMatcherVM : MatcherBaseVM
    {
        #region Fields

        /// <summary>
        ///     The m_matchers.
        /// </summary>
        private readonly ObservableCollection<MatcherControlVM> matchers;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GroupMatcherVM"/> class.
        /// </summary>
        /// <param name="matchers">
        /// The matchers.
        /// </param>
        public GroupMatcherVM(ObservableCollection<MatcherControlVM> matchers)
            : base(null)
        {
            this.matchers = matchers;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The get matcher.
        /// </summary>
        /// <returns>
        ///     The <see cref="IMatch" />.
        /// </returns>
        /// <exception cref="InvalidOperationException">
        ///     The <see cref="MatcherType" /> is invalid.
        /// </exception>
        public override IMatch GetMatcher()
        {
            switch (this.MatchType)
            {
                case MatcherType.Or:
                    return new Or(this.matchers.Select(s => s.GetMatcher()).ToArray());
                case MatcherType.And:
                    return new And(this.matchers.Select(s => s.GetMatcher()).ToArray());
                default:
                    throw new InvalidOperationException();
            }
        }

        /// <summary>
        /// The populate.
        /// </summary>
        /// <param name="matcher">
        /// The matcher.
        /// </param>
        public override void Populate(IMatch matcher)
        {
            var m = matcher as GroupBase;

            this.MatchType = m.MatchType;

            // if(m != null)
            // m_matchers = new ObservableCollection<MatcherVM>(m.Matchers.Select(n => MatchVMFactory.UpdateMatcher(n)).ToList());
        }

        #endregion
    }
}