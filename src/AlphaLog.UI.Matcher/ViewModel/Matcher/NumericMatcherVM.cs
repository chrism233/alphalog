﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NumericMatcherVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The numeric matcher vm.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Matcher.ViewModel.Matcher
{
    using System.ComponentModel.Composition;

    using AlphaLog.Matcher;
    using AlphaLog.Matcher.Matchers.Numeric;
    using AlphaLog.UI.Common.Models;

    /// <summary>
    ///     The numeric matcher vm.
    /// </summary>
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class NumericMatcherVM : MatcherBaseVM
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="NumericMatcherVM"/> class.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        [ImportingConstructor]
        public NumericMatcherVM(FieldSettings settings)
            : base(settings)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Get the matcher.
        /// </summary>
        /// <returns>
        ///     The <see cref="IMatch" />.
        /// </returns>
        public override IMatch GetMatcher()
        {
            return MatchFactory.Create(this.MatchType, this.SearchValue, this.SearchField);
        }

        /// <summary>
        /// Populate this matcher.
        /// </summary>
        /// <param name="matcher">
        /// The matcher.
        /// </param>
        public override void Populate(IMatch matcher)
        {
            var m = matcher as NumericBase;
            this.SearchField = string.IsNullOrEmpty(m.SearchField) ? string.Empty : m.SearchField;
            this.SearchValue = string.IsNullOrEmpty(m.SearchValue) ? string.Empty : m.SearchValue;
            this.MatchType = m.MatchType;
        }

        #endregion
    }
}