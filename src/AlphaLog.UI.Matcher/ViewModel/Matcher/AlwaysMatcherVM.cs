﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AlwaysMatcherVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The always matcher vm.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Matcher.ViewModel.Matcher
{
    using System.ComponentModel.Composition;

    using AlphaLog.Matcher;
    using AlphaLog.Matcher.Matchers;
    using AlphaLog.UI.Common.Models;

    /// <summary>
    ///     The always matcher vm.
    /// </summary>
    [Export]
    public class AlwaysMatcherVM : MatcherBaseVM
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AlwaysMatcherVM"/> class.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        [ImportingConstructor]
        public AlwaysMatcherVM(FieldSettings settings)
            : base(settings)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The get matcher.
        /// </summary>
        /// <returns>
        ///     The <see cref="IMatch" />.
        /// </returns>
        public override IMatch GetMatcher()
        {
            return new AlwaysMatcher();
        }

        /// <summary>
        /// The populate.
        /// </summary>
        /// <param name="matcher">
        /// The matcher.
        /// </param>
        public override void Populate(IMatch matcher)
        {
            this.MatchType = matcher.MatchType;
        }

        #endregion
    }
}