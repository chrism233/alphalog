﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="App.xaml.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   Interaction logic for App.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI
{
    using System;
    using System.ComponentModel.Composition;
    using System.IO;
    using System.Windows;

    using AlphaLog.UI.Common;
    using AlphaLog.UI.Properties;

    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    [Export]
    public partial class App : Application
    {
        #region Fields

        /// <summary>
        ///     The settings manager.
        /// </summary>
        private SettingsManager settingsManager;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="App" /> class.
        /// </summary>
        public App()
        {

        }

        #endregion

        #region Methods

        /// <summary>
        /// The on exit.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);

            if (this.settingsManager != null)
            {
                this.settingsManager.SaveChanges(Settings.Default.AlphaLogSettingsFileName);
            }
        }

        /// <summary>
        /// The on startup.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnStartup(StartupEventArgs e)
        {
            this.RunInReleaseMode();
            
            base.OnStartup(e);
        }

        /// <summary>
        /// The app domain unhandled exception.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private static void AppDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            HandleException(e.ExceptionObject as Exception);
        }

        /// <summary>
        /// The handle exception.
        /// </summary>
        /// <param name="ex">
        /// The ex.
        /// </param>
        private static void HandleException(Exception ex)
        {
            if (ex == null)
            {
            }

            MessageBox.Show(
                string.Format("An error ha occurred {0}.", ex.Message), 
                "Application error", 
                MessageBoxButton.OK, 
                MessageBoxImage.Error);
            Environment.Exit(1);
        }

        /// <summary>
        ///     The run in release mode.
        /// </summary>
        private void RunInReleaseMode()
        {
            Current.ShutdownMode = ShutdownMode.OnExplicitShutdown;

            if (!File.Exists(Settings.Default.AlphaLogSettingsFileName))
            {
                var dialog = new CreateSettingsWindow();
                dialog.ShowDialog();
            }

            if (!File.Exists(Settings.Default.AlphaLogSettingsFileName))
            {
                Current.Shutdown(-1);
            }
            else
            {
                Current.ShutdownMode = ShutdownMode.OnMainWindowClose;
                this.settingsManager = new SettingsManager(Settings.Default.AlphaLogSettingsFileName);
                var bootstrapper = new AlphaLogBootstrapper(this.settingsManager);
                bootstrapper.Run();
            }
        }

        #endregion
    }
}