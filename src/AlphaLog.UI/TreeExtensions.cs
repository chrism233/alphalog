﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TreeExtensions.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The tree extensions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AlphaLog.UI
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;

    using LinqToVisualTree;

    /// <summary>
    ///     The tree extensions.
    /// </summary>
    public static class TreeExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// Returns a collection of ancestor elements.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public static IEnumerable<DependencyObject> Ancestors(this DependencyObject item)
        {
            ILinqTree<DependencyObject> adapter = new VisualTreeAdapter(item);

            DependencyObject parent = adapter.Parent;
            while (parent != null)
            {
                yield return parent;
                adapter = new VisualTreeAdapter(parent);
                parent = adapter.Parent;
            }
        }

        /// <summary>
        /// Returns a collection of ancestor elements which match the given type.
        /// </summary>
        /// <typeparam name="T">
        /// The ancestor type.
        /// </typeparam>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{DependencyObject}"/>.
        /// </returns>
        public static IEnumerable<DependencyObject> Ancestors<T>(this DependencyObject item)
        {
            return item.Ancestors().Where(i => i is T).Cast<DependencyObject>();
        }

        #endregion
    }
}