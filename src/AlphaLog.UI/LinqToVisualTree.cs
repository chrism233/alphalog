﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LinqToVisualTree.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   Adapts a DependencyObject to provide methods required for generate
//   a Linq To Tree API
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LinqToVisualTree
{
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Media;

    using AlphaLog.UI;

    /// <summary>
    ///     Adapts a DependencyObject to provide methods required for generate
    ///     a Linq To Tree API
    /// </summary>
    public class VisualTreeAdapter : ILinqTree<DependencyObject>
    {
        #region Fields

        /// <summary>
        ///     The item.
        /// </summary>
        private readonly DependencyObject item;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="VisualTreeAdapter"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public VisualTreeAdapter(DependencyObject item)
        {
            this.item = item;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the parent.
        /// </summary>
        public DependencyObject Parent
        {
            get
            {
                return VisualTreeHelper.GetParent(this.item);
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The children.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerable" />.
        /// </returns>
        public IEnumerable<DependencyObject> Children()
        {
            int childrenCount = VisualTreeHelper.GetChildrenCount(this.item);
            for (int i = 0; i < childrenCount; i++)
            {
                yield return VisualTreeHelper.GetChild(this.item, i);
            }
        }

        #endregion
    }
}