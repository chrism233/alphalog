﻿namespace AlphaLog.UI
{
    using System.ComponentModel;
    using System.Diagnostics.CodeAnalysis;

    using Microsoft.Practices.Prism.PubSubEvents;

    /// <summary>
    ///     The font changed event.
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Reviewed. Suppression is OK here.")]
    public class FormatChangedEvent : PubSubEvent<DataGridFormat>
    {
    }

    public class DataGridFormat
    {
        /// <summary>
        /// Gets or sets the font family to be used in the data grid.
        /// </summary>
        public string FontFamily { get; set; }

        /// <summary>
        /// Gets or sets the font size 
        /// </summary>
        public double FontSize { get; set; }

        /// <summary>
        /// Gets or sets the datetime format to be used in the data grid. 
        /// </summary>
        public string TimestampFormat { get; set; }
    }

    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Reviewed. Suppression is OK here.")]
    public class ScrollBehaviorChangedEvent : PubSubEvent<ScrollBehavior>
    {
    }

    public enum ActivateScroll
    {
        [Description("On scroll to end.")]
        OnScrollToEnd,
        
        [Description("Never")]
        Never
    }

    public enum DeactivateScroll
    {
        [Description("On scroll")]
        OnScroll,

        [Description("Never")]
        Never
    }

    public class ScrollBehavior 
    {
        public ActivateScroll ActivateScroll { get; set; }

        public DeactivateScroll DeactivateScroll { get; set; }

        public int RefreshRate { get; set;}
    }
}