﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainMenuVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The main menu vm.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.ViewModel
{
    using System.IO;
    using System.Windows.Forms;
    using System.Windows.Input;

    using Microsoft.Practices.Prism.Commands;

    using OpenFileDialog = Microsoft.Win32.OpenFileDialog;

    /// <summary>
    ///     The main menu vm.
    /// </summary>
    public class MainMenuVM
    {
        #region Fields

        /// <summary>
        ///     The filter.
        /// </summary>
        private readonly FilterVM filter;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenuVM"/> class.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        public MainMenuVM(FilterVM filter)
        {
            this.OpenFileCommand = new DelegateCommand(this.OpenFile);
            this.OpenFolderCommand = new DelegateCommand(this.OpenFolder);
            this.filter = filter;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the open file command.
        /// </summary>
        public ICommand OpenFileCommand { get; protected set; }

        /// <summary>
        ///     Gets or sets the open folder command.
        /// </summary>
        public ICommand OpenFolderCommand { get; protected set; }

        #endregion

        #region Methods

        /// <summary>
        ///     The open file.
        /// </summary>
        private void OpenFile()
        {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Open log file";
            openFileDialog.Multiselect = true;
            bool? dialogResult = openFileDialog.ShowDialog();
            if (dialogResult.Value)
            {
                string[] fileName = openFileDialog.FileNames;
                this.filter.AddFiles(fileName);
            }
        }

        /// <summary>
        ///     The open folder.
        /// </summary>
        private void OpenFolder()
        {
            var d = new FolderBrowserDialog();
            DialogResult dialogResult = d.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                string[] files = Directory.GetFiles(d.SelectedPath, "*", SearchOption.AllDirectories);
                this.filter.AddFiles(files);
            }
        }

        #endregion
    }
}