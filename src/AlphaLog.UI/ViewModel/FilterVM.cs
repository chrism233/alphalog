﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FilterVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The filter vm.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel.Composition;
    using System.IO;
    using System.Linq;
    using System.Windows.Forms;
    using System.Windows.Input;

    using AlphaLog.Matcher.Matchers;
    using AlphaLog.UI.Common;
    using AlphaLog.UI.Common.Models;

    using Microsoft.Practices.Prism.Commands;
    using Microsoft.Practices.Prism.Mvvm;

    using OpenFileDialog = Microsoft.Win32.OpenFileDialog;

    /// <summary>
    ///     The filter vm.
    /// </summary>
    [Export]
    public class FilterVM : BindableBase
    {
        #region Fields

        /// <summary>
        ///     The files.
        /// </summary>
        private readonly ObservableCollection<Details> files;

        /// <summary>
        ///     The log parser.
        /// </summary>
        private readonly AlphaLogParser logParser;

        /// <summary>
        ///     The settings.
        /// </summary>
        private readonly ISettingsManager settings;

        /// <summary>
        ///     The date time filtering enabled.
        /// </summary>
        private bool dateTimeFilteringEnabled;

        /// <summary>
        ///     The from date time.
        /// </summary>
        private DateTime fromDateTime;

        /// <summary>
        ///     The from is enabled.
        /// </summary>
        private bool fromIsEnabled;

        /// <summary>
        ///     The matcher items.
        /// </summary>
        private ObservableCollection<MatcherItem> matcherItems;

        /// <summary>
        ///     The selected matcher.
        /// </summary>
        private MatcherItem selectedMatcher;

        /// <summary>
        ///     The show filter.
        /// </summary>
        private bool showFilter;

        /// <summary>
        ///     The to date time.
        /// </summary>
        private DateTime toDateTime;

        /// <summary>
        ///     The to is enabled.
        /// </summary>
        private bool toIsEnabled;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FilterVM"/> class.
        /// </summary>
        /// <param name="parser">
        /// The parser instance.
        /// </param>
        /// <param name="settings">
        /// The settings.
        /// </param>
        [ImportingConstructor]
        public FilterVM(AlphaLogParser parser, ISettingsManager settings)
        {
            this.logParser = parser;
            this.FromDateTime = DateTime.Now;
            this.ToDateTime = DateTime.Now;
            this.settings = settings;
            this.EnableDateTimeFilteringCommand = new DelegateCommand(this.EnableDateTimeFilteingCommand);
            this.AddFileCommand = new DelegateCommand(this.AddFile);
            this.AddFolderCommand = new DelegateCommand(this.AddFolder);
            this.RemoveFileCommand = new DelegateCommand<Details>(this.RemoveFile);
            this.ManageMatchersCommand = new DelegateCommand<MatcherItem>(this.ManageMatcher);

            this.files = new ObservableCollection<Details>();

            this.MatcherItems = new ObservableCollection<MatcherItem>();
            this.MatcherItems.Add(new MatcherItem("No Filter", new AlwaysMatcher()));
            settings.GetsSetting<MatcherDatabase>().GetItems().ToList().ForEach(m => this.MatcherItems.Add(m));
            this.SelectedMatcher = this.MatcherItems.FirstOrDefault();
        }

        #endregion

        #region Public Events

        /// <summary>
        ///     The manage matcher event.
        /// </summary>
        public event Action<MatcherItem> ManageMatcherEvent;

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the add file command.
        /// </summary>
        public ICommand AddFileCommand { get; set; }

        /// <summary>
        ///     Gets or sets the add folder command.
        /// </summary>
        public ICommand AddFolderCommand { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether date time filtering enabled.
        /// </summary>
        public bool DateTimeFilteringEnabled
        {
            get
            {
                return this.dateTimeFilteringEnabled;
            }

            set
            {
                this.dateTimeFilteringEnabled = value;
                this.OnPropertyChanged("DateTimeFilteringEnabled");
            }
        }

        /// <summary>
        ///     Gets or sets the enable date time filtering command.
        /// </summary>
        public ICommand EnableDateTimeFilteringCommand { get; set; }

        /// <summary>
        ///     Gets the files.
        /// </summary>
        public ObservableCollection<Details> Files
        {
            get
            {
                return this.files;
            }
        }

        /// <summary>
        ///     Gets or sets the from date time.
        /// </summary>
        public DateTime FromDateTime
        {
            get
            {
                return this.fromDateTime;
            }

            protected set
            {
                this.fromDateTime = value;
                this.OnPropertyChanged("FromDateTime");
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether if the from date time is enabled.
        /// </summary>
        public bool FromIsEnabled
        {
            get
            {
                return this.fromIsEnabled;
            }

            set
            {
                this.SetProperty(ref this.fromIsEnabled, value);
            }
        }

        /// <summary>
        ///     Gets or sets the manage matchers command.
        /// </summary>
        public ICommand ManageMatchersCommand { get; set; }

        /// <summary>
        ///     Gets or sets the matcher items.
        /// </summary>
        public ObservableCollection<MatcherItem> MatcherItems
        {
            get
            {
                return this.matcherItems;
            }

            set
            {
                this.matcherItems = value;
                this.OnPropertyChanged("MatcherItems");
            }
        }

        /// <summary>
        ///     Gets or sets the remove file command.
        /// </summary>
        public ICommand RemoveFileCommand { get; set; }

        /// <summary>
        ///     Gets or sets the selected matcher.
        /// </summary>
        public MatcherItem SelectedMatcher
        {
            get
            {
                return this.selectedMatcher;
            }

            set
            {
                this.selectedMatcher = value;
                this.OnPropertyChanged("SelectedMatcher");
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether show filter.
        /// </summary>
        public bool ShowFilter
        {
            get
            {
                return this.showFilter;
            }

            set
            {
                this.showFilter = value;
                this.OnPropertyChanged("ShowFilter");
            }
        }

        /// <summary>
        ///     Gets or sets the to date time.
        /// </summary>
        public DateTime ToDateTime
        {
            get
            {
                return this.toDateTime;
            }

            set
            {
                this.toDateTime = value;
                this.OnPropertyChanged("ToDateTime");
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the to date time is enabled.
        /// </summary>
        public bool ToIsEnabled
        {
            get
            {
                return this.toIsEnabled;
            }

            set
            {
                this.SetProperty(ref this.toIsEnabled, value);
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add files.
        /// </summary>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        public void AddFiles(IEnumerable<string> fileName)
        {
            this.showFilter = true;
            foreach (string file in fileName)
            {
                if (this.Files.All(f => f.FilePath != file))
                {
                    Details regexColumniser = this.logParser.Add(file);
                    this.Files.Add(regexColumniser);
                }
            }
        }

        /// <summary>
        ///     The refresh.
        /// </summary>
        public void Refresh()
        {
            this.MatcherItems = new ObservableCollection<MatcherItem>();
            this.MatcherItems.Add(new MatcherItem("No Filter", new AlwaysMatcher()));
            this.settings.GetsSetting<MatcherDatabase>().GetItems().ToList().ForEach(m => this.MatcherItems.Add(m));
        }

        #endregion

        #region Methods

        /// <summary>
        ///     The add file.
        /// </summary>
        private void AddFile()
        {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Open log file";
            openFileDialog.Multiselect = true;

            bool? dialogResult = openFileDialog.ShowDialog();

            if (dialogResult.Value)
            {
                string[] fileName = openFileDialog.FileNames;

                this.AddFiles(fileName);
            }
        }

        /// <summary>
        ///     The add folder.
        /// </summary>
        private void AddFolder()
        {
            var d = new FolderBrowserDialog();

            DialogResult dialogResult = d.ShowDialog();

            if (dialogResult == DialogResult.OK)
            {
                string[] files = Directory.GetFiles(d.SelectedPath, "*", SearchOption.AllDirectories);

                this.AddFiles(files);
            }
        }

        /// <summary>
        ///     The enable date time filteing command.
        /// </summary>
        private void EnableDateTimeFilteingCommand()
        {
            this.DateTimeFilteringEnabled = !this.DateTimeFilteringEnabled;
        }

        /// <summary>
        /// The manage matcher.
        /// </summary>
        /// <param name="selectedMatcher">
        /// The selected matcher.
        /// </param>
        private void ManageMatcher(MatcherItem selectedMatcher)
        {
            if (this.ManageMatcherEvent != null)
            {
                this.ManageMatcherEvent(selectedMatcher);
            }
        }

        /// <summary>
        /// The remove file.
        /// </summary>
        /// <param name="selected">
        /// The selected.
        /// </param>
        private void RemoveFile(Details selected)
        {
            if (selected != null)
            {
                this.Files.Remove(selected);
                this.logParser.Remove(selected.FilePath);
            }
        }

        #endregion
    }
}