﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainWindowVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   View Model for the Main Window
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Globalization;
    using System.Reactive.Linq;
    using System.Windows.Input;
    using System.Windows.Threading;

    using AlphaLog.Columniser;
    using AlphaLog.Matcher;
    using AlphaLog.Matcher.Matchers.Group;
    using AlphaLog.Matcher.Matchers.Numeric;
    using AlphaLog.UI.Common;
    using AlphaLog.UI.Common.Models;
    using AlphaLog.UI.Properties;

    using Microsoft.Practices.Prism.Commands;
    using Microsoft.Practices.Prism.Interactivity.InteractionRequest;
    using Microsoft.Practices.Prism.Mvvm;

    /// <summary>
    ///     View Model for the Main Window
    /// </summary>
    [Export]
    public class MainWindowVM : BindableBase
    {
        #region Fields

        /// <summary>
        ///     The dispatcher.
        /// </summary>
        private readonly Dispatcher dispatcher;

        /// <summary>
        ///     The parser.
        /// </summary>
        private readonly AlphaLogParser parser;

        /// <summary>
        ///     The refresh timer.
        /// </summary>
        private readonly DispatcherTimer refreshTimer;

        /// <summary>
        ///     The settings.
        /// </summary>
        private readonly ISettingsManager settings;

        /// <summary>
        ///     The is tailing.
        /// </summary>
        private bool isTailing;

        /// <summary>
        ///     The log items.
        /// </summary>
        private DynamicItemCollection<LogDescriptorCollection> logItems;

        /// <summary>
        ///     The update observer.
        /// </summary>
        private IDisposable updateObserver;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindowVM"/> class.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        [ImportingConstructor]
        public MainWindowVM(ISettingsManager settings)
        {
            this.parser = new AlphaLogParser(Mef.GetExports<IColumnizerDescriptor>());

            this.ShowOptionsCommand = new DelegateCommand(this.ShowOptions);
            this.SearchCommand = new DelegateCommand(this.Search);
            this.RaiseSettingsRequest = new InteractionRequest<INotification>();
            this.TailStateChangedCommand = new DelegateCommand<object>(this.EnableTail);
            this.settings = settings;

            this.parser.Progress.Subscribe(p => this.Progress.Progress = p.Progress);

            this.Filter = new FilterVM(this.parser, settings);
            this.Filter.ManageMatcherEvent += this.Filter_ManageMatcherEvent;
            this.Filter.ShowFilter = false;
            this.MainMenu = new MainMenuVM(this.Filter);
            this.Progress = new ProgressVM();

            this.refreshTimer = new DispatcherTimer();
            this.refreshTimer.Interval = TimeSpan.FromMilliseconds(Settings.Default.RefreshRate);
            this.refreshTimer.Tick += this.UpdateTick;
            this.dispatcher = Dispatcher.CurrentDispatcher;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the filter.
        /// </summary>
        public FilterVM Filter { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether is tailing.
        /// </summary>
        public bool IsTailing
        {
            get
            {
                return this.isTailing;
            }

            set
            {
                this.SetProperty(ref this.isTailing, value);
            }
        }

        /// <summary>
        ///     Gets or sets the log items.
        /// </summary>
        public DynamicItemCollection<LogDescriptorCollection> LogItems
        {
            get
            {
                return this.logItems;
            }

            set
            {
                this.SetProperty(ref this.logItems, value);
            }
        }

        /// <summary>
        ///     Gets or sets the main menu view-model.
        /// </summary>
        public MainMenuVM MainMenu { get; set; }

        /// <summary>
        ///     Gets or sets the progress view-model.
        /// </summary>
        public ProgressVM Progress { get; set; }

        /// <summary>
        ///     Gets or sets the raise settings request.
        /// </summary>
        public InteractionRequest<INotification> RaiseSettingsRequest { get; protected set; }

        /// <summary>
        ///     Gets or sets the search command.
        /// </summary>
        public ICommand SearchCommand { get; protected set; }

        /// <summary>
        ///     Gets or sets the show options command.
        /// </summary>
        public ICommand ShowOptionsCommand { get; protected set; }

        /// <summary>
        ///     Gets the tail state changed command.
        /// </summary>
        public ICommand TailStateChangedCommand { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        /// The enable tail.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        private void EnableTail(object obj)
        {
            if (obj == null)
            {
                return;
            }

            if ((bool)obj)
            {
                this.IsTailing = true;
                this.refreshTimer.Start();
            }
            else
            {
                this.IsTailing = false;
                this.refreshTimer.Stop();
            }
        }

        /// <summary>
        /// The filter_ manage matcher event.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        private void Filter_ManageMatcherEvent(MatcherItem obj)
        {
            this.ShowOptions();
        }

        /// <summary>
        /// The reset.
        /// </summary>
        private void Reset()
        {
            this.Progress.Progress = 0;
            if (this.updateObserver != null)
            {
                this.updateObserver.Dispose();
            }

            this.parser.Reset();
            if (this.LogItems != null)
            {
                this.LogItems.Clear();
            }
        }

        /// <summary>
        ///     The search.
        /// </summary>
        private async void Search()
        {
            this.Progress.IsIndeterminate = true;

            DateTime from = this.Filter.FromIsEnabled ? this.Filter.FromDateTime : DateTime.MinValue;
            DateTime to = this.Filter.ToIsEnabled ? this.Filter.ToDateTime : DateTime.MaxValue;

            var dateRange =
                new And(
                    new GreaterThanMatcher(@from.ToUnixTime().ToString(CultureInfo.InvariantCulture), "Timestamp"), 
                    new LessThanMatcher(to.ToUnixTime().ToString(CultureInfo.InvariantCulture), "Timestamp"));

            IMatch matcher = new And(this.Filter.SelectedMatcher.Matcher, dateRange);

            var logItems = new List<LogDescriptorCollection>();

            // Reset 
            this.Reset();

            // Process the initial results.
            using (this.parser.Where(matcher.IsMatch).Subscribe(i => logItems.Add(new LogDescriptorCollection(i))))
            {
                await this.parser.Update();
                this.LogItems = new DynamicItemCollection<LogDescriptorCollection>(logItems);
                logItems.Clear();
            }

            // Instantiate an observer to handle updates.
            this.updateObserver =
                this.parser.ObserveOn(this.dispatcher)
                    .Where(matcher.IsMatch)
                    .Subscribe(i => this.LogItems.Add(new LogDescriptorCollection(i)));
        }

        /// <summary>
        ///     The show options.
        /// </summary>
        private void ShowOptions()
        {
            this.RaiseSettingsRequest.Raise(new Notification { Title = "AlphaLog - Settings" });
        }

        /// <summary>
        /// The update tick.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void UpdateTick(object sender, EventArgs e)
        {
            this.parser.Update();
        }

        #endregion
    }
}