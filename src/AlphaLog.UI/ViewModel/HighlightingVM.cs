﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HighlightingVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The highlighting view-model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.ViewModel
{
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Linq;
    using System.Windows.Input;
    using System.Windows.Media;

    using AlphaLog.Matcher.Matchers.Text;
    using AlphaLog.UI.Common;
    using AlphaLog.UI.Common.Models;
    using AlphaLog.UI.Matcher.ViewModel;

    using Microsoft.Practices.Prism.Commands;

    /// <summary>
    ///     The highlighting view-model.
    /// </summary>
    [Export]
    public class HighlightingVM : TabSettingsVM
    {
        #region Fields

        /// <summary>
        ///     The settings manager.
        /// </summary>
        private readonly ISettingsManager settingsManager;

        /// <summary>
        ///     The background color.
        /// </summary>
        private Color backgroundColor;

        /// <summary>
        ///     The fileds.
        /// </summary>
        private List<string> fileds;

        /// <summary>
        ///     The foreground color.
        /// </summary>
        private Color foregroundColor;

        /// <summary>
        ///     The matcher.
        /// </summary>
        private MatcherControlVM matcher;

        /// <summary>
        ///     The selected highlighter.
        /// </summary>
        private HighlightItem selectedHighlighter;

        /// <summary>
        ///     The selectedindex.
        /// </summary>
        private int selectedindex;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="HighlightingVM"/> class.
        /// </summary>
        /// <param name="settingsManager">
        /// The settings Manager.
        /// </param>
        [ImportingConstructor]
        public HighlightingVM(ISettingsManager settingsManager)
            : base("Highlighting")
        {
            this.settingsManager = settingsManager;
            this.MoveUpCommand = new DelegateCommand(this.MoveUp);
            this.MoveDownCommand = new DelegateCommand(this.MoveDown);
            this.AddCommand = new DelegateCommand(this.Add);
            this.DeleteCommand = new DelegateCommand(this.Delete);
            this.ApplyCommand = new DelegateCommand(this.Apply);
            this.fileds = settingsManager.GetsSetting<FieldSettings>().GetItems().Select(f => f.Title).ToList();

            var temp = new HighlightItem(Colors.Red, Colors.Blue, new RegexMatcher("Title", string.Empty, false));

            // var temp1 = new HighlightItem(0, Colors.Red, Colors.Blue, new ContainsMatcher("Title", string.Empty, false));
            this.HighligherItems =
                new ObservableCollectionWithPropertyChanged<HighlightItem>(
                    settingsManager.GetsSetting<HighlightDatabase>().GetItems());
            this.SelectedHighlighter = temp;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the add command.
        /// </summary>
        public ICommand AddCommand { get; private set; }

        /// <summary>
        ///     Gets the apply command.
        /// </summary>
        public ICommand ApplyCommand { get; private set; }

        /// <summary>
        ///     Gets or sets the background color.
        /// </summary>
        public Color BackgroundColor
        {
            get
            {
                return this.backgroundColor;
            }

            set
            {
                this.SetProperty(ref this.backgroundColor, value);
            }
        }

        /// <summary>
        ///     Gets the delete command.
        /// </summary>
        public ICommand DeleteCommand { get; private set; }

        /// <summary>
        ///     Gets or sets the foreground color.
        /// </summary>
        public Color ForegroundColor
        {
            get
            {
                return this.foregroundColor;
            }

            set
            {
                this.SetProperty(ref this.foregroundColor, value);
            }
        }

        /// <summary>
        ///     Gets or sets the highligher items.
        /// </summary>
        public ObservableCollectionWithPropertyChanged<HighlightItem> HighligherItems { get; set; }

        /// <summary>
        ///     Gets or sets the matcher.
        /// </summary>
        public MatcherControlVM Matcher
        {
            get
            {
                return this.matcher;
            }

            set
            {
                this.SetProperty(ref this.matcher, value);
            }
        }

        /// <summary>
        ///     Gets the move down command.
        /// </summary>
        public ICommand MoveDownCommand { get; private set; }

        /// <summary>
        ///     Gets the move up command.
        /// </summary>
        public ICommand MoveUpCommand { get; private set; }

        /// <summary>
        ///     Gets or sets the selected highlighter.
        /// </summary>
        public HighlightItem SelectedHighlighter
        {
            get
            {
                return this.selectedHighlighter;
            }

            set
            {
                if (value != null)
                {
                    this.selectedHighlighter = value;
                    this.ForegroundColor = value.ForegroundColor;
                    this.BackgroundColor = value.BackgroundColor;
                    this.Matcher =
                        new MatcherControlVM(
                            this.settingsManager.GetsSetting<FieldSettings>().GetItems().Select(f => f.Title).ToList(), 
                            value.Matcher);
                    this.OnPropertyChanged("SelectedHighlighter");
                }
            }
        }

        /// <summary>
        ///     Gets or sets the selected index.
        /// </summary>
        public int SelectedIndex
        {
            get
            {
                return this.selectedindex;
            }

            set
            {
                this.SetProperty(ref this.selectedindex, value);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     The save changes.
        /// </summary>
        protected override void SaveChanges()
        {
            base.SaveChanges();
            this.settingsManager.GetsSetting<HighlightDatabase>().Store(this.HighligherItems);
            this.HasUnSavedChanges = false;
        }

        /// <summary>
        ///     The add.
        /// </summary>
        private void Add()
        {
            var temp = new HighlightItem(
                this.SelectedHighlighter.ForegroundColor, 
                this.SelectedHighlighter.BackgroundColor, 
                this.Matcher.GetMatcher());
            this.HighligherItems.Add(temp);
            this.SelectedHighlighter = temp;
            this.HasUnSavedChanges = true;
        }

        /// <summary>
        ///     The apply.
        /// </summary>
        private void Apply()
        {
            int s = this.SelectedIndex;
            this.HighligherItems[this.SelectedIndex] = new HighlightItem(
                this.ForegroundColor, 
                this.BackgroundColor, 
                this.Matcher.GetMatcher());
            this.SelectedIndex = s;
            this.HasUnSavedChanges = true;
        }

        /// <summary>
        ///     The delete.
        /// </summary>
        private void Delete()
        {
            int s = this.SelectedIndex;
            this.HighligherItems.RemoveAt(this.SelectedIndex);
            this.SelectedHighlighter = this.HighligherItems[s > 0 ? s - 1 : 0];
            this.HasUnSavedChanges = true;
        }

        /// <summary>
        ///     The move down.
        /// </summary>
        private void MoveDown()
        {
            if (this.SelectedIndex < (this.HighligherItems.Count() - 1))
            {
                int s = this.SelectedIndex;
                HighlightItem temp = this.HighligherItems[this.SelectedIndex + 1];
                this.HighligherItems[this.SelectedIndex + 1] = this.HighligherItems[this.SelectedIndex];
                this.HighligherItems[this.SelectedIndex] = temp;
                this.SelectedIndex = s + 1;
                this.HasUnSavedChanges = true;
            }
        }

        /// <summary>
        ///     The move up.
        /// </summary>
        private void MoveUp()
        {
            if (this.SelectedIndex > 0)
            {
                int s = this.SelectedIndex;
                HighlightItem temp = this.HighligherItems[this.SelectedIndex - 1];
                this.HighligherItems[this.SelectedIndex - 1] = this.HighligherItems[this.SelectedIndex];
                this.HighligherItems[this.SelectedIndex] = temp;
                this.SelectedIndex = s - 1;
                this.HasUnSavedChanges = true;
            }
        }

        #endregion
    }
}