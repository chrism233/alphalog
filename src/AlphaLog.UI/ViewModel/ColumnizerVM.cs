﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaLog.UI.ViewModel
{
    using System.Collections.ObjectModel;
    using System.ComponentModel.Composition;

    using AlphaLog.UI.Common;

    [Export]
    public class ColumnizerVM : TabSettingsVM
    {
      //  public ObservableCollection<TreeSettingsVM> Columnizers { get; private set; }

        [ImportingConstructor]
        public ColumnizerVM()
            : base("Columnizers")
        {

        }
    }
}
