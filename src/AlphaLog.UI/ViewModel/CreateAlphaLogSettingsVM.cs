﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreateAlphaLogSettingsVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The create alpha log settings vm.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.ViewModel
{
    using System.Windows;
    using System.Windows.Forms;
    using System.Windows.Input;
    using System.Xml.Linq;

    using AlphaLog.UI.Properties;

    using Microsoft.Practices.Prism.Commands;
    using Microsoft.Practices.Prism.Mvvm;

    using Application = System.Windows.Application;

    /// <summary>
    ///     The create alpha log settings vm.
    /// </summary>
    public class CreateAlphaLogSettingsVM : BindableBase
    {
        #region Fields

        /// <summary>
        ///     The window.
        /// </summary>
        private readonly Window window;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CreateAlphaLogSettingsVM"/> class.
        /// </summary>
        /// <param name="createSettingsWindow">
        /// The create settings window.
        /// </param>
        public CreateAlphaLogSettingsVM(Window createSettingsWindow)
        {
            this.window = createSettingsWindow;
            this.CreateSettingsCommand = new DelegateCommand(this.CreateSettings);
            this.ChooseSettingsCommand = new DelegateCommand(this.ChooseSettings);
            this.CloseCommand = new DelegateCommand(this.Close);
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the choose settings command.
        /// </summary>
        public ICommand ChooseSettingsCommand { get; set; }

        /// <summary>
        ///     Gets or sets the close command.
        /// </summary>
        public ICommand CloseCommand { get; set; }

        /// <summary>
        ///     Gets or sets the create settings command.
        /// </summary>
        public ICommand CreateSettingsCommand { get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     The choose settings.
        /// </summary>
        private void ChooseSettings()
        {
            var openDialog = new OpenFileDialog();
            DialogResult result = openDialog.ShowDialog();
            if (result != DialogResult.Cancel)
            {
                Settings.Default.AlphaLogSettingsFileName = openDialog.FileName;
                Settings.Default.Save();
                this.window.Close();
            }
        }

        /// <summary>
        ///     The close.
        /// </summary>
        private void Close()
        {
            this.window.Close();
        }

        /// <summary>
        ///     The create settings.
        /// </summary>
        private void CreateSettings()
        {
            var saveDialog = new SaveFileDialog();
            saveDialog.DefaultExt = "xml";
            DialogResult result = saveDialog.ShowDialog();
            if (result != DialogResult.Cancel)
            {
                new XElement("AlphaLog").Save(saveDialog.FileName);
                Settings.Default.AlphaLogSettingsFileName = saveDialog.FileName;
                Settings.Default.Save();
                this.window.Close();
            }
        }

        #endregion
    }
}