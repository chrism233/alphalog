﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GlobalSettingsVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The global settings view-model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.ViewModel
{
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.ComponentModel.Composition;
    using System.Windows.Forms;
    using System.Windows.Input;

    using AlphaLog.UI.Common;
    using AlphaLog.UI.Properties;

    using Microsoft.Practices.Prism.Commands;
    using Microsoft.Practices.Prism.PubSubEvents;

    /// <summary>
    ///     The global settings view-model.
    /// </summary>
    [Export]
    public class GlobalSettingsVM : TabSettingsVM
    {
        #region Fields

        /// <summary>
        ///     The event aggregator.
        /// </summary>
        private readonly IEventAggregator eventAggregator;

        /// <summary>
        ///     The records to display.
        /// </summary>
        private int recordsToDisplay;

        /// <summary>
        ///     The settings path.
        /// </summary>
        private string settingsPath;

        /// <summary>
        ///     The data grid format.
        /// </summary>
        private DataGridFormat datagridFormat;

        /// <summary>
        ///     The data frid scroll behaviour.
        /// </summary>
        private ScrollBehavior scrollBehavior;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GlobalSettingsVM"/> class.
        /// </summary>
        /// <param name="eventAggregator">
        /// The event Aggregator.
        /// </param>
        [ImportingConstructor]
        public GlobalSettingsVM(IEventAggregator eventAggregator)
            : base("Options")
        {
            this.eventAggregator = eventAggregator;
            this.SelectSettingsCommand = new DelegateCommand(this.SelectSettings);
            this.AddPathCommand = new DelegateCommand(this.AddPath);
            this.RemovePathCommand = new DelegateCommand<string>(this.RemovePath);
            this.AdditionalColumniserSearchPaths = new ObservableCollection<string>(Settings.Default.AdditionalColumniserSearchPaths.Split(','));
            this.Initialize();
            this.PropertyChanged += this.OnPropertyChanged;
        }

        #endregion

        #region Public Properties

        #region Scroll Behaviour 

        /// <summary>
        ///     Gets or sets the activate scroll.
        /// </summary>
        public ActivateScroll ActivateScroll
        {
            get
            {
                return this.scrollBehavior.ActivateScroll;
            }

            set
            {
                this.scrollBehavior.ActivateScroll = value;
                this.OnPropertyChanged("ActivateScroll");
                this.eventAggregator.GetEvent<ScrollBehaviorChangedEvent>().Publish(scrollBehavior);
            }
        }

        /// <summary>
        ///     Gets or sets the deactivate scroll.
        /// </summary>
        public DeactivateScroll DeactivateScroll
        {
            get
            {
                return this.scrollBehavior.DeactivateScroll;
            }

            set
            {
                this.scrollBehavior.DeactivateScroll = value;
                this.OnPropertyChanged("DeactivateScroll");
                this.eventAggregator.GetEvent<ScrollBehaviorChangedEvent>().Publish(this.scrollBehavior);
            }
        }

        /// <summary>
        ///     Gets or sets the refresh rate.
        /// </summary>
        public int RefreshRate
        {
            get
            {
                return this.scrollBehavior.RefreshRate;
            }

            set
            {
                this.scrollBehavior.RefreshRate = value;
                this.OnPropertyChanged("RefreshRate");
                this.eventAggregator.GetEvent<ScrollBehaviorChangedEvent>().Publish(this.scrollBehavior);
            }
        }

        #endregion

        #region Columniser search Paths

        /// <summary>
        ///     Gets or sets the add path command.
        /// </summary>
        public ICommand AddPathCommand { get; set; }

        /// <summary>
        ///     Gets or sets the remove path command.
        /// </summary>
        public ICommand RemovePathCommand { get; set; }
        
        /// <summary>
        ///     Gets or sets the additional columniser search paths.
        /// </summary>
        public ObservableCollection<string> AdditionalColumniserSearchPaths { get; set; }

        #endregion

        #region Datagrid format settings 

        /// <summary>
        ///     Gets or sets the font family.
        /// </summary>
        public string FontFamily
        {
            get
            {
                return this.datagridFormat.FontFamily;
            }

            set
            {
                this.datagridFormat.FontFamily = value;
                this.OnPropertyChanged("FontFamily");
                
                this.eventAggregator.GetEvent<FormatChangedEvent>().Publish(this.datagridFormat);
            }
        }

        /// <summary>
        ///     Gets or sets the font size.
        /// </summary>
        public double FontSize
        {
            get
            {
                return this.datagridFormat.FontSize;
            }

            set
            {
                this.datagridFormat.FontSize = value;
                this.OnPropertyChanged("FintSize");

                this.eventAggregator.GetEvent<FormatChangedEvent>().Publish(this.datagridFormat);
            }
        }

        /// <summary>
        ///     Gets or sets the timestamp format.
        /// </summary>
        public string TimestampFormat
        {
            get
            {
                return this.datagridFormat.TimestampFormat;
            }

            set
            {
                this.datagridFormat.TimestampFormat = value;
                this.OnPropertyChanged("TimestampFormat");

                this.eventAggregator.GetEvent<FormatChangedEvent>().Publish(this.datagridFormat);
            }
        }

        #endregion

        #region Settings path

        /// <summary>
        ///     Gets or sets the select settings command.
        /// </summary>
        public ICommand SelectSettingsCommand { get; set; }

        /// <summary>
        ///     Gets or sets the settings path.
        /// </summary>
        public string SettingsPath
        {
            get
            {
                return this.settingsPath;
            }

            set
            {
                this.SetProperty(ref this.settingsPath, value);
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the settings.
        /// </summary>
        private void Initialize()
        {
            this.datagridFormat = new DataGridFormat();
            this.datagridFormat.FontFamily = Settings.Default.FontFamily;
            this.datagridFormat.FontSize = Settings.Default.FontSize;
            this.datagridFormat.TimestampFormat = Settings.Default.TimestampFormat;

            this.SettingsPath = Settings.Default.AlphaLogSettingsFileName;

            this.scrollBehavior = new ScrollBehavior();
            this.scrollBehavior.ActivateScroll = (ActivateScroll)Settings.Default.ActivateScroll;
            this.scrollBehavior.DeactivateScroll = (DeactivateScroll)Settings.Default.DeactivateScroll;
            this.scrollBehavior.RefreshRate = Settings.Default.RefreshRate;
        }

        /// <summary>
        ///     The save changes.
        /// </summary>
        protected override void SaveChanges()
        {
            Settings.Default.AdditionalColumniserSearchPaths = string.Join(",", this.AdditionalColumniserSearchPaths);
            Settings.Default.AlphaLogSettingsFileName = this.SettingsPath;
            Settings.Default.FontSize = this.FontSize;
            Settings.Default.FontFamily = this.FontFamily;
            Settings.Default.RefreshRate = this.RefreshRate;
            Settings.Default.ActivateScroll = (int)this.ActivateScroll;
            Settings.Default.DeactivateScroll = (int)this.DeactivateScroll;

            Settings.Default.Save();
            base.SaveChanges();
        }

        /// <summary>
        ///     The add path.
        /// </summary>
        private void AddPath()
        {
            var save = new FolderBrowserDialog();
            if (save.ShowDialog() != DialogResult.Cancel)
            {
                this.AdditionalColumniserSearchPaths.Add(save.SelectedPath);
            }
        }

        /// <summary>
        /// The on property changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "HasUnSavedChanges")
            {
                this.HasUnSavedChanges = true;
            }
        }

        /// <summary>
        /// The remove path.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        private void RemovePath(string path)
        {
            this.AdditionalColumniserSearchPaths.Remove(path);
        }

        /// <summary>
        ///     The select settings.
        /// </summary>
        private void SelectSettings()
        {
            var open = new OpenFileDialog();
            open.Multiselect = false;
            if (open.ShowDialog() != DialogResult.Cancel)
            {
                this.SettingsPath = open.FileName;
            }
        }

        #endregion
    }
}