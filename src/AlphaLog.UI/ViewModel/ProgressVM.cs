// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgressVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The progress vm.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AlphaLog.UI.ViewModel
{
    using Microsoft.Practices.Prism.Mvvm;

    /// <summary>
    /// The progress vm.
    /// </summary>
    public class ProgressVM : BindableBase
    {
        #region Fields

        /// <summary>
        ///     The is indeterminate.
        /// </summary>
        private bool isIndeterminate;

        /// <summary>
        ///     The progress.
        /// </summary>
        private int progres;

        /// <summary>
        /// The state.
        /// </summary>
        private ProcessState state;

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets a value indicating whether the progress is indeterminate.
        /// </summary>
        public bool IsIndeterminate
        {
            get
            {
                return this.isIndeterminate;
            }

            set
            {
                this.SetProperty(ref this.isIndeterminate, value);
            }
        }

        /// <summary>
        ///     Gets or sets the progress.
        /// </summary>
        public int Progress
        {
            get
            {
                return this.progres;
            }

            set
            {
                this.SetProperty(ref this.progres, value);
                if (this.progres > 0)
                {
                    this.IsIndeterminate = false;
                }
            }
        }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        public ProcessState State
        {
            get
            {
                return this.state;
            }

            set
            {
                this.state = value;
                this.OnPropertyChanged("State");
            }
        }

        #endregion
    }
}