﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainRight.xaml.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   Interaction logic for MainRight.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Views
{
    using System.Windows.Controls;

    /// <summary>
    ///     Interaction logic for MainRight.xaml
    /// </summary>
    public partial class MainRight : UserControl
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="MainRight" /> class.
        /// </summary>
        public MainRight()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}