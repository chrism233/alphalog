﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PiePiece.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   A pie piece shape
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Shapes;

    /// <summary>
    ///     A pie piece shape
    /// </summary>
    public class PiePiece : Canvas
    {
        #region Static Fields

        /// <summary>
        ///     The centre x property.
        /// </summary>
        public static readonly DependencyProperty CentreXProperty = DependencyProperty.Register(
            "CentreX", 
            typeof(double), 
            typeof(PiePiece), 
            new PropertyMetadata(OnDependencyPropertyChanged));

        /// <summary>
        ///     The centre y property.
        /// </summary>
        public static readonly DependencyProperty CentreYProperty = DependencyProperty.Register(
            "CentreY", 
            typeof(double), 
            typeof(PiePiece), 
            new PropertyMetadata(OnDependencyPropertyChanged));

        /// <summary>
        ///     The fill property.
        /// </summary>
        public static readonly DependencyProperty FillProperty = DependencyProperty.Register(
            "Fill", 
            typeof(Brush), 
            typeof(PiePiece), 
            new PropertyMetadata(OnDependencyPropertyChanged));

        /// <summary>
        ///     The inner radius property.
        /// </summary>
        public static readonly DependencyProperty InnerRadiusProperty = DependencyProperty.Register(
            "InnerRadius", 
            typeof(double), 
            typeof(PiePiece), 
            new PropertyMetadata(OnDependencyPropertyChanged));

        /// <summary>
        ///     The radius property.
        /// </summary>
        public static readonly DependencyProperty RadiusProperty = DependencyProperty.Register(
            "Radius", 
            typeof(double), 
            typeof(PiePiece), 
            new PropertyMetadata(OnDependencyPropertyChanged));

        /// <summary>
        ///     The rotation angle property.
        /// </summary>
        public static readonly DependencyProperty RotationAngleProperty = DependencyProperty.Register(
            "RotationAngle", 
            typeof(double), 
            typeof(PiePiece), 
            new PropertyMetadata(OnDependencyPropertyChanged));

        /// <summary>
        ///     The stroke property.
        /// </summary>
        public static readonly DependencyProperty StrokeProperty = DependencyProperty.Register(
            "Stroke", 
            typeof(Brush), 
            typeof(PiePiece), 
            new PropertyMetadata(OnDependencyPropertyChanged));

        /// <summary>
        ///     The wedge angle property.
        /// </summary>
        public static readonly DependencyProperty WedgeAngleProperty = DependencyProperty.Register(
            "WedgeAngle", 
            typeof(double), 
            typeof(PiePiece), 
            new PropertyMetadata(OnDependencyPropertyChanged));

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="PiePiece" /> class.
        /// </summary>
        public PiePiece()
        {
            this.AddPathToCanvas();
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the Y coordinate of centre of the circle from which this pie piece is cut.
        /// </summary>
        public double CentreX
        {
            get
            {
                return (double)this.GetValue(CentreXProperty);
            }

            set
            {
                this.SetValue(CentreXProperty, value);
            }
        }

        /// <summary>
        ///     Gets or sets the Y coordinate of centre of the circle from which this pie piece is cut.
        /// </summary>
        public double CentreY
        {
            get
            {
                return (double)this.GetValue(CentreYProperty);
            }

            set
            {
                this.SetValue(CentreYProperty, value);
            }
        }

        /// <summary>
        ///     Gets or sets the <see cref="Brush" /> of this pie piece
        /// </summary>
        public Brush Fill
        {
            get
            {
                return (Brush)this.GetValue(FillProperty);
            }

            set
            {
                this.SetValue(FillProperty, value);
            }
        }

        /// <summary>
        ///     Gets or sets the inner radius of this pie piece
        /// </summary>
        public double InnerRadius
        {
            get
            {
                return (double)this.GetValue(InnerRadiusProperty);
            }

            set
            {
                this.SetValue(InnerRadiusProperty, value);
            }
        }

        /// <summary>
        ///     Gets or sets the value that this pie piece represents.
        /// </summary>
        public double PieceValue { get; set; }

        /// <summary>
        ///     Gets or sets the radius of this pie piece
        /// </summary>
        public double Radius
        {
            get
            {
                return (double)this.GetValue(RadiusProperty);
            }

            set
            {
                this.SetValue(RadiusProperty, value);
            }
        }

        /// <summary>
        ///     Gets or sets the rotation, in degrees, from the Y axis vector of this pie piece.
        /// </summary>
        public double RotationAngle
        {
            get
            {
                return (double)this.GetValue(RotationAngleProperty);
            }

            set
            {
                this.SetValue(RotationAngleProperty, value);
            }
        }

        /// <summary>
        ///     Gets or sets the fill color of this pie piece
        /// </summary>
        public Brush Stroke
        {
            get
            {
                return (Brush)this.GetValue(StrokeProperty);
            }

            set
            {
                this.SetValue(StrokeProperty, value);
            }
        }

        /// <summary>
        ///     Gets or sets the wedge angle of this pie piece in degrees
        /// </summary>
        public double WedgeAngle
        {
            get
            {
                return (double)this.GetValue(WedgeAngleProperty);
            }

            set
            {
                this.SetValue(WedgeAngleProperty, value);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The on dependency property changed.
        /// </summary>
        /// <param name="d">
        /// The d.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private static void OnDependencyPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var source = d as PiePiece;
            source.AddPathToCanvas();
        }

        /// <summary>
        ///     The add path to canvas.
        /// </summary>
        private void AddPathToCanvas()
        {
            this.Children.Clear();

            string tooltip = (this.WedgeAngle / 360.00).ToString("#0.##%");

            Path path = this.ConstructPath();
            ToolTipService.SetToolTip(path, tooltip);
            this.Children.Add(path);
        }

        /// <summary>
        ///     Constructs a path that represents this pie segment
        /// </summary>
        /// <returns>
        ///     The <see cref="Path" />.
        /// </returns>
        private Path ConstructPath()
        {
            if (this.WedgeAngle >= 360)
            {
                var path = new Path
                               {
                                   Fill = this.Fill, 
                                   Stroke = this.Stroke, 
                                   StrokeThickness = 1, 
                                   Data =
                                       new GeometryGroup
                                           {
                                               FillRule = FillRule.EvenOdd, 
                                               Children =
                                                   new GeometryCollection
                                                       {
                                                           new EllipseGeometry
                                                               {
                                                                   Center = new Point(this.CentreX, this.CentreY), 
                                                                   RadiusX = this.Radius, 
                                                                   RadiusY = this.Radius
                                                               }, 
                                                           new EllipseGeometry
                                                               {
                                                                   Center = new Point(this.CentreX, this.CentreY), 
                                                                   RadiusX = this.InnerRadius, 
                                                                   RadiusY = this.InnerRadius
                                                               }
                                                       }, 
                                           }
                               };
                return path;
            }

            var startPoint = new Point(this.CentreX, this.CentreY);

            Point innerArcStartPoint = Utils.ComputeCartesianCoordinate(this.RotationAngle, this.InnerRadius);
            innerArcStartPoint.Offset(this.CentreX, this.CentreY);
            Point innerArcEndPoint = Utils.ComputeCartesianCoordinate(
                this.RotationAngle + this.WedgeAngle, 
                this.InnerRadius);
            innerArcEndPoint.Offset(this.CentreX, this.CentreY);
            Point outerArcStartPoint = Utils.ComputeCartesianCoordinate(this.RotationAngle, this.Radius);
            outerArcStartPoint.Offset(this.CentreX, this.CentreY);
            Point outerArcEndPoint = Utils.ComputeCartesianCoordinate(this.RotationAngle + this.WedgeAngle, this.Radius);
            outerArcEndPoint.Offset(this.CentreX, this.CentreY);

            bool largeArc = this.WedgeAngle > 180.0;
            var outerArcSize = new Size(this.Radius, this.Radius);
            var innerArcSize = new Size(this.InnerRadius, this.InnerRadius);

            var figure = new PathFigure
                             {
                                 StartPoint = innerArcStartPoint, 
                                 Segments =
                                     new PathSegmentCollection
                                         {
                                             new LineSegment { Point = outerArcStartPoint }, 
                                             new ArcSegment
                                                 {
                                                     Point = outerArcEndPoint, 
                                                     Size = outerArcSize, 
                                                     IsLargeArc = largeArc, 
                                                     SweepDirection =
                                                         SweepDirection.Clockwise, 
                                                     RotationAngle = 0
                                                 }, 
                                             new LineSegment { Point = innerArcEndPoint }, 
                                             new ArcSegment
                                                 {
                                                     Point = innerArcStartPoint, 
                                                     Size = innerArcSize, 
                                                     IsLargeArc = largeArc, 
                                                     SweepDirection =
                                                         SweepDirection
                                                         .Counterclockwise, 
                                                     RotationAngle = 0
                                                 }
                                         }
                             };

            return new Path
                       {
                           Fill = this.Fill, 
                           Stroke = this.Stroke, 
                           StrokeThickness = 1, 
                           Data = new PathGeometry { Figures = new PathFigureCollection { figure } }
                       };
        }

        #endregion
    }
}