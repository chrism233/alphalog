﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SaveColumnSettingsTrigger.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The save column settings trigger.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Triggers
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Interactivity;

    using AlphaLog.UI.Common;
    using AlphaLog.UI.Common.Models;
    using AlphaLog.UI.Filtering;
    using AlphaLog.UI.Filtering.ViewModels;

    /// <summary>
    ///     The save column settings trigger.
    /// </summary>
    public class SaveColumnSettingsTrigger : TriggerBase<FilteringDataGrid>
    {
        #region Fields

        /// <summary>
        ///     The settings.
        /// </summary>
        private ISettingsManager settings;

        #endregion

        #region Methods

        /// <summary>
        ///     The on attached.
        /// </summary>
        protected override void OnAttached()
        {
            this.settings = Mef.GetExport<ISettingsManager>();
            this.AssociatedObject.LayoutUpdated += this.AssociatedObject_LayoutUpdated;
        }

        /// <summary>
        /// The associated object_ layout updated.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void AssociatedObject_LayoutUpdated(object sender, EventArgs e)
        {
            foreach (DataGridColumn column in this.AssociatedObject.Columns)
            {
                var columnHeader = column.Header as ColumnHeaderVM;
                if (columnHeader != null)
                {
                    this.settings.GetsSetting<FieldSettings>()
                        .Store(
                            new FieldItem(
                                columnHeader.Title, 
                                columnHeader.DataType,
                                column.Width.Value,
                                column.Visibility == Visibility.Visible,
                                column.DisplayIndex,
                                columnHeader.Serialize()));
                }
            }
        }

        #endregion
    }
}