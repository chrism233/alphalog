﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TreeViewRegionBehavior.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The tree node vm.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Triggers
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.ComponentModel.Composition;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Interactivity;

    using AlphaLog.UI.Common;

    using Microsoft.Practices.Prism.Mvvm;
    using Microsoft.Practices.Prism.Regions;
    using Microsoft.Practices.ServiceLocation;

    /// <summary>
    ///     The tree node vm.
    /// </summary>
    public class TreeNodeVM : BindableBase
    {
        #region Constants

        /// <summary>
        ///     The end of node marker.
        /// </summary>
        private const string EndOfNodeMarker = ".";

        #endregion

        #region Fields

        /// <summary>
        ///     The childeren.
        /// </summary>
        private readonly ObservableCollection<TreeNodeVM> childeren = new ObservableCollection<TreeNodeVM>();

        /// <summary>
        ///     The parent.
        /// </summary>
        private readonly TreeNodeVM parent;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TreeNodeVM"/> class.
        /// </summary>
        /// <param name="nodePath">
        /// The node path.
        /// </param>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        public TreeNodeVM(string nodePath, TreeNodeVM parent, object value)
        {
            this.parent = parent;
            string[] nodes = nodePath.Split(char.Parse(EndOfNodeMarker));
            this.Name = nodes[0];

            if (nodes.Length == 1)
            {
                this.View = value;
            }

            if (nodes.Length >= 2)
            {
                TreeNodeVM child = this.childeren.FirstOrDefault(c => c.Name == nodes[1]);
                if (child != null)
                {
                    child.AddChild(string.Join(EndOfNodeMarker, nodes.Skip(1)), value);
                }
                else
                {
                    this.Children.Add(new TreeNodeVM(string.Join(EndOfNodeMarker, nodes.Skip(1)), this, value));
                }
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the children.
        /// </summary>
        public ObservableCollection<TreeNodeVM> Children
        {
            get
            {
                return this.childeren;
            }
        }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Gets the tree path.
        /// </summary>
        public string TreePath
        {
            get
            {
                return this.parent != null ? string.Concat(this.parent.TreePath, ".", this.Name) : this.Name;
            }
        }

        /// <summary>
        ///     Gets or sets the view.
        /// </summary>
        public object View { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get first node.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetFirstNode(string path)
        {
            return path.Substring(0, path.IndexOf('.') >= 0 ? path.IndexOf('.') : path.Length);
        }

        /// <summary>
        /// The get nodes.
        /// </summary>
        /// <param name="treePaths">
        /// The tree paths.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public static IEnumerable<TreeNodeVM> GetNodes(Dictionary<string, object> treePaths)
        {
            var nodes = new List<TreeNodeVM>();
            foreach (var node in treePaths)
            {
                string rootNode = GetFirstNode(node.Key);
                TreeNodeVM nodel = nodes.FirstOrDefault(r => r.Name == rootNode);
                if (nodel != null)
                {
                    nodel.AddChild(node.Key.Remove(0, node.Key.IndexOf('.') + 1), node.Value);
                }
                else
                {
                    nodes.Add(new TreeNodeVM(node.Key, null, node.Value));
                }
            }

            return nodes;
        }

        /// <summary>
        /// The add child.
        /// </summary>
        /// <param name="remove">
        /// The remove.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        public void AddChild(string remove, object value)
        {
            this.Children.Add(new TreeNodeVM(remove, this, value));
        }

        #endregion
    }

    /// <summary>
    ///     The tree view region behavior.
    /// </summary>
    [Export]
    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", 
        Justification = "Reviewed. Suppression is OK here.")]
    public class TreeViewRegionBehavior : Behavior<TreeView>
    {
        #region Fields

        /// <summary>
        ///     The region manager.
        /// </summary>
        private readonly IRegionManager regionManager;

        /// <summary>
        ///     The region.
        /// </summary>
        private IRegion region;

        /// <summary>
        ///     The tree path to view.
        /// </summary>
        private Dictionary<string, object> treePathToView;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="TreeViewRegionBehavior" /> class.
        /// </summary>
        public TreeViewRegionBehavior()
        {
            this.regionManager = ServiceLocator.Current.GetInstance<IRegionManager>();
            this.regionManager.Regions.CollectionChanged += this.Regions_CollectionChanged;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     The on attached.
        /// </summary>
        protected override void OnAttached()
        {
        }

        /// <summary>
        /// The associated object_ selected item changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void AssociatedObject_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var newValue = e.NewValue as TreeNodeVM;
            if (newValue != null)
            {
                if (this.treePathToView.ContainsKey(newValue.TreePath))
                {
                    this.regionManager.Regions[RegionNames.SettingsTab].Activate(this.treePathToView[newValue.TreePath]);
                }
            }
        }

        /// <summary>
        /// The regions_ collection changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void Regions_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                if (this.regionManager.Regions.ContainsRegionWithName(RegionNames.SettingsTab))
                {
                    this.region = this.regionManager.Regions[RegionNames.SettingsTab];
                    this.region.Views.CollectionChanged += this.Views_CollectionChanged;
                    this.treePathToView =
                        this.region.Views.ToDictionary(
                            k =>
                            k.GetType()
                                .GetCustomAttributes(typeof(ViewExportAttribute), false)
                                .Cast<ViewExportAttribute>()
                                .FirstOrDefault()
                                .TreePath, 
                            v => v);

                    TreeNodeVM.GetNodes(this.treePathToView).ToList().ForEach(i => this.AssociatedObject.Items.Add(i));
                    this.AssociatedObject.SelectedItemChanged += this.AssociatedObject_SelectedItemChanged;
                }
            }
        }

        /// <summary>
        /// The views_ collection changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void Views_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
        }

        #endregion
    }
}