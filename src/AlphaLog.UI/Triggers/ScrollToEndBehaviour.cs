﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScrollToEndBehaviour.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The scroll to end behaviour.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Triggers
{
    using System;
    using System.Collections.Specialized;
    using System.Diagnostics.CodeAnalysis;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Interactivity;
    using System.Windows.Media;

    using AlphaLog.UI.Filtering;

    /// <summary>
    ///     The scroll to end behaviour.
    /// </summary>
    public class ScrollToEndBehaviour : TriggerBase<FilteringDataGrid>
    {
        #region Static Fields

        /// <summary>
        ///     The scroll enabled property.
        /// </summary>
        public static readonly DependencyProperty ScrollEnabledProperty = DependencyProperty.Register(
            "ScrollEnabled", 
            typeof(bool), 
            typeof(ScrollToEndBehaviour), 
            new PropertyMetadata(PropertyChanged));

        #endregion

        #region Fields

        /// <summary>
        ///     The scroll.
        /// </summary>
        private ScrollViewer scroll;

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets a value indicating whether scroll enabled.
        /// </summary>
        public bool ScrollEnabled
        {
            get
            {
                return (bool)this.GetValue(ScrollEnabledProperty);
            }

            set
            {
                this.SetValue(ScrollEnabledProperty, value);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     The on attached.
        /// </summary>
        protected override void OnAttached()
        {
            this.AssociatedObject.LoadingRow += this.AssociatedObject_Initialized;
        }

        /// <summary>
        /// The property changed.
        /// </summary>
        /// <param name="d">
        /// The d.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private static void PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var instance = d as ScrollToEndBehaviour;
            if (instance != null)
            {
                instance.ScrollToEnd();
            }
        }

        /// <summary>
        /// The associated object_ initialized.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void AssociatedObject_Initialized(object sender, EventArgs e)
        {
            if (this.scroll == null)
            {
                var coll = this.AssociatedObject.ItemsSource as INotifyCollectionChanged;
                if (coll != null)
                {
                    coll.CollectionChanged += this.coll_CollectionChanged;
                }

                var border = VisualTreeHelper.GetChild(this.AssociatedObject, 0) as Decorator;
                if (border != null)
                {
                    var scroll = border.Child as ScrollViewer;
                    if (scroll != null)
                    {
                        scroll.ScrollChanged += this.ScrollChanged;
                        this.scroll = scroll;
                    }
                }
            }
        }

        /// <summary>
        /// The scroll_ scroll changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var scrollViewer = (ScrollViewer)sender;

            if (scrollViewer.ScrollableHeight == 0)
            {
                return;
            }

            if (scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
            {
                this.ScrollEnabled = true;
                this.InvokeActions(this.ScrollEnabled);
            }
            else if (scrollViewer.ScrollableHeight - scrollViewer.VerticalOffset > 10)
            {
                this.ScrollEnabled = false;
                this.InvokeActions(this.ScrollEnabled);
            }
        }

        /// <summary>
        ///     The scroll to end.
        /// </summary>
        private void ScrollToEnd()
        {
            if (this.scroll != null && this.ScrollEnabled)
            {
                this.scroll.ScrollToEnd();
            }
        }

        /// <summary>
        /// The coll_ collection changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Reviewed. Suppression is OK here.")]
        private void coll_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                if (this.scroll != null && this.ScrollEnabled)
                {
                    this.scroll.ScrollToEnd();
                }
            }
        }

        #endregion
    }
}