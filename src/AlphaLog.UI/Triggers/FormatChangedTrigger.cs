﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HighlightingVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The highlighting view-model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Triggers
{
    using System.Windows.Interactivity;
    using System.Windows.Media;

    using AlphaLog.UI.Filtering;

    using Microsoft.Practices.Prism.PubSubEvents;
    using Microsoft.Practices.ServiceLocation;

    public class FormatChangedTrigger : TriggerBase<FilteringDataGrid>
    {
        protected override void OnAttached()
        {
            var eventAggrigator = ServiceLocator.Current.GetInstance<IEventAggregator>();
            eventAggrigator.GetEvent<FormatChangedEvent>().Subscribe(this.FormatChanged);
            base.OnAttached();
        }

        private void FormatChanged(DataGridFormat fontFamily)
        {
            this.AssociatedObject.FontSize = fontFamily.FontSize;
            this.AssociatedObject.FontFamily = new FontFamily(fontFamily.FontFamily);
            this.AssociatedObject.DateTimeFormat = fontFamily.TimestampFormat;
        }
    }
}
