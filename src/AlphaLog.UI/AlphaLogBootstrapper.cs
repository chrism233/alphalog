﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AlphaLogBootstrapper.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The alpha log bootstrapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI
{
    using System;
    using System.ComponentModel.Composition;
    using System.ComponentModel.Composition.Hosting;
    using System.IO;
    using System.Reflection;
    using System.Windows;

    using AlphaLog.UI.Common;
    using AlphaLog.UI.Common.Behaviors;
    using AlphaLog.UI.Matcher.ViewModel.Matcher;

    using Microsoft.Practices.Prism.MefExtensions;
    using Microsoft.Practices.Prism.Regions;

    /// <summary>
    ///     The alpha log bootstrapper.
    /// </summary>
    public class AlphaLogBootstrapper : MefBootstrapper
    {
        #region Fields

        /// <summary>
        ///     The settings manager.
        /// </summary>
        private readonly SettingsManager settingsManager;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AlphaLogBootstrapper"/> class.
        /// </summary>
        /// <param name="settingsManager">
        /// The settings manager.
        /// </param>
        public AlphaLogBootstrapper(SettingsManager settingsManager)
        {
            this.settingsManager = settingsManager;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the assembly directory.
        /// </summary>
        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                var uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     The configure aggregate catalog.
        /// </summary>
        protected override void ConfigureAggregateCatalog()
        {
            this.AggregateCatalog.Catalogs.Add(new AssemblyCatalog(typeof(AlwaysMatcherVM).Assembly));
            this.AggregateCatalog.Catalogs.Add(new AssemblyCatalog(typeof(ISettingsManager).Assembly));
            this.AggregateCatalog.Catalogs.Add(new AssemblyCatalog(typeof(AlphaLogBootstrapper).Assembly));
            this.AggregateCatalog.Catalogs.Add(new AssemblyCatalog(typeof(ColumniserFactory).Assembly));
            this.AggregateCatalog.Catalogs.Add(new DirectoryCatalog(Path.Combine(AssemblyDirectory, "Columniser"), "*.dll"));
        }

        /// <summary>
        ///     The configure container.
        /// </summary>
        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();
        }

        /// <summary>
        ///     The configure default region behaviors.
        /// </summary>
        /// <returns>
        ///     The <see cref="IRegionBehaviorFactory" />.
        /// </returns>
        protected override IRegionBehaviorFactory ConfigureDefaultRegionBehaviors()
        {
            IRegionBehaviorFactory factory = base.ConfigureDefaultRegionBehaviors();
            factory.AddIfMissing("AutoPopulateExportedViewsBehavior", typeof(AutoPopulateExportedViewsBehavior));

            return factory;
        }

        /// <summary>
        ///     The create shell.
        /// </summary>
        /// <returns>
        ///     The <see cref="DependencyObject" />.
        /// </returns>
        protected override DependencyObject CreateShell()
        {
            Mef.Container = this.Container;
            return this.Container.GetExportedValue<MainWindow>();
        }

        /// <summary>
        ///     The initialize shell.
        /// </summary>
        protected override void InitializeShell()
        {
            base.InitializeShell();
            Application.Current.MainWindow = (MainWindow)this.Shell;
            Application.Current.MainWindow.Show();
        }

        /// <summary>
        ///     The register bootstrapper provided types.
        /// </summary>
        protected override void RegisterBootstrapperProvidedTypes()
        {
            base.RegisterBootstrapperProvidedTypes();
            this.Container.ComposeExportedValue<ISettingsManager>(this.settingsManager);
        }

        #endregion
    }
}