﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   Interaction logic for MainWindow.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI
{
    using System.ComponentModel.Composition;
    using System.Windows;

    using AlphaLog.UI.ViewModel;

    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    [Export]
    public partial class MainWindow : Window
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="MainWindow" /> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();

            // this.Style = (Style)FindResource(typeof(Window));
        }

        #endregion

        // <summary>
        // Gets or sets the view model.
        // </summary>
        #region Public Properties

        /// <summary>
        ///     Gets or sets the view model.
        /// </summary>
        [Import]
        public MainWindowVM ViewModel
        {
            get
            {
                return this.DataContext as MainWindowVM;
            }

            set
            {
                this.DataContext = value;
            }
        }

        #endregion
    }
}