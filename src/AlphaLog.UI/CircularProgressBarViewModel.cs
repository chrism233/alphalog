﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CircularProgressBarViewModel.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   An attached view model that adapts a ProgressBar control to provide properties
//   that assist in the creation of a circular template
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI
{
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;

    /// <summary>
    ///     An attached view model that adapts a ProgressBar control to provide properties
    ///     that assist in the creation of a circular template
    /// </summary>
    public class CircularProgressBarViewModel : FrameworkElement, INotifyPropertyChanged
    {
        #region Static Fields

        /// <summary>
        ///     The attach property.
        /// </summary>
        public static readonly DependencyProperty AttachProperty = DependencyProperty.RegisterAttached(
            "Attach", 
            typeof(object), 
            typeof(CircularProgressBarViewModel), 
            new PropertyMetadata(null, OnAttachChanged));

        #endregion

        #region Fields

        /// <summary>
        ///     The _angle.
        /// </summary>
        private double angle;

        /// <summary>
        ///     The _centre x.
        /// </summary>
        private double centreX;

        /// <summary>
        ///     The _centre y.
        /// </summary>
        private double centreY;

        /// <summary>
        ///     The _diameter.
        /// </summary>
        private double diameter;

        /// <summary>
        ///     The _hole size factor.
        /// </summary>
        private double holeSizeFactor;

        /// <summary>
        ///     The _inner radius.
        /// </summary>
        private double innerRadius;

        /// <summary>
        ///     The _percent.
        /// </summary>
        private double percent;

        /// <summary>
        ///     The _progress bar.
        /// </summary>
        private ProgressBar progressBar;

        /// <summary>
        ///     The _radius.
        /// </summary>
        private double radius;

        #endregion

        #region Public Events

        /// <summary>
        ///     The property changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the angle.
        /// </summary>
        public double Angle
        {
            get
            {
                return this.angle;
            }

            set
            {
                this.angle = value;
                this.OnPropertyChanged("Angle");
            }
        }

        /// <summary>
        ///     Gets or sets the centre x.
        /// </summary>
        public double CentreX
        {
            get
            {
                return this.centreX;
            }

            set
            {
                this.centreX = value;
                this.OnPropertyChanged("CentreX");
            }
        }

        /// <summary>
        ///     Gets or sets the centre y.
        /// </summary>
        public double CentreY
        {
            get
            {
                return this.centreY;
            }

            set
            {
                this.centreY = value;
                this.OnPropertyChanged("CentreY");
            }
        }

        /// <summary>
        ///     Gets or sets the diameter.
        /// </summary>
        public double Diameter
        {
            get
            {
                return this.diameter;
            }

            set
            {
                this.diameter = value;
                this.OnPropertyChanged("Diameter");
            }
        }

        /// <summary>
        ///     Gets or sets the hole size factor.
        /// </summary>
        public double HoleSizeFactor
        {
            get
            {
                return this.holeSizeFactor;
            }

            set
            {
                this.holeSizeFactor = value;
                this.ComputeViewModelProperties();
            }
        }

        /// <summary>
        ///     Gets or sets the inner radius.
        /// </summary>
        public double InnerRadius
        {
            get
            {
                return this.innerRadius;
            }

            set
            {
                this.innerRadius = value;
                this.OnPropertyChanged("InnerRadius");
            }
        }

        /// <summary>
        ///     Gets or sets the percent.
        /// </summary>
        public double Percent
        {
            get
            {
                return this.percent;
            }

            set
            {
                this.percent = value;
                this.OnPropertyChanged("Percent");
            }
        }

        /// <summary>
        ///     Gets or sets the radius.
        /// </summary>
        public double Radius
        {
            get
            {
                return this.radius;
            }

            set
            {
                this.radius = value;
                this.OnPropertyChanged("Radius");
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get attach.
        /// </summary>
        /// <param name="d">
        /// The d.
        /// </param>
        /// <returns>
        /// The <see cref="CircularProgressBarViewModel"/>.
        /// </returns>
        public static CircularProgressBarViewModel GetAttach(DependencyObject d)
        {
            return (CircularProgressBarViewModel)d.GetValue(AttachProperty);
        }

        /// <summary>
        /// The set attach.
        /// </summary>
        /// <param name="d">
        /// The d.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        public static void SetAttach(DependencyObject d, CircularProgressBarViewModel value)
        {
            d.SetValue(AttachProperty, value);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Re-computes the various properties that the elements in the template bind to.
        /// </summary>
        protected virtual void ComputeViewModelProperties()
        {
            if (this.progressBar == null)
            {
                return;
            }

            this.Angle = (this.progressBar.Value - this.progressBar.Minimum) * 360
                         / (this.progressBar.Maximum - this.progressBar.Minimum);
            this.CentreX = this.progressBar.ActualWidth / 2;
            this.CentreY = this.progressBar.ActualHeight / 2;
            this.Radius = Math.Min(this.CentreX, this.CentreY);
            this.Diameter = this.Radius * 2;
            this.InnerRadius = this.Radius * this.HoleSizeFactor;
            this.Percent = this.Angle / 360;
        }

        /// <summary>
        /// The on property changed.
        /// </summary>
        /// <param name="property">
        /// The property.
        /// </param>
        protected void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        /// <summary>
        /// Handle the Loaded event of the element to which this view model is attached
        ///     in order to enable the attached
        ///     view model to bind to properties of the parent element
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private static void Element_Loaded(object sender, RoutedEventArgs e)
        {
            var targetElement = sender as FrameworkElement;
            CircularProgressBarViewModel attachedModel = GetAttach(targetElement);

            // find the ProgressBar and associated it with the view model
            var progressBar = targetElement.Ancestors<ProgressBar>().Single() as ProgressBar;
            attachedModel.SetProgressBar(progressBar);
        }

        /// <summary>
        /// Change handler for the Attach property
        /// </summary>
        /// <param name="d">
        /// The d.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private static void OnAttachChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            // set the view model as the DataContext for the rest of the template
            var targetElement = d as FrameworkElement;
            var viewModel = e.NewValue as CircularProgressBarViewModel;
            targetElement.DataContext = viewModel;

            // handle the loaded event
            targetElement.Loaded += Element_Loaded;
        }

        /// <summary>
        /// The register for notification.
        /// </summary>
        /// <param name="propertyName">
        /// The property Name.
        /// </param>
        /// <param name="element">
        /// The element.
        /// </param>
        /// <param name="callback">
        /// The callback.
        /// </param>
        /// Add a handler for a DP change
        /// see: http://amazedsaint.blogspot.com/2009/12/silverlight-listening-to-dependency.html
        private void RegisterForNotification(
            string propertyName, 
            FrameworkElement element, 
            PropertyChangedCallback callback)
        {
            // Bind to a dependency property  
            var b = new Binding(propertyName) { Source = element };
            DependencyProperty prop = DependencyProperty.RegisterAttached(
                "ListenAttached" + propertyName, 
                typeof(object), 
                typeof(UserControl), 
                new PropertyMetadata(callback));

            element.SetBinding(prop, b);
        }

        /// <summary>
        /// Add handlers for the updates on various properties of the ProgressBar
        /// </summary>
        /// <param name="progressBar">
        /// The progress Bar.
        /// </param>
        private void SetProgressBar(ProgressBar progressBar)
        {
            this.progressBar = progressBar;
            this.progressBar.SizeChanged += (s, e) => this.ComputeViewModelProperties();
            this.RegisterForNotification("Value", progressBar, (d, e) => this.ComputeViewModelProperties());
            this.RegisterForNotification("Maximum", progressBar, (d, e) => this.ComputeViewModelProperties());
            this.RegisterForNotification("Minimum", progressBar, (d, e) => this.ComputeViewModelProperties());

            this.ComputeViewModelProperties();
        }

        #endregion
    }
}