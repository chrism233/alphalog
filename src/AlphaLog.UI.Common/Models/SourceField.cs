// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SourceField.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The field.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Common.Settings.Model
{
    using System;
    using System.ComponentModel.Composition;
    using System.Xml.Linq;

    using AlphaLog.UI.Common.Models;

    using Microsoft.Practices.Prism.Mvvm;

    /// <summary>
    ///     The field.
    /// </summary>
    [Export]
    [Serializable]
    public class SourceField : BindableBase
    {
        #region Constants

        /// <summary>
        ///     The column setting name.
        /// </summary>
        public const string ColumnSettingName = "Field";

        #endregion

        #region Fields

        /// <summary>
        ///     The datatype.
        /// </summary>
        private DataType datatype;

        /// <summary>
        ///     The format.
        /// </summary>
        private string format = string.Empty;

        /// <summary>
        ///     The title.
        /// </summary>
        private string title;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="SourceField" /> class.
        /// </summary>
        public SourceField()
            : this(string.Empty, DataType.Text, string.Empty)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceField"/> class.
        /// </summary>
        /// <param name="field">
        /// The field
        /// </param>
        public SourceField(XElement field)
            : this(
                field.Attribute("name").Value, 
                (DataType)Enum.Parse(typeof(DataType), field.Attribute("datatype").Value), 
                field.Attribute("format").Value)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceField"/> class.
        ///     Initializes a new instance of the <see cref="FieldItem"/> class.
        /// </summary>
        /// <param name="title">
        /// The title.
        /// </param>
        /// <param name="datatype">
        /// The datatype.
        /// </param>
        /// <param name="format">
        /// The format.
        /// </param>
        [ImportingConstructor]
        public SourceField(string title, DataType datatype, string format)
        {
            this.Title = title;
            this.DataType = datatype;
            this.Format = format;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the data type.
        /// </summary>
        public DataType DataType
        {
            get
            {
                return this.datatype;
            }

            set
            {
                this.SetProperty(ref this.datatype, value);
            }
        }

        /// <summary>
        ///     Gets or sets the format.
        /// </summary>
        public string Format
        {
            get
            {
                return this.format;
            }

            set
            {
                this.SetProperty(ref this.format, value);
            }
        }

        /// <summary>
        ///     Gets or sets the title.
        /// </summary>
        public string Title
        {
            get
            {
                return this.title;
            }

            set
            {
                this.SetProperty(ref this.title, value);
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The serialize.
        /// </summary>
        /// <returns>
        ///     The <see cref="XElement" />.
        /// </returns>
        public virtual XElement Serialize()
        {
            return new XElement(
                ColumnSettingName, 
                new XAttribute("name", this.Title), 
                new XAttribute("datatype", this.DataType), 
                new XAttribute("format", this.Format));
        }

        #endregion
    }
}