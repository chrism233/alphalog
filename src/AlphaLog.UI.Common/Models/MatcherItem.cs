// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatcherItem.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   A data item for matchers.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Common.Models
{
    using System.Xml.Linq;

    using AlphaLog.Matcher;

    using Microsoft.Practices.Prism.Mvvm;

    /// <summary>
    ///     A data item for matchers.
    /// </summary>
    public class MatcherItem : BindableBase, IPersist
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MatcherItem"/> class.
        /// </summary>
        /// <param name="name">
        /// The matcher name.
        /// </param>
        /// <param name="matcher">
        /// The matcher instance.
        /// </param>
        public MatcherItem(string name, IMatch matcher)
        {
            this.Name = name;
            this.Matcher = matcher;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MatcherItem"/> class.
        /// </summary>
        /// <param name="name">
        /// The matcher type.
        /// </param>
        /// <param name="matcher">
        /// The matcher instance.
        /// </param>
        public MatcherItem(string name, XElement matcher)
            : this(name, MatchFactory.Parse(matcher))
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the matcher.
        /// </summary>
        public IMatch Matcher { get; set; }

        /// <summary>
        ///     Gets or sets the matcher name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the settings name.
        /// </summary>
        public string SettingsName
        {
            get
            {
                return "Matcher";
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Serializes the instance to XML.
        /// </summary>
        /// <returns>
        ///     The <see cref="XElement" /> representing the <see cref="MatcherItem" />.
        /// </returns>
        public XElement Serialize()
        {
            return new XElement(this.SettingsName, new XAttribute("name", this.Name), this.Matcher.Serialize());
        }

        #endregion
    }
}