// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HighlightDatabase.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The highlight database.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Common.Models
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    /// <summary>
    ///     The highlight database.
    /// </summary>
    public class HighlightDatabase : Repository<HighlightItem>
    {
        #region Constants

        /// <summary>
        ///     The highlight settings name.
        /// </summary>
        private const string HighlightSettingsName = "HighlightItems";

        #endregion

        #region Fields

        /// <summary>
        ///     The highlight items.
        /// </summary>
        private List<HighlightItem> highlightItems = new List<HighlightItem>();

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the settings name.
        /// </summary>
        public override string SettingsName
        {
            get
            {
                return HighlightSettingsName;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The get highlighters.
        /// </summary>
        /// <returns>
        ///     The <see cref="IEnumerable{Highlight}" />.
        /// </returns>
        public override IEnumerable<HighlightItem> GetItems()
        {
            return this.highlightItems;
        }

        /// <summary>
        /// The initialize.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        public override void Initialize(XElement element)
        {
            if (element != null)
            {
                this.highlightItems = element.Elements().Select(e => new HighlightItem(e)).ToList();
            }
        }

        /// <summary>
        ///     The serialize.
        /// </summary>
        /// <returns>
        ///     The <see cref="XElement" />.
        /// </returns>
        public override XElement Serialize()
        {
            return new XElement(HighlightSettingsName, this.highlightItems.Select(i => i.Serialize()));
        }

        #endregion

        #region Methods

        /// <summary>
        /// The store.
        /// </summary>
        /// <param name="items">
        /// The items.
        /// </param>
        protected override void StoreCore(IEnumerable<HighlightItem> items)
        {
            this.highlightItems = items.ToList();
        }

        #endregion
    }
}