﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FieldItem.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The column state.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Common.Models
{
    using System;
    using System.ComponentModel.Composition;
    using System.Xml.Linq;

    using Microsoft.Practices.Prism.Mvvm;

    /// <summary>
    ///     The column state.
    /// </summary>
    [Export]
    [Serializable]
    public class FieldItem : BindableBase, IPersist
    {
        #region Constants

        /// <summary>
        ///     The column setting name.
        /// </summary>
        public const string ColumnSettingName = "Field";

        #endregion

        #region Fields

        /// <summary>
        ///     The column header.
        /// </summary>
        private XElement columnHeader;

        /// <summary>
        ///     The datatype.
        /// </summary>
        private DataType datatype;

        /// <summary>
        ///     The description.
        /// </summary>
        private string description;

        /// <summary>
        ///     The title.
        /// </summary>
        private string title;

        /// <summary>
        ///     The visibility.
        /// </summary>
        private bool visibility;

        /// <summary>
        ///     The display index.
        /// </summary>
        private int displayIndex;

        /// <summary>
        ///     The width.
        /// </summary>
        private double width;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldItem"/> class.
        /// </summary>
        /// <param name="field">
        /// The field items.
        /// </param>
        public FieldItem(XElement field)
        {
            this.title = field.Attribute("name").Value;
            this.datatype = (DataType)Enum.Parse(typeof(DataType), field.Attribute("datatype").Value);
            this.width = double.Parse(field.Attribute("width").Value);
            this.visibility = bool.Parse(field.Attribute("visible").Value);
            this.displayIndex = int.Parse(field.Attribute("displayIndex").Value);
            this.columnHeader = field.Element("ColumnHeader");
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldItem"/> class.
        /// </summary>
        /// <param name="title">
        /// The title.
        /// </param>
        /// <param name="datatype">
        /// The datatype.
        /// </param>
        /// <param name="width">
        /// The width.
        /// </param>
        /// <param name="visibility">
        /// The visibility.
        /// </param>
        /// <param name="header">
        /// The header.
        /// </param>
        public FieldItem(string title, DataType datatype, double width, bool visibility,int displayIndex, XElement header)
        {
            this.Title = title;
            this.datatype = datatype;
            this.width = width;
            this.visibility = visibility;
            this.displayIndex = displayIndex;
            this.columnHeader = header;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the column header element.
        /// </summary>
        public XElement ColumnHeader
        {
            get
            {
                return this.columnHeader;
            }

            set
            {
                this.SetProperty(ref this.columnHeader, value);
            }
        }

        /// <summary>
        ///     Gets or sets the data type.
        /// </summary>
        public DataType DataType
        {
            get
            {
                return this.datatype;
            }

            set
            {
                this.SetProperty(ref this.datatype, value);
            }
        }

        /// <summary>
        ///     Gets or sets the description.
        /// </summary>
        public string Description
        {
            get
            {
                return this.description;
            }

            set
            {
                this.SetProperty(ref this.description, value);
            }
        }

        /// <summary>
        ///     Gets the settings name.
        /// </summary>
        public string SettingsName
        {
            get
            {
                return ColumnSettingName;
            }
        }

        /// <summary>
        ///     Gets or sets the title.
        /// </summary>
        public string Title
        {
            get
            {
                return this.title;
            }

            set
            {
                this.SetProperty(ref this.title, value);
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether visible.
        /// </summary>
        public bool Visible
        {
            get
            {
                return this.visibility;
            }

            set
            {
                this.SetProperty(ref this.visibility, value);
            }
        }

        /// <summary>
        ///     Gets or sets the width.
        /// </summary>
        public double Width
        {
            get
            {
                return this.width;
            }

            set
            {
                this.SetProperty(ref this.width, value);
            }
        }

        /// <summary>
        /// Gets or sets the display index.
        /// </summary>
        public int DisplayIndex
        {
            get
            {
                return this.displayIndex;
            }
            set
            {
                this.SetProperty(ref this.displayIndex, value);
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get default.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="FieldItem"/>.
        /// </returns>
        public static FieldItem GetDefault(string name)
        {
            return new FieldItem(name, DataType.Text, 200, false, 0, new XElement("ColumnHeader"));
        }

        /// <summary>
        ///     The serialize.
        /// </summary>
        /// <returns>
        ///     The <see cref="XElement" />.
        /// </returns>
        public XElement Serialize()
        {
            return new XElement(
                this.SettingsName, 
                new XAttribute("name", this.Title), 
                new XAttribute("width", this.width), 
                new XAttribute("visible", this.visibility), 
                new XAttribute("displayIndex", this.displayIndex),
                new XAttribute("datatype", this.DataType), 
                this.ColumnHeader);
        }

        #endregion
    }
}