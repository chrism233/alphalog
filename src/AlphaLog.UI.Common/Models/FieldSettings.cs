// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FieldSettings.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The field settings.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Common.Models
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    /// <summary>
    ///     The field settings.
    /// </summary>
    public class FieldSettings : Repository<FieldItem>
    {
        #region Constants

        /// <summary>
        ///     The field settings name.
        /// </summary>
        public const string FieldSettingsName = "Fields";

        #endregion

        #region Fields

        /// <summary>
        ///     The column dictionary.
        /// </summary>
        private Dictionary<string, FieldItem> columnDictionary;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="FieldSettings" /> class.
        /// </summary>
        public FieldSettings()
        {
            this.columnDictionary = new Dictionary<string, FieldItem>();
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the settings name.
        /// </summary>
        public override string SettingsName
        {
            get
            {
                return FieldSettingsName;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get items.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{FieldItem}"/>.
        /// </returns>
        public override IEnumerable<FieldItem> GetItems()
        {
            return this.columnDictionary.Values;
        }

        /// <summary>
        /// The initialize.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        public override void Initialize(XElement element)
        {
            this.columnDictionary = element.Elements().Select(c => new FieldItem(c)).ToDictionary(k => k.Title, v => v);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The store core.
        /// </summary>
        /// <param name="items">
        /// The items.
        /// </param>
        protected override void StoreCore(IEnumerable<FieldItem> items)
        {
            foreach (FieldItem field in items)
            {
                if (this.columnDictionary.ContainsKey(field.Title))
                {
                    this.columnDictionary[field.Title] = field;
                }
                else
                {
                    this.columnDictionary.Add(field.Title, field);
                }
            }
        }

        #endregion
    }
}