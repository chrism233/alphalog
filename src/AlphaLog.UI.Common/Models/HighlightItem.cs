﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HighlightItem.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The highlight item.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Common.Models
{
    using System.Linq;
    using System.Windows.Media;
    using System.Xml.Linq;

    using AlphaLog.Matcher;

    using Microsoft.Practices.Prism.Mvvm;

    /// <summary>
    ///     The highlight item.
    /// </summary>
    public class HighlightItem : BindableBase, IPersist
    {
        #region Fields

        /// <summary>
        ///     The background color.
        /// </summary>
        private Color backgroundColor;

        /// <summary>
        ///     The foreground color.
        /// </summary>
        private Color foregroundColor;

        /// <summary>
        ///     The matcher.
        /// </summary>
        private IMatch matcher;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="HighlightItem"/> class.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        public HighlightItem(XElement element)
            : this(
                (Color)ColorConverter.ConvertFromString(element.Attribute("foreground").Value), 
                (Color)ColorConverter.ConvertFromString(element.Attribute("background").Value), 
                MatchFactory.Parse(element.Element("Matcher").Elements().FirstOrDefault()))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HighlightItem"/> class.
        /// </summary>
        /// <param name="foreground">
        /// The foreground.
        /// </param>
        /// <param name="background">
        /// The background.
        /// </param>
        /// <param name="matcher">
        /// The matcher.
        /// </param>
        public HighlightItem(Color foreground, Color background, IMatch matcher)
        {
            this.ForegroundColor = foreground;
            this.BackgroundColor = background;
            this.Matcher = matcher;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the background color.
        /// </summary>
        public Color BackgroundColor
        {
            get
            {
                return this.backgroundColor;
            }

            set
            {
                this.SetProperty(ref this.backgroundColor, value);
            }
        }

        /// <summary>
        ///     Gets the description.
        /// </summary>
        public string Description
        {
            get
            {
                return this.Matcher.Description();
            }
        }

        /// <summary>
        ///     Gets or sets the foreground color.
        /// </summary>
        public Color ForegroundColor
        {
            get
            {
                return this.foregroundColor;
            }

            set
            {
                this.SetProperty(ref this.foregroundColor, value);
            }
        }

        /// <summary>
        ///     Gets or sets the matcher.
        /// </summary>
        public IMatch Matcher
        {
            get
            {
                return this.matcher;
            }

            set
            {
                this.SetProperty(ref this.matcher, value);
            }
        }

        /// <summary>
        ///     Gets the settings name.
        /// </summary>
        public string SettingsName
        {
            get
            {
                return "Highlighting";
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The serialize.
        /// </summary>
        /// <returns>
        ///     The <see cref="XElement" />.
        /// </returns>
        public XElement Serialize()
        {
            return new XElement(
                this.SettingsName, 
                new XAttribute("foreground", this.foregroundColor.ToString()), 
                new XAttribute("background", this.backgroundColor.ToString()), 
                new XElement("Matcher", this.Matcher.Serialize()));
        }

        #endregion
    }
}