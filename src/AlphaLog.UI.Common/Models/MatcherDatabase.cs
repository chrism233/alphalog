﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatcherDatabase.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The matcher database.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Common.Models
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Xml.Linq;

    using AlphaLog.Matcher;

    /// <summary>
    ///     The matcher database.
    /// </summary>
    public class MatcherDatabase : Repository<MatcherItem>
    {
        #region Constants

        /// <summary>
        ///     The matcher settings name.
        /// </summary>
        public const string MatcherSettingsName = "Matchers";

        #endregion

        #region Fields

        /// <summary>
        ///     The database.
        /// </summary>
        private Dictionary<string, IMatch> database = new Dictionary<string, IMatch>();

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the settings name.
        /// </summary>
        public override string SettingsName
        {
            get
            {
                return MatcherSettingsName;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Add a new matcher to the database.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="matcher">
        /// The matcher.
        /// </param>
        public void Add(string name, IMatch matcher)
        {
            if (this.database.ContainsKey(name))
            {
                this.database[name] = matcher;
            }
            else
            {
                this.database.Add(name, matcher);
            }
        }

        /// <summary>
        ///     Load the matchers from the database.
        /// </summary>
        /// <returns>
        ///     A <see cref="IEnumerable{MatcherItem}" /> of the matchers.
        /// </returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", 
            Justification = "This would not be appropriate in this case.")]
        public override IEnumerable<MatcherItem> GetItems()
        {
            return this.database.Select(v => new MatcherItem(v.Key, v.Value));
        }

        /// <summary>
        /// The initialize.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        public override void Initialize(XElement element)
        {
            this.database = element.Elements()
                .ToDictionary(e => e.Attribute("name").Value, v => MatchFactory.Parse(v.Elements().FirstOrDefault()));
        }

        /// <summary>
        /// Remove the matcher.
        /// </summary>
        /// <param name="selectedMatcherItem">
        /// The matcher to remove from the database.
        /// </param>
        public void Remove(MatcherItem selectedMatcherItem)
        {
            this.database.Remove(selectedMatcherItem.Name);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Store the items.
        /// </summary>
        /// <param name="items">
        /// The matcher items.
        /// </param>
        protected override void StoreCore(IEnumerable<MatcherItem> items)
        {
            foreach (MatcherItem item in items)
            {
                this.Add(item.Name, item.Matcher);
            }
        }

        #endregion
    }
}