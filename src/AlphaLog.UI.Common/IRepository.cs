namespace AlphaLog.UI.Common
{
    using System.Xml.Linq;

    /// <summary>
    /// The Repository interface.
    /// </summary>
    public interface IRepository : IPersist
    {
        #region Public Methods and Operators

        /// <summary>
        /// The initialize.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        void Initialize(XElement element);

        #endregion
    }
}