﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AutoPopulateExportedViewBehavior.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The auto populate exported views behavior.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Common.Behaviors
{
    using System;
    using System.ComponentModel.Composition;
    using System.Diagnostics.CodeAnalysis;

    using Microsoft.Practices.Prism.Regions;

    /// <summary>
    ///     The auto populate exported views behavior.
    /// </summary>
    [Export(typeof(AutoPopulateExportedViewsBehavior))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class AutoPopulateExportedViewsBehavior : RegionBehavior, IPartImportsSatisfiedNotification
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the region manager.
        /// </summary>
        [Import]
        public IRegionManager RegionManager { get; set; }

        /// <summary>
        ///     Gets or sets the registered views.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays", 
            Justification = "MEF injected values")]
        [ImportMany(AllowRecomposition = true)]
        public Lazy<object, IViewRegionRegistration>[] RegisteredViews { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The on imports satisfied.
        /// </summary>
        public void OnImportsSatisfied()
        {
            this.AddRegisteredViews();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     The on attach.
        /// </summary>
        protected override void OnAttach()
        {
            this.AddRegisteredViews();
        }

        /// <summary>
        ///     The add registered views.
        /// </summary>
        private void AddRegisteredViews()
        {
            if (this.Region != null)
            {
                foreach (var viewEntry in this.RegisteredViews)
                {
                    if (viewEntry.Metadata.RegionName == this.Region.Name)
                    {
                        object view = viewEntry.Value;

                        if (!this.Region.Views.Contains(view))
                        {
                            this.Region.Add(view);
                        }
                    }
                }

                if (!this.RegionManager.Regions.ContainsRegionWithName(this.Region.Name))
                {
                    this.RegionManager.Regions.Add(this.Region);
                }
            }
        }

        #endregion
    }
}