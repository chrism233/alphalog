// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObservableCollectionWithPropertyChanged.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The observable collection with property changed.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Common
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;

    /// <summary>
    /// The observable collection with property changed.
    /// </summary>
    /// <typeparam name="T">
    /// The collection type.
    /// </typeparam>
    public class ObservableCollectionWithPropertyChanged<T> : ObservableCollection<T>
        where T : INotifyPropertyChanged
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ObservableCollectionWithPropertyChanged{T}"/> class.
        /// </summary>
        /// <param name="list">
        /// The list.
        /// </param>
        public ObservableCollectionWithPropertyChanged(IEnumerable<T> list)
        {
            foreach (T item in list)
            {
                this.Add(item);
            }
        }

        #endregion

        #region Public Events

        /// <summary>
        ///     The item property changed.
        /// </summary>
        public event PropertyChangedEventHandler ItemPropertyChanged;

        #endregion

        #region Methods

        /// <summary>
        /// The insert item.
        /// </summary>
        /// <param name="index">
        /// The index.
        /// </param>
        /// <param name="item">
        /// The item.
        /// </param>
        protected override void InsertItem(int index, T item)
        {
            base.InsertItem(index, item);
            item.PropertyChanged += this.Item_PropertyChanged;
        }

        /// <summary>
        /// The item_ property changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void Item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            PropertyChangedEventHandler handler = this.ItemPropertyChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        #endregion
    }
}