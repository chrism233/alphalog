﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SettingsManager.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The settings.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Common
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;

    /// <summary>
    ///     The settings.
    /// </summary>
    public class SettingsManager : ISettingsManager
    {
        #region Fields

        /// <summary>
        ///     The settings collection.
        /// </summary>
        private readonly Dictionary<Type, IRepository> settingsCollection = new Dictionary<Type, IRepository>();

        /// <summary>
        ///     The settings.
        /// </summary>
        private XDocument settings;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsManager"/> class.
        /// </summary>
        /// <param name="alphaLogSettingsFileName">
        /// The alpha log settings file name.
        /// </param>
        public SettingsManager(string alphaLogSettingsFileName)
        {
            this.settings = XDocument.Load(alphaLogSettingsFileName);
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Gets the settings.
        /// </summary>
        /// <typeparam name="TSettings">
        ///     The settings to resolve.
        /// </typeparam>
        /// <returns>
        ///     The <see cref="TSettings" /> instance.
        /// </returns>
        public TSettings GetsSetting<TSettings>() where TSettings : IRepository, new()
        {
            IRepository settings = null;

            if (!this.settingsCollection.TryGetValue(typeof(TSettings), out settings))
            {
                var newSettings = new TSettings();
                XElement settingsElement = this.GetElement(newSettings.SettingsName);
                if (settingsElement != null)
                {
                    newSettings.Initialize(settingsElement);
                }

                this.settingsCollection[typeof(TSettings)] = newSettings;
                return newSettings;
            }

            return (TSettings)settings;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsManager"/> class.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        public void Initialize(string path)
        {
            this.settings = XDocument.Load(path);
        }

        /// <summary>
        /// The save changes.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        public void SaveChanges(string path)
        {
            foreach (IPersist columnizer in this.settingsCollection.Values)
            {
                XElement element = this.GetElement(columnizer.SettingsName);
                if (element != null)
                {
                    element.ReplaceWith(columnizer.Serialize());
                }
                else
                {
                    this.settings.Root.Add(columnizer.Serialize());
                }
            }

            this.settings.Save(path);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get element.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="XElement"/>.
        /// </returns>
        private XElement GetElement(string name)
        {
            return this.settings.Root.Element(name);
        }

        #endregion
    }
}