// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LogDescriptorCollection.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The log descriptor collection.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;

    /// <summary>
    ///     The log descriptor collection.
    /// </summary>
    public class LogDescriptorCollection : PropertyDescriptorCollection, ICustomTypeDescriptor
    {
        #region Fields

        /// <summary>
        ///     The log items.
        /// </summary>
        private readonly Dictionary<string, object> logItems = new Dictionary<string, object>();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LogDescriptorCollection"/> class.
        /// </summary>
        /// <param name="dictionary">
        /// The dictionary.
        /// </param>
        public LogDescriptorCollection(Dictionary<string, object> dictionary)
            : base(
                dictionary.Where(k => k.Value != null)
                    .Select(k => new LogItemDescriptor(k.Key, k.Value.GetType()))
                    .ToArray())
        {
            this.logItems = dictionary;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the items.
        /// </summary>
        public Dictionary<string, object> Items
        {
            get
            {
                return this.logItems;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add item.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        public void AddItem(string name, object value)
        {
            this.logItems.Add(name, value);
        }

        /// <summary>
        ///     Returns a collection of custom attributes for this instance of a component.
        /// </summary>
        /// <returns>
        ///     An <see cref="T:System.ComponentModel.AttributeCollection" /> containing the attributes for this object.
        /// </returns>
        public AttributeCollection GetAttributes()
        {
            return TypeDescriptor.GetAttributes(this);
        }

        /// <summary>
        ///     Returns the class name of this instance of a component.
        /// </summary>
        /// <returns>
        ///     The class name of the object, or null if the class does not have a name.
        /// </returns>
        public string GetClassName()
        {
            return TypeDescriptor.GetClassName(this);
        }

        /// <summary>
        ///     Returns the name of this instance of a component.
        /// </summary>
        /// <returns>
        ///     The name of the object, or null if the object does not have a name.
        /// </returns>
        public string GetComponentName()
        {
            return TypeDescriptor.GetComponentName(this);
        }

        /// <summary>
        ///     Returns a type converter for this instance of a component.
        /// </summary>
        /// <returns>
        ///     A <see cref="T:System.ComponentModel.TypeConverter" /> that is the converter for this object, or null if there is
        ///     no <see cref="T:System.ComponentModel.TypeConverter" /> for this object.
        /// </returns>
        public TypeConverter GetConverter()
        {
            return TypeDescriptor.GetConverter(this);
        }

        /// <summary>
        ///     Returns the default event for this instance of a component.
        /// </summary>
        /// <returns>
        ///     An <see cref="T:System.ComponentModel.EventDescriptor" /> that represents the default event for this object, or
        ///     null if this object does not have events.
        /// </returns>
        public EventDescriptor GetDefaultEvent()
        {
            return TypeDescriptor.GetDefaultEvent(this);
        }

        /// <summary>
        ///     Returns the default property for this instance of a component.
        /// </summary>
        /// <returns>
        ///     A <see cref="T:System.ComponentModel.PropertyDescriptor" /> that represents the default property for this object,
        ///     or null if this object does not have properties.
        /// </returns>
        public PropertyDescriptor GetDefaultProperty()
        {
            return TypeDescriptor.GetDefaultProperty(this);
        }

        /// <summary>
        /// Returns an editor of the specified type for this instance of a component.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Object"/> of the specified type that is the editor for this object, or null if the editor
        ///     cannot be found.
        /// </returns>
        /// <param name="editorBaseType">
        /// A <see cref="T:System.Type"/> that represents the editor for this object.
        /// </param>
        public object GetEditor(Type editorBaseType)
        {
            return TypeDescriptor.GetEditor(this, editorBaseType);
        }

        /// <summary>
        ///     Returns the events for this instance of a component.
        /// </summary>
        /// <returns>
        ///     An <see cref="T:System.ComponentModel.EventDescriptorCollection" /> that represents the events for this component
        ///     instance.
        /// </returns>
        public EventDescriptorCollection GetEvents()
        {
            return TypeDescriptor.GetEvents(this);
        }

        /// <summary>
        /// Returns the events for this instance of a component using the specified attribute array as a filter.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.ComponentModel.EventDescriptorCollection"/> that represents the filtered events for this
        ///     component instance.
        /// </returns>
        /// <param name="attributes">
        /// An array of type <see cref="T:System.Attribute"/> that is used as a filter.
        /// </param>
        public EventDescriptorCollection GetEvents(Attribute[] attributes)
        {
            return TypeDescriptor.GetEvents(this, attributes);
        }

        /// <summary>
        ///     Returns the properties for this instance of a component.
        /// </summary>
        /// <returns>
        ///     A <see cref="T:System.ComponentModel.PropertyDescriptorCollection" /> that represents the properties for this
        ///     component instance.
        /// </returns>
        public PropertyDescriptorCollection GetProperties()
        {
            var col = new PropertyDescriptorCollection(null);

            foreach (var item in this.logItems.Where(k => k.Value != null))
            {
                col.Add(new LogItemDescriptor(item.Key, item.Value.GetType()));
            }

            return col;
        }

        /// <summary>
        /// Returns the properties for this instance of a component using the attribute array as a filter.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.ComponentModel.PropertyDescriptorCollection"/> that represents the filtered properties for
        ///     this component instance.
        /// </returns>
        /// <param name="attributes">
        /// An array of type <see cref="T:System.Attribute"/> that is used as a filter.
        /// </param>
        public PropertyDescriptorCollection GetProperties(Attribute[] attributes)
        {
            return this.GetProperties();
        }

        /// <summary>
        /// Returns an object that contains the property described by the specified property descriptor.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Object"/> that represents the owner of the specified property.
        /// </returns>
        /// <param name="pd">
        /// A <see cref="T:System.ComponentModel.PropertyDescriptor"/> that represents the property whose owner
        ///     is to be found.
        /// </param>
        public object GetPropertyOwner(PropertyDescriptor pd)
        {
            return this;
        }

        #endregion
    }
}