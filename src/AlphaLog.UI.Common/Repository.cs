// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PersistanceCollection.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The Repository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    /// <summary>
    /// The persistence collection.
    /// </summary>
    /// <typeparam name="T">
    /// The item type to be persisted.
    /// </typeparam>
    public abstract class Repository<T> : IRepository
        where T : IPersist
    {
        #region Public Events

        /// <summary>
        ///     The collection changed.
        /// </summary>
        public event EventHandler<EventArgs> Updated;

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the settings name.
        /// </summary>
        public abstract string SettingsName { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Get the items.
        /// </summary>
        /// <returns>The items.</returns>
        public abstract IEnumerable<T> GetItems();

        /// <summary>
        /// The initialize.
        /// </summary>
        /// <param name="element">
        /// The element.
        /// </param>
        public abstract void Initialize(XElement element);

        /// <summary>
        ///     The serialize.
        /// </summary>
        /// <returns>
        ///     The <see cref="XElement" />.
        /// </returns>
        public virtual XElement Serialize()
        {
            return new XElement(this.SettingsName, this.GetItems().Select(c => c.Serialize()));
        }

        /// <summary>
        /// Store a single item.
        /// </summary>
        /// <param name="item">
        /// An item to store.
        /// </param>
        public void Store(T item)
        {
            this.Store(new[] { item });
        }

        /// <summary>
        /// Store items to be persisted.
        /// </summary>
        /// <param name="items">
        /// The items to store.
        /// </param>
        public void Store(IEnumerable<T> items)
        {
            this.StoreCore(items);
            this.OnUpdated();
        }

        #endregion

        #region Methods

        /// <summary>
        /// The on updated.
        /// </summary>
        protected virtual void OnUpdated()
        {
            EventHandler<EventArgs> handler = this.Updated;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Store items to be persisted.
        /// </summary>
        /// <param name="items">
        /// The items to store.
        /// </param>
        protected abstract void StoreCore(IEnumerable<T> items);

        #endregion
    }
}