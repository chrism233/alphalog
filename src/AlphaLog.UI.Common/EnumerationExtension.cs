﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnumerationExtension.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The enumeration extension.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Common
{
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows.Markup;

    /// <summary>
    ///     The enumeration extension.
    /// </summary>
    public class EnumerationExtension : MarkupExtension
    {
        #region Fields

        /// <summary>
        ///     The _enum type.
        /// </summary>
        private Type enumType;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumerationExtension"/> class.
        /// </summary>
        /// <param name="enumType">
        /// The enum type.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// The <paramref name="enumType"/> is null.
        /// </exception>
        public EnumerationExtension(Type enumType)
        {
            if (enumType == null)
            {
                throw new ArgumentNullException("enumType");
            }

            this.EnumType = enumType;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the enum type.
        /// </summary>
        /// <exception cref="ArgumentException">
        ///     Thrown if the type is not an enum.
        /// </exception>
        public Type EnumType
        {
            get
            {
                return this.enumType;
            }

            private set
            {
                if (this.enumType == value)
                {
                    return;
                }

                Type enumType = Nullable.GetUnderlyingType(value) ?? value;

                if (enumType.IsEnum == false)
                {
                    throw new ArgumentException("Type must be an Enum.");
                }

                this.enumType = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The provide value.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            Array enumValues = Enum.GetValues(this.EnumType);

            return (from object enumValue in enumValues
                    select new EnumerationMember { Value = enumValue, Description = this.GetDescription(enumValue) })
                .ToArray();
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get description.
        /// </summary>
        /// <param name="enumValue">
        /// The enum value.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetDescription(object enumValue)
        {
            var descriptionAttribute =
                this.EnumType.GetField(enumValue.ToString())
                    .GetCustomAttributes(typeof(DescriptionAttribute), false)
                    .FirstOrDefault() as DescriptionAttribute;

            return descriptionAttribute != null ? descriptionAttribute.Description : enumValue.ToString();
        }

        #endregion

        /// <summary>
        ///     The enumeration member.
        /// </summary>
        public class EnumerationMember
        {
            #region Public Properties

            /// <summary>
            ///     Gets or sets the description.
            /// </summary>
            public string Description { get; set; }

            /// <summary>
            ///     Gets or sets the value.
            /// </summary>
            public object Value { get; set; }

            #endregion
        }
    }
}