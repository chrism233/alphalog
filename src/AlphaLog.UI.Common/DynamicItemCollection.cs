// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DynamicItemCollection.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The dynamic item collection.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Common
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;

    /// <summary>
    /// The dynamic item collection.
    /// </summary>
    /// <typeparam name="T">
    /// The dynamic item type.
    /// </typeparam>
    public class DynamicItemCollection<T> : ObservableCollection<T>, ITypedList
        where T : PropertyDescriptorCollection, ICustomTypeDescriptor
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="DynamicItemCollection{T}" /> class.
        /// </summary>
        public DynamicItemCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicItemCollection{T}"/> class.
        /// </summary>
        /// <param name="toList">
        /// The to list.
        /// </param>
        public DynamicItemCollection(List<T> toList)
            : base(toList)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get item properties.
        /// </summary>
        /// <param name="listAccessors">
        /// The list accessors.
        /// </param>
        /// <returns>
        /// The <see cref="PropertyDescriptorCollection"/>.
        /// </returns>
        public PropertyDescriptorCollection GetItemProperties(PropertyDescriptor[] listAccessors)
        {
            var dynamicDescriptors = new PropertyDescriptor[0];

            var set = new HashSet<PropertyDescriptor>();
            foreach (T item in this.Items)
            {
                foreach (object i in item)
                {
                    set.Add((PropertyDescriptor)i);
                }
            }

            return new PropertyDescriptorCollection(set.ToArray());
        }

        /// <summary>
        /// The get list name.
        /// </summary>
        /// <param name="listAccessors">
        /// The list accessors.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetListName(PropertyDescriptor[] listAccessors)
        {
            return null;
        }

        #endregion
    }
}