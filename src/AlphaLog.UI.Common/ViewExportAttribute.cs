﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ViewExportAttribute.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The view export attribute.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Common
{
    using System;
    using System.ComponentModel.Composition;

    using AlphaLog.UI.Common.Behaviors;

    /// <summary>
    ///     The view export attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    [MetadataAttribute]
    public sealed class ViewExportAttribute : ExportAttribute, IViewRegionRegistration
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ViewExportAttribute" /> class.
        /// </summary>
        public ViewExportAttribute()
            : base(typeof(object))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewExportAttribute"/> class.
        /// </summary>
        /// <param name="viewName">
        /// The view name.
        /// </param>
        public ViewExportAttribute(string viewName)
            : base(viewName, typeof(object))
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the region name.
        /// </summary>
        public string RegionName { get; set; }

        /// <summary>
        ///     Gets or sets the tree path.
        /// </summary>
        public string TreePath { get; set; }

        /// <summary>
        ///     Gets the view name.
        /// </summary>
        public string ViewName
        {
            get
            {
                return this.ContractName;
            }
        }

        #endregion
    }
}