﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LevelToSolidColorConverter.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The level to solid color converter.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Common.Converters
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    ///     The level to solid color converter.
    /// </summary>
    public class LevelToSolidColorConverter : IValueConverter
    {
        #region Fields

        /// <summary>
        ///     The debug color.
        /// </summary>
        private readonly SolidColorBrush debugColor =
            Application.Current.FindResource("DebugLevelColor") as SolidColorBrush;

        /// <summary>
        ///     The error color.
        /// </summary>
        private readonly SolidColorBrush errorColor =
            Application.Current.FindResource("ErrorLevelColor") as SolidColorBrush;

        /// <summary>
        ///     The fatal color.
        /// </summary>
        private readonly SolidColorBrush fatalColor =
            Application.Current.FindResource("FatalLevelColor") as SolidColorBrush;

        /// <summary>
        ///     The info color.
        /// </summary>
        private readonly SolidColorBrush infoColor =
            Application.Current.FindResource("InfoLevelColor") as SolidColorBrush;

        /// <summary>
        ///     The warn color.
        /// </summary>
        private readonly SolidColorBrush warnColor =
            Application.Current.FindResource("WarnLevelColor") as SolidColorBrush;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The convert.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="targetType">
        /// The target type.
        /// </param>
        /// <param name="parameter">
        /// The parameter.
        /// </param>
        /// <param name="culture">
        /// The culture.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (null == value)
            {
                return Brushes.Transparent;
            }

            var levelIndex = (int)value;
            switch (levelIndex)
            {
                case 1:
                    return this.debugColor ?? Brushes.Transparent;
                case 2:
                    return this.infoColor ?? Brushes.Transparent;
                case 3:
                    return this.warnColor ?? Brushes.Transparent;
                case 4:
                    return this.errorColor ?? Brushes.Transparent;
                case 5:
                    return this.fatalColor ?? Brushes.Transparent;
                default:
                    return Brushes.Transparent;
            }
        }

        /// <summary>
        /// The convert back.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="targetType">
        /// The target type.
        /// </param>
        /// <param name="parameter">
        /// The parameter.
        /// </param>
        /// <param name="culture">
        /// The culture.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

        #endregion
    }
}