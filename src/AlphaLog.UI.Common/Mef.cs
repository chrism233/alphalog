﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Mef.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   Holds the singleton instance of the MEF CompositionContainer
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition.Hosting;
    using System.Diagnostics;
    using System.Reflection;

    /// <summary>
    ///     Holds the singleton instance of the MEF CompositionContainer
    /// </summary>
    public class Mef
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the container.
        /// </summary>
        public static CompositionContainer Container { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add from directory.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="filter">
        /// The filter.
        /// </param>
        public static void AddFromDirectory(string path, string filter)
        {
            var caralog = (AggregateCatalog)Container.Catalog;

            caralog.Catalogs.Add(new DirectoryCatalog(path, filter));
        }

        /// <summary>
        /// Adds the specified types to the catalogue.
        /// </summary>
        /// <param name="types">
        /// Add a type to the container.
        /// </param>
        public static void AddTypes(params Type[] types)
        {
            var catalog = (AggregateCatalog)Container.Catalog;

            catalog.Catalogs.Add(new TypeCatalog(types));
        }

        /// <summary>
        /// Adds exported types from the specified assembly into the container.
        /// </summary>
        /// <param name="assembly">
        /// The assembly.
        /// </param>
        public static void AddTypesFromAssembly(Assembly assembly)
        {
            var catelog = (AggregateCatalog)Container.Catalog;

            Debug.WriteLine("MEF: Adding " + assembly.FullName);

            catelog.Catalogs.Add(new AssemblyCatalog(assembly));
        }

        /// <summary>
        ///     Gets an exported value from the container
        /// </summary>
        /// <typeparam name="TObject">
        ///     The object to obtain.
        /// </typeparam>
        /// <returns>
        ///     The <see cref="TObject" />.
        /// </returns>
        public static TObject GetExport<TObject>()
        {
            DebugWarnDisposableObject<TObject>();
            return Container.GetExportedValue<TObject>();
        }

        /// <summary>
        /// Gets an exported value from container.
        /// </summary>
        /// <param name="type">
        /// The type to resolve.
        /// </param>
        /// <returns>
        /// The instance.
        /// </returns>
        public static object GetExport(Type type)
        {
            MethodInfo getExportForType = typeof(Mef).GetMethod("GetExport", Type.EmptyTypes).MakeGenericMethod(type);

            return getExportForType.Invoke(null, null);
        }

        /// <summary>
        ///     Gets an exported value from the container
        /// </summary>
        /// <typeparam name="TObject">
        ///     The object type to resolve.
        /// </typeparam>
        /// <returns>
        ///     An <see cref="IEnumerable{T}" /> of the resolved objects.
        /// </returns>
        public static IEnumerable<TObject> GetExports<TObject>()
        {
            DebugWarnDisposableObject<TObject>();
            return Container.GetExportedValues<TObject>();
        }

        /// <summary>
        ///     Reset the container (i.e. dispose and create a new one).
        /// </summary>
        public static void Reset()
        {
            if (Container != null)
            {
                Container.Dispose();
            }

            Container = CreateThreadSafeContainer();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     The create thread safe container.
        /// </summary>
        /// <returns>
        ///     The <see cref="CompositionContainer" />.
        /// </returns>
        private static CompositionContainer CreateThreadSafeContainer()
        {
            return new CompositionContainer(new AggregateCatalog(), true);
        }

        /// <summary>
        ///     The debug warn disposable object.
        /// </summary>
        /// <typeparam name="TObject">
        ///     The object to display warnings for.
        /// </typeparam>
        [Conditional("DEBUG")]
        private static void DebugWarnDisposableObject<TObject>()
        {
            if (typeof(IDisposable).IsAssignableFrom(typeof(TObject)))
            {
                Debug.WriteLine(
                    string.Format(
                        "MEMORY LEAK WARNING: {0} returned by Mef.GetObject. MEF will hold a reference indefinitely.", 
                        typeof(TObject).FullName));
            }
        }

        #endregion
    }
}