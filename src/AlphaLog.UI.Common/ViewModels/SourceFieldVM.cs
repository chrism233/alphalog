// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SourceFieldVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The source field vm.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Common.ViewModels
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;

    using AlphaLog.UI.Common.Models;
    using AlphaLog.UI.Common.Settings.Model;

    using Microsoft.Practices.Prism.Mvvm;
    using Microsoft.Practices.Prism.ViewModel;

    /// <summary>
    ///     The source field vm.
    /// </summary>
    public class SourceFieldVM : BindableBase, INotifyDataErrorInfo
    {
        #region Fields

        /// <summary>
        ///     The error container.
        /// </summary>
        private readonly ErrorsContainer<string> errorContainer;

        /// <summary>
        ///     The field.
        /// </summary>
        private readonly SourceField field = new SourceField();

        /// <summary>
        ///     The settings.
        /// </summary>
        private readonly ISettingsManager settings;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceFieldVM"/> class.
        /// </summary>
        /// <param name="field">
        /// The field.
        /// </param>
        /// <param name="settings">
        /// The settings.
        /// </param>
        public SourceFieldVM(SourceField field, ISettingsManager settings)
        {
            this.errorContainer = new ErrorsContainer<string>(this.OnErrorsChanged);
            this.Title = field.Title;
            this.DataType = field.DataType;
            this.Format = field.Format;
            this.settings = settings;
            this.PropertyChanged += (e, o) => this.HasUnSavedChanges = true;
        }

        #endregion

        #region Public Events

        /// <summary>
        ///     The errors changed.
        /// </summary>
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the data type.
        /// </summary>
        public DataType DataType
        {
            get
            {
                return this.field.DataType;
            }

            set
            {
                this.field.DataType = value;
                this.OnPropertyChanged(() => this.DataType);
                this.UpdateValidation();
            }
        }

        /// <summary>
        ///     Gets or sets the format.
        /// </summary>
        public string Format
        {
            get
            {
                return this.field.Format;
            }

            set
            {
                this.field.Format = value;
                this.OnPropertyChanged(() => this.Format);
            }
        }

        /// <summary>
        ///     Gets a value indicating whether has errors.
        /// </summary>
        public bool HasErrors
        {
            get
            {
                return this.errorContainer.HasErrors;
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether has un saved changes.
        /// </summary>
        public bool HasUnSavedChanges { get; set; }

        /// <summary>
        ///     Gets or sets the title.
        /// </summary>
        public string Title
        {
            get
            {
                return this.field.Title;
            }

            set
            {
                this.field.Title = value;
                this.OnPropertyChanged(() => this.Title);
                this.UpdateValidation();
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get errors.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable GetErrors(string propertyName)
        {
            return this.errorContainer.GetErrors(propertyName);
        }

        /// <summary>
        ///     The get field.
        /// </summary>
        /// <returns>
        ///     The <see cref="SourceField" />.
        /// </returns>
        public SourceField GetField()
        {
            if (!this.HasErrors)
            {
                this.HasUnSavedChanges = false;
            }

            return this.field;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The on errors changed.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        protected virtual void OnErrorsChanged(string propertyName)
        {
            EventHandler<DataErrorsChangedEventArgs> handler = this.ErrorsChanged;
            if (handler != null)
            {
                handler(this, new DataErrorsChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        ///     The update validation.
        /// </summary>
        private void UpdateValidation()
        {
            if (this.settings != null)
            {
                if (
                    this.settings.GetsSetting<FieldSettings>()
                        .GetItems()
                        .Any(f => f.Title == this.Title && f.DataType != this.DataType))
                {
                    this.errorContainer.SetErrors(
                        () => this.Title, 
                        new List<string> { "This Title or DataType conflicts with an existing field" });
                    this.errorContainer.SetErrors(
                        () => this.DataType, 
                        new List<string> { "This Title or DataType conflicts with an existing field" });
                }
                else if (string.IsNullOrEmpty(this.Title))
                {
                    this.errorContainer.SetErrors(() => this.Title, new List<string> { "The title must have a value." });
                }
                else
                {
                    this.errorContainer.ClearErrors(() => this.Title);
                    this.errorContainer.ClearErrors(() => this.DataType);
                }
            }
        }

        #endregion
    }
}