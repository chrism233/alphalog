// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BindingListEx.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The binding list ex.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    /// <summary>
    /// The binding list ex.
    /// </summary>
    /// <typeparam name="T">
    /// The binding list type.
    /// </typeparam>
    public class BindingListEx<T> : BindingList<T>
    {
        #region Fields

        /// <summary>
        ///     The factory.
        /// </summary>
        private readonly Func<T> factory;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BindingListEx{T}"/> class.
        /// </summary>
        /// <param name="select">
        /// The select.
        /// </param>
        /// <param name="factory">
        /// The factory.
        /// </param>
        public BindingListEx(List<T> @select, Func<T> factory)
            : base(@select)
        {
            this.factory = factory;
            this.AllowNew = true;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     The add new core.
        /// </summary>
        /// <returns>
        ///     The <see cref="object" />.
        /// </returns>
        protected override object AddNewCore()
        {
            T f = this.factory();
            this.Add(f);
            return f;
        }

        #endregion
    }
}