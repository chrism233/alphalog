﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LogItemDescriptor.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The log item descriptor.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Common
{
    using System;
    using System.ComponentModel;

    /// <summary>
    ///     The log item descriptor.
    /// </summary>
    public class LogItemDescriptor : PropertyDescriptor
    {
        #region Fields

        /// <summary>
        ///     The m_prop type.
        /// </summary>
        private readonly Type type;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LogItemDescriptor"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        public LogItemDescriptor(string name, Type type)
            : base(name, null)
        {
            this.type = type;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     When overridden in a derived class, gets the type of the component this property is bound to.
        /// </summary>
        /// <returns>
        ///     A <see cref="T:System.Type" /> that represents the type of component this property is bound to. When the
        ///     <see cref="M:System.ComponentModel.PropertyDescriptor.GetValue(System.Object)" /> or
        ///     <see cref="M:System.ComponentModel.PropertyDescriptor.SetValue(System.Object,System.Object)" /> methods are
        ///     invoked, the object specified might be an instance of this type.
        /// </returns>
        public override Type ComponentType
        {
            get
            {
                return this.GetType();
            }
        }

        /// <summary>
        ///     When overridden in a derived class, gets a value indicating whether this property is read-only.
        /// </summary>
        /// <returns>
        ///     true if the property is read-only; otherwise, false.
        /// </returns>
        public override bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        ///     When overridden in a derived class, gets the type of the property.
        /// </summary>
        /// <returns>
        ///     A <see cref="T:System.Type" /> that represents the type of the property.
        /// </returns>
        public override Type PropertyType
        {
            get
            {
                return this.type;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// When overridden in a derived class, returns whether resetting an object changes its value.
        /// </summary>
        /// <returns>
        /// true if resetting the component changes its value; otherwise, false.
        /// </returns>
        /// <param name="component">
        /// The component to test for reset capability.
        /// </param>
        public override bool CanResetValue(object component)
        {
            return true;
        }

        /// <summary>
        /// When overridden in a derived class, gets the current value of the property on a component.
        /// </summary>
        /// <returns>
        /// The value of a property for a given component.
        /// </returns>
        /// <param name="component">
        /// The component with the property for which to retrieve the value.
        /// </param>
        public override object GetValue(object component)
        {
            return ((LogDescriptorCollection)component).Items[this.Name];
        }

        /// <summary>
        /// When overridden in a derived class, resets the value for this property of the component to the default value.
        /// </summary>
        /// <param name="component">
        /// The component with the property value that is to be reset to the default value.
        /// </param>
        public override void ResetValue(object component)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// When overridden in a derived class, sets the value of the component to a different value.
        /// </summary>
        /// <param name="component">
        /// The component with the property value that is to be set.
        /// </param>
        /// <param name="value">
        /// The new value.
        /// </param>
        public override void SetValue(object component, object value)
        {
            ((LogDescriptorCollection)component).Items[this.Name] = value;
        }

        /// <summary>
        /// When overridden in a derived class, determines a value indicating whether the value of this property needs to be
        ///     persisted.
        /// </summary>
        /// <returns>
        /// true if the property should be persisted; otherwise, false.
        /// </returns>
        /// <param name="component">
        /// The component with the property to be examined for persistence.
        /// </param>
        public override bool ShouldSerializeValue(object component)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}