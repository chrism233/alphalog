﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TabSettingsVM.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The tab settings vm.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog.UI.Common
{
    using System.Windows.Input;

    using Microsoft.Practices.Prism.Commands;
    using Microsoft.Practices.Prism.Mvvm;

    /// <summary>
    ///     The tab settings vm.
    /// </summary>
    public class TabSettingsVM : BindableBase
    {
        #region Fields

        /// <summary>
        ///     The has changes.
        /// </summary>
        private bool hasChanges;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TabSettingsVM"/> class.
        /// </summary>
        /// <param name="headerText">
        /// The header text.
        /// </param>
        public TabSettingsVM(string headerText)
        {
            this.HeaderText = headerText;
            this.SaveChangesCommand = new DelegateCommand(this.SaveChanges);
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets a value indicating whether has un saved changes.
        /// </summary>
        public bool HasUnSavedChanges
        {
            get
            {
                return this.hasChanges;
            }

            set
            {
                this.SetProperty(ref this.hasChanges, value);
            }
        }

        /// <summary>
        ///     Gets or sets the header text.
        /// </summary>
        public string HeaderText { get; protected set; }

        /// <summary>
        ///     Gets or sets the save changed command.
        /// </summary>
        public ICommand SaveChangesCommand { get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     The save changes.
        /// </summary>
        protected virtual void SaveChanges()
        {
            this.HasUnSavedChanges = false;
        }

        #endregion
    }
}