// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ColumniserDetails.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The log level.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog
{
    using System.IO;

    /// <summary>
    ///     The columniser details.
    /// </summary>
    public class Details
    {
        #region Fields

        /// <summary>
        ///     The file path.
        /// </summary>
        private string filePath;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Details"/> class.
        /// </summary>
        /// <param name="file">
        /// The file path.
        /// </param>
        /// <param name="description">
        /// The columniser description.
        /// </param>
        public Details(string file, string description)
        {
            this.FilePath = file;
            this.Description = description;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the description.
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        ///     Gets the file name.
        /// </summary>
        public string FileName
        {
            get
            {
                return Path.GetFileName(this.filePath);
            }
        }

        /// <summary>
        ///     Gets or sets the file path.
        /// </summary>
        public string FilePath
        {
            get
            {
                return this.filePath;
            }

            set
            {
                this.filePath = value;
            }
        }

        #endregion
    }
}