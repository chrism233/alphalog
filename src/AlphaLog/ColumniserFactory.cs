// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ColumniserFactory.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   Columniser factory.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog
{
    using System.Collections.Generic;
    using System.Linq;

    using AlphaLog.Columniser;

    /// <summary>
    ///     Columniser factory.
    /// </summary>
    public class ColumniserFactory
    {
        #region Fields

        /// <summary>
        ///     The columnizers.
        /// </summary>
        private readonly IEnumerable<IColumnizerDescriptor> columnizers;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ColumniserFactory"/> class.
        /// </summary>
        /// <param name="columnizers">
        /// The columnizers.
        /// </param>
        public ColumniserFactory(IEnumerable<IColumnizerDescriptor> columnizers)
        {
            this.columnizers = columnizers;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Get a valid columniser  instance.
        /// </summary>
        /// <param name="filePath">
        /// A sample log entry.
        /// </param>
        /// <returns>
        /// The <see cref="ColumnizerDescriptor"/>.
        /// </returns>
        public IColumnizerDescriptor GetColumniser(string filePath)
        {
            return this.columnizers.SingleOrDefault(c => c.CanColumnize(filePath)) ?? new DefaultColumniserDescriptor();
        }

        #endregion
    }
}