﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AlphaLogProgress.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The alpha log progress.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog
{
    using System;
    using System.Reactive;
    using System.Reactive.Subjects;

    /// <summary>
    ///     The alpha log progress.
    /// </summary>
    public class AlphaLogProgress : ObservableBase<ProgressInfo>, IProgress<ProgressInfo>
    {
        #region Fields

        /// <summary>
        ///     The progress update.
        /// </summary>
        private readonly Subject<ProgressInfo> progressUpdate = new Subject<ProgressInfo>();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The report.
        /// </summary>
        /// <param name="state">
        /// The state.
        /// </param>
        /// <param name="progress">
        /// The progress.
        /// </param>
        public void Report(ProcessState state, int progress)
        {
            this.Report(new ProgressInfo(state, progress));
        }

        /// <summary>
        /// The report.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public void Report(ProgressInfo value)
        {
            this.progressUpdate.OnNext(value);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The subscribe core.
        /// </summary>
        /// <param name="observer">
        /// The observer.
        /// </param>
        /// <returns>
        /// The <see cref="IDisposable"/>.
        /// </returns>
        protected override IDisposable SubscribeCore(IObserver<ProgressInfo> observer)
        {
            return this.progressUpdate.Subscribe(observer);
        }

        #endregion
    }
}