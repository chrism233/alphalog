﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AlphaLog.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The alpha log parser.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AlphaLog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reactive;
    using System.Reactive.Subjects;
    using System.Threading.Tasks;

    using AlphaLog.Columniser;

    /// <summary>
    ///     The alpha log parser.
    /// </summary>
    public class AlphaLogParser : IObservable<Dictionary<string, object>>, IDisposable
    {
        #region Fields

        /// <summary>
        ///     The column name to data type.
        /// </summary>
        private readonly Dictionary<string, Type> columnNameToDataType = new Dictionary<string, Type>();

        /// <summary>
        ///     The columniser factory.
        /// </summary>
        private readonly ColumniserFactory columniserFactory;

        /// <summary>
        ///     The progress.
        /// </summary>
        private readonly AlphaLogProgress progress;

        /// <summary>
        ///     The active columnisers.
        /// </summary>
        private Dictionary<string, IColumnizer> activeColumnisers = new Dictionary<string, IColumnizer>();

        /// <summary>
        ///     The alpha log subscribers.
        /// </summary>
        private Subject<Dictionary<string, object>> alphaLogSubscribers = new Subject<Dictionary<string, object>>();

        /// <summary>
        ///     The disposed.
        /// </summary>
        private bool disposed;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AlphaLogParser"/> class.
        /// </summary>
        /// <param name="columnisers">
        /// The columnisers.
        /// </param>
        public AlphaLogParser(IEnumerable<IColumnizerDescriptor> columnisers)
        {
            this.columniserFactory = new ColumniserFactory(columnisers);
            this.progress = new AlphaLogProgress();
            this.Progress = this.progress;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the progress.
        /// </summary>
        public IObservable<ProgressInfo> Progress { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add file.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <returns>
        /// The <see cref="Details"/>.
        /// </returns>
        /// <exception cref="ObjectDisposedException">
        /// The <see cref="AlphaLog"/> parser has been disposed.
        /// </exception>
        public Details Add(string file)
        {
            if (this.disposed)
            {
                throw new ObjectDisposedException(typeof(AlphaLogParser).Name);
            }

            IColumnizer tempColumniser = this.GetColumniser(file);
            tempColumniser.Subscribe(this.alphaLogSubscribers.AsObserver());

            var columniserDetails = new Details(tempColumniser.File, tempColumniser.Name);
            if (this.UpdateMappingTable(tempColumniser))
            {
                columniserDetails = new ErrorDetails(file, "The data types conflict with another file.");
                if (this.progress != null)
                {
                    this.progress.Report(ProcessState.Error, 0);
                }
            }

            return columniserDetails;
        }

        /// <summary>
        ///     The dispose.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);

            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Removes the specified file.
        /// </summary>
        /// <param name="file">
        /// The file to remove.
        /// </param>
        /// <exception cref="ObjectDisposedException">
        /// The <see cref="AlphaLog"/> parser has been disposed.
        /// </exception>
        public void Remove(string file)
        {
            if (this.disposed)
            {
                throw new ObjectDisposedException(typeof(AlphaLogParser).Name);
            }

            if (this.activeColumnisers.ContainsKey(file))
            {
                this.activeColumnisers[file].Dispose();
                this.activeColumnisers.Remove(file);
            }

            if (this.RebuildMappingTable())
            {
                if (this.progress != null)
                {
                    this.progress.Report(ProcessState.Error, 0);
                }
            }
            else
            {
                if (this.progress != null)
                {
                    this.progress.Report(ProcessState.None, 0);
                }
            }
        }

        /// <summary>
        ///     Rests the columnisers.
        /// </summary>
        public void Reset()
        {
            List<string> files = this.activeColumnisers.Keys.ToList();
            foreach (string file in files)
            {
                this.Remove(file);
                this.Add(file);
            }
        }

        /// <summary>
        /// The subscribe.
        /// </summary>
        /// <param name="observer">
        /// The observer.
        /// </param>
        /// <returns>
        /// A <see cref="IDisposable"/> allowing the observer to be unsubscribed.
        /// </returns>
        /// <exception cref="ObjectDisposedException">
        /// The <see cref="AlphaLog"/> has been disposed.
        /// </exception>
        public IDisposable Subscribe(IObserver<Dictionary<string, object>> observer)
        {
            if (this.disposed)
            {
                throw new ObjectDisposedException(typeof(AlphaLogParser).Name);
            }

            return this.alphaLogSubscribers.Subscribe(observer);
        }

        /// <summary>
        /// Triggers an update.
        /// </summary>
        /// <exception cref="ObjectDisposedException">
        /// The <see cref="AlphaLog"/> parser has been disposed.
        /// </exception>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public Task Update()
        {
            if (this.disposed)
            {
                throw new ObjectDisposedException(typeof(AlphaLogParser).Name);
            }

            var tcs = new TaskCompletionSource<bool>();

            Task.Factory.StartNew(
                () =>
                    {
                        Task[] tasks =
                            this.activeColumnisers.Values.Select(c => Task.Factory.StartNew(c.Update)).ToArray();
                        int completedTasks = 0;
                        int numberOfTasks = tasks.Count();
                        do
                        {
                            int index = Task.WaitAny(tasks);
                            completedTasks++;
                            if (this.progress != null)
                            {
                                this.progress.Report(
                                    ProcessState.Processing, 
                                    this.CalculatePercentage(completedTasks, numberOfTasks));
                            }
                        }
                        while (tasks.Count() > completedTasks);

                        Task.WaitAll(tasks);
                        tcs.SetResult(true);
                    });

            return tcs.Task;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.alphaLogSubscribers.Dispose();
                    foreach (IColumnizer activerColumniser in this.activeColumnisers.Values)
                    {
                        activerColumniser.Dispose();
                    }

                    this.activeColumnisers.Clear();
                    this.columnNameToDataType.Clear();
                }

                this.alphaLogSubscribers = null;
                this.activeColumnisers = null;

                this.disposed = true;
            }
        }

        /// <summary>
        /// The calculate percentage.
        /// </summary>
        /// <param name="complete">
        /// The complete.
        /// </param>
        /// <param name="total">
        /// The total.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        private int CalculatePercentage(int complete, int total)
        {
            double percent = 1 - ((double)(total - complete) / total);
            return (int)(percent * 100.0);
        }

        /// <summary>
        /// The get columniser.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <returns>
        /// The <see cref="IColumnizer"/>.
        /// </returns>
        private IColumnizer GetColumniser(string file)
        {
            IColumnizer tempColumniser;
            if (this.activeColumnisers.ContainsKey(file))
            {
                tempColumniser = this.activeColumnisers[file];
            }
            else
            {
                tempColumniser = this.columniserFactory.GetColumniser(file).Create(file);
                this.activeColumnisers.Add(file, tempColumniser);
            }

            return tempColumniser;
        }

        /// <summary>
        /// The rebuild mapping table.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool RebuildMappingTable()
        {
            bool hasConflictingDataTypes = false;
            this.columnNameToDataType.Clear();
            foreach (IColumnizer col in this.activeColumnisers.Values)
            {
                foreach (var item in col.GetColumns())
                {
                    if (!this.columnNameToDataType.ContainsKey(item.Key))
                    {
                        this.columnNameToDataType.Add(item.Key, item.Value);
                    }
                    else
                    {
                        if (this.columnNameToDataType[item.Key] != item.Value)
                        {
                            hasConflictingDataTypes = true;
                        }
                    }
                }
            }

            return hasConflictingDataTypes;
        }

        /// <summary>
        /// The update mapping table.
        /// </summary>
        /// <param name="tempColumniser">
        /// The temp columniser.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool UpdateMappingTable(IColumnizer tempColumniser)
        {
            bool hasConflictingDataTypes = false;
            foreach (var item in tempColumniser.GetColumns())
            {
                if (!this.columnNameToDataType.ContainsKey(item.Key))
                {
                    this.columnNameToDataType.Add(item.Key, item.Value);
                }
                else
                {
                    if (this.columnNameToDataType[item.Key] != item.Value)
                    {
                        hasConflictingDataTypes = true;
                    }
                }
            }

            return hasConflictingDataTypes;
        }

        #endregion
    }
}