﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgressInfo.cs" company="AlphaLog">
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//   
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary>
//   The progress info.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AlphaLog
{
    /// <summary>
    ///     The progress info.
    /// </summary>
    public class ProgressInfo
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressInfo"/> class.
        /// </summary>
        /// <param name="state">
        /// The state.
        /// </param>
        /// <param name="calculatePercentage">
        /// The calculate percentage.
        /// </param>
        public ProgressInfo(ProcessState state, int calculatePercentage)
        {
            this.State = state;
            this.Progress = calculatePercentage;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the progress.
        /// </summary>
        public int Progress { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        public ProcessState State { get; set; }

        #endregion
    }
}