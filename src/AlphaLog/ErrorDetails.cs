namespace AlphaLog
{
    /// <summary>
    /// The error details.
    /// </summary>
    public class ErrorDetails : Details
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorDetails"/> class.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        public ErrorDetails(string file, string description)
            : base(file, description)
        {
        }

        #endregion
    }
}